<?php

namespace Vermal\Admin;


use Vermal\Admin\Defaults\Controller;
use Vermal\Admin\Modules\Auth\Auth;
use Vermal\Database\Database;
use Vermal\Router;

class MenuRenderer
{
    private static $isAdminMenu = false;
    private static $isDropdownActive = false;
    /**
     * Render menu
     *
     * @param $menuName
     * @param $template
     * @return string
     */
    public static function menu($menuName, $template = null)
    {
        // Get Menu
        $menu = Database::Menu()->where('m.name', $menuName)->first();

        // Check if is admin menu
        if ($menu->name === 'admin') self::$isAdminMenu = true;

        // Get items
        $items = Database::MenuItem()
            ->order('m.order', 'ASC')
            ->where('m.parent', '=', null)
            ->where('m.menu', $menu->id)
            ->get();

        if (is_string($template) && $template == 'bootstrap') {
            $template = [
                'ul_first' => 'navbar-nav',
                'ul' => 'dropdown-menu',
                'li' => 'nav-item',
                'li_active' => 'active',
                'li_dropdown' => '',
                'li_dropdown_active' => '',
                'a' => 'nav-link',
                'a_active' => '',
                'a_dropdown' => 'dropdown-toggle'
            ];
        } else if (is_string($template) && $template == 'atmos') {
            $template = [
                'ul_first' => 'menu',
                'ul' => 'sub-menu',
                'li' => 'menu-item',
                'li_active' => 'active',
                'li_dropdown' => '',
                'li_dropdown_active' => 'opened',
                'a' => 'menu-link',
                'a_active' => '',
                'a_dropdown' => 'open-dropdown'
            ];
        } else if (!is_array($template) || $template == null) {
            $template = [
                'ul_first' => '',
                'ul' => '',
                'li' => 'active',
                'li_active' => '',
                'li_dropdown' => '',
                'li_dropdown_active' => '',
                'a' => '',
                'a_active' => '',
                'a_dropdown' => ''
            ];
        }

//        self::renderMenu($items, $template, Router::$currentName);
//        die();
        return self::renderMenu($items, $template, Router::$currentName);
    }

    /**
     * Render menu
     *
     * @param $items
     * @param $template
     * @param $currentUrl
     * @param bool $first
     * @return string
     */
    private static function renderMenu($items, $template, $currentUrl, $first = true)
    {
        $html = '<ul class="' . (!$first ? $template['ul'] : $template['ul_first']) . '" ' . (self::$isDropdownActive ? "style=\"display: block\"" : "") . '>' . PHP_EOL;
        foreach ($items as $k => $v) {

            // Get url
            if ($v->type == 'static') {
                $url = $v->url;
                $urlToCheck = $url;
            }  else {
                $params = json_decode($v->parameters, true);
                $params = ($params !== null ? $params : []);
                $url = Router::link($v->route, $params);
                $urlToCheck = $v->route;
            }

            // Check if link is active
            $active = false;
            if ($urlToCheck == $currentUrl) {
                $active = true;
            } else {
                foreach ($v->children as $child) {
                    if ($child->type == 'static' && $child->url == $currentUrl) $active = true;
                    else if ($child->route == $currentUrl) $active = true;
                    else if (strpos($currentUrl, '.') !== false) {
                        $route = explode('.', $child->route);
                        $currentRoute = explode('.', $currentUrl);
                        unset($route[count($route) - 1]);
                        unset($currentRoute[count($currentRoute) - 1]);
                        if ($route == $currentRoute) {
                            $active = true;
                            break;
                        }
                    }
                }
            }

            if (self::$isAdminMenu && $v->note !== '') {
                $resource = Database::Model('Resource')->find($v->note);
                if ($resource === null || !Auth::isAuthorized($resource->name)) continue;
            }
            if (!$v->children->isEmpty()) {
                self::$isDropdownActive = $active;
                $html .= self::li($template, $active, true);
                $html .= self::renderMenuList($v, $url, $template, $active, true, true);
                $html .= self::renderMenu($v->children, $template, $currentUrl, false);
                $html .= '</li>';
            } else {
                $html .= self::renderMenuList($v, $url, $template, $active, false, false);
            }
        }
        $html .= '</ul>' . PHP_EOL;
        return $html;
    }

    /**
     * Render menu list item
     *
     * @param $v
     * @param $url
     * @param array|null $template
     * @param boolean $active
     * @param $skipLi
     * @param $dropdown
     * @return string
     */
    private static function renderMenuList($v, $url, $template, $active, $skipLi, $dropdown)
    {
        $html = "";
        if (!$skipLi)
            $html .= self::li($template, $active, $dropdown);
        $html .= '<a href="' . $url . '" target="' . $v->target . '"  
                        class="' . $template['a'] . ' ' . ($active ? $template['a_active'] : '') . ($dropdown ? $template['a_dropdown'] : '') . '">';
        $html .= self::label($v->title, $dropdown);
        if (trim($v->iconClass) !== '') {
            $html .= self::icon($v->iconClass);
        }
        $html .= '</a>';
        if (!$skipLi)
            $html .= '</li>';
        return $html;
    }

    /**
     * Create li start tag
     *
     * @param $template
     * @param $active
     * @param $dropdown
     * @return string
     */
    private static function li($template, $active, $dropdown)
    {
        return '<li class="' . $template['li'] . ' ' .
            ($active ? $template['li_active'] : '') . ' ' .
            ($active && $dropdown ? $template['li_dropdown_active'] : '') . ' ' .
            ($dropdown ? $template['li_dropdown'] : '') . '">';
    }

    private static function label($label, $dropdown)
    {
        if (strpos($label, '__') === 0) {
            $label = Controller::__(str_replace('__', '', $label));
        }
        return '<span class="menu-label"><span class="menu-name">' . $label . ($dropdown ? '<span class="menu-arrow"></span>' : '') . ' </span></span>';
    }

    private static function icon($icon)
    {
        return '<span class="menu-icon"><i class="icon-placeholder ' . $icon . ' "></i></span>';
    }
}
