<?php

namespace Vermal\Admin;
use Vermal\Admin\Defaults\Seeder;
use Vermal\Admin\Defaults\Texts;
use Vermal\Admin\Modules\Auth\Auth;
use Vermal\Admin\Modules\Auth\Entities\Permission\Permission;
use Vermal\Admin\Modules\Auth\Entities\Resource\Resource;
use Vermal\Admin\Modules\Auth\Entities\Role\Role;
use Vermal\Admin\Modules\Auth\Entities\User\User;
use Vermal\Admin\Modules\Extensions\Datagrid;
use Vermal\Admin\Modules\Faq\Entities\FaqTr;
use Vermal\Admin\Modules\FormBuilder\Entities\Form;
use Vermal\Admin\Modules\FormBuilder\Entities\FormItem;
use Vermal\Admin\Modules\Gallery\Entities\Gallery;
use Vermal\Admin\Modules\Gallery\Entities\GalleryItem;
use Vermal\Admin\Modules\Modules\Entities\Module\Module;
use Vermal\Admin\Modules\Modules\Entities\ModuleValue\ModuleValue;
use Vermal\Admin\Modules\Menu\Entities\Menu;
use Vermal\Admin\Modules\Menu\Entities\MenuItem;
use Vermal\Admin\Modules\PDFTemplates\Entities\PDFTemplate;
use Vermal\Admin\Modules\Seo\Entities\Seo;
use Vermal\Admin\Modules\Settings\Entities\LocalizationSetting;
use Vermal\Admin\Modules\Settings\Entities\Setting;
use Vermal\Admin\Modules\Multimedia\Entities\Multimedia;
use Vermal\Admin\Modules\Multimedia\Entities\MultimediaFolder;
use Vermal\Admin\Modules\Post\Entities\Post;
use Vermal\Admin\Modules\Post\Entities\PostMeta;
use Vermal\Admin\Modules\Post\Entities\PostMetaTr;
use Vermal\Admin\Modules\Post\Entities\PostTr;
use Vermal\Admin\Modules\Text\Entities\Text;
use Vermal\Admin\Modules\Text\Entities\TextTr;
use Vermal\App;
use Vermal\Console\Console;
use Vermal\Admin\Modules\Faq\Entities\Faq;
use Vermal\Router;

/**
 * Class AdminServiceProvider
 * @package Vermal\Admin
 */
class AdminServiceProvider
{

    /**
     * Init admin
     */
    public function boot()
    {
//        // Add Router shortcuts
//        Router::addShortcut('locale', '(sk|en)->sk');

        // Define default HASH
        define('ADMIN_HASH', PASSWORD_DEFAULT);

        // define admin path
		App::bind('admin.base_path', __DIR__);

        // Load routes
        require_once __DIR__ . '/routes/web.php';

        // Add text tag
        (new Texts())->command();

        // Register entities
        $this->registerEntities([
            // Auth Entites
            "User" => [
                "namespace" => User::class,
                "path" => "Modules/Auth/Entities/User"
            ],
            "Role" => [
                "namespace" => Role::class,
                "path" => "Modules/Auth/Entities/Role"
            ],
            "Permission" => [
                "namespace" => Permission::class,
                "path" => "Modules/Auth/Entities/Permission"
            ],
            "Resource" => [
                "namespace" => Resource::class,
                "path" => "Modules/Auth/Entities/Resource"
            ],

            // Menu builder
            "Menu" => [
                "namespace" => Menu::class,
                "path" => "Modules/Menu/Entities"
            ],
            "MenuItem" => [
                "namespace" => MenuItem::class,
                "path" => "Modules/Menu/Entities"
            ],

            // Multimedia
            "Multimedia" => [
                "namespace" => Multimedia::class,
                "path" => "Modules/Multimedia/Entities"
            ],
            "MultimediaFolder" => [
                "namespace" => MultimediaFolder::class,
                "path" => "Modules/Multimedia/Entities"
            ],


            // Posts
            "Post" => [
                "namespace" => Post::class,
                "path" => "Modules/Post/Entities"
            ],
            "PostTr" => [
                "namespace" => PostTr::class,
                "path" => "Modules/Post/Entities"
            ],
            "PostMeta" => [
                "namespace" => PostMeta::class,
                "path" => "Modules/Post/Entities"
            ],
            "PostMetaTr" => [
                "namespace" => PostMetaTr::class,
                "path" => "Modules/Post/Entities"
            ],


            // Settings
            "Setting" => [
                "namespace" => Setting::class,
                "path" => "Modules/Settings/Entities"
            ],
            "LocalizationSetting" => [
                "namespace" => LocalizationSetting::class,
                "path" => "Modules/Settings/Entities"
            ],

            // Text
            "Text" => [
                "namespace" => Text::class,
                "path" => "Modules/Text/Entities"
            ],
            "TextTr" => [
                "namespace" => TextTr::class,
                "path" => "Modules/Text/Entities"
            ],

            // FormBuilder
            "Form" => [
                "namespace" => Form::class,
                "path" => "Modules/FormBuilder/Entities"
            ],
            "FormItem" => [
                "namespace" => FormItem::class,
                "path" => "Modules/FormBuilder/Entities"
            ],

            // Modules
            "Module" => [
                "namespace" => Module::class,
                "path" => "Modules/Modules/Entities"
            ],
            "ModuleValue" => [
                "namespace" => ModuleValue::class,
                "path" => "Modules/Modules/Entities"
            ],

            // PDF Template
            "PDFTemplate" => [
                "namespace" => PDFTemplate::class,
                "path" => "Modules/PDFTemplates/Entities"
            ],

			// FAQ
			"Faq" => [
				"namespace" => Faq::class,
				"path" => "Modules/Faq/Entities"
			],
			"FaqTr" => [
				"namespace" => FaqTr::class,
				"path" => "Modules/Faq/Entities"
			],

			// Gallery
			"Gallery" => [
				"namespace" => Gallery::class,
				"path" => "Modules/Gallery/Entities"
			],
//			"GalleryItem" => [
//				"namespace" => GalleryItem::class,
//				"path" => "Modules/Gallery/Entities"
//			],

			"Seo" => [
				"namespace" => Seo::class,
				"path" => "Modules/Seo/Entities"
			],
        ]);

        // Add localization file
        App::merge('localization-files', [
			__DIR__ . '/../public/lang/'
		]);
        Console::registerCommand('admin-seed', [Seeder::class, 'main']);
    }

    public function afterBoot()
    {
		// Custom View tags
		$this->view();

        Router::prefix('{locale::locale}?/admin/', function() {
            // Error 404
            Router::get('*', '\\Vermal\\Admin\\Defaults\\ErrorPages@e404');
        }, 'admin.', ['locale']);

		// Datagrid action
		\Vermal\DataGrid::setAction(routerLink('admin.datagrid-action'));
    }

    /**
     * Register all entites and paths mentioned in boot method
     *
     * @param $entites
     */
    private function registerEntities($entites)
    {
        $database = App::get('database');
        foreach ($entites as $key => $entity) {
            $database['entities'][$entity['namespace']] = $key;
            $database['paths'][$key] = 'vendor/vermal/admin/src/App/' . $entity['path'];
        }
        App::bind('database', $database);
    }

    /**
     * Custom View tags
     */
    private function view()
    {
        View::addRegexTag("v:admin:href=[\"\'](.*?)[\"\']", 'href="' . URL . 'admin/$1"');
        View::addRegexTag("v:admin:src=[\"\'](.*?)[\"\']", 'src="' . URL . 'admin/$1"');
        View::addRegexTag("v:admin:src=[\"\'](.*?)[\"\']", 'src="' . URL . 'vendor/vermal/admin/src/' . '$1"');
        View::addRegexTag("v:admin:public:href=[\"\'](.*?)[\"\']", 'href="' . URL . 'vendor/vermal/admin/src/public/$1"');
        View::addRegexTag("v:admin:public:src=[\"\'](.*?)[\"\']", 'src="' . URL . 'vendor/vermal/admin/src/public/$1"');

        // Add menu tag
        View::addTag('menu', function ($match) {
            $params = View::filterMatch($match);
            return call_user_func_array([MenuRenderer::class, 'menu'], $params);
        });

        // Auth
        View::addTag("auth", function ($match) {
            $params = View::filterMatch($match);
            return '<?php if (' . (Auth::hasRole($params[0]) ? '1' : '0') . '): ?>';
        });
        View::addRegexTag("\@auth", '<?php if (' . (Auth::isLoggedIn() ? '1' : '0') . '): ?>');
        View::addRegexTag("\@elseauth", '<?php else: ?>');
        View::addRegexTag("\@endauth", '<?php endif; ?>');

        // Admin nav
        $frontendAdminNav = \Vermal\View::view("/vendor/vermal/admin/src/App/Defaults/templates/parts/frontendNav", [
			'frontendNavLinks' => App::get('frontendNavLinks')
		], true);
        View::addRegexTag('\@adminNav', $frontendAdminNav);
    }

}
