<?php

namespace Vermal\Admin\Defaults\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation;
use Vermal\App;
use Vermal\Database\Database;

trait Translatable
{
    /**
     * While getting translations store them in this property
     *
     * @var array $storage
     */
    protected $storage = [];

    /**
     * Translatable constructor.
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    // Clear translations
    public function clearTr()
    {
        $this->translations->clear();
    }

    // Get all translations
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * Add translation
     *
     * @param $t
     */
    public function addTranslation($t)
    {
        if (!$this->translations->contains($t)) {
            $this->translations[] = $t;
            $t->setObject($this);
        }
    }

    /**
     * Get translation
     *
     * @param string $locale
     * @param string $field
     * @return mixed
     */
    public function getTr($locale, $field)
    {
//        // Get default value beacuse this is default locale
//        if ($locale == App::get('locale'))
//            return $this->{$field};

        // Get value from storage
        if (isset($this->storage[$locale . '_' . $field]))
            return $this->storage[$locale . '_' . $field];

        /** @var AbstractPersonalTranslation $tr */
        foreach ($this->translations as $tr) {
            if ($tr->getLocale() == $locale && $tr->getField() == $field) {
                $this->storage[$locale . '_' . $field] = $tr->getContent();
                return $tr->getContent();
            }
        }
    }

}
