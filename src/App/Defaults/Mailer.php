<?php

namespace Vermal\Admin\Defaults;


use PHPMailer\PHPMailer\PHPMailer;
use Vermal\App;

class Mailer
{
    /**
     * @var PHPMailer
     */
    private $mail;

    /**
     * @var string $from
     */
    private $from;

    /**
     * Mailer constructor.
     *
     * @param string $username
     * @param string $password
     * @param string $host
     * @param string $port
     * @param string $encryption
     * @param int $SMTPDebug
     */
    public function __construct($username = '', $password = '', $host = '', $port = '', $encryption = 'tls', $SMTPDebug = 0)
    {
        $this->mail = new PHPMailer(App::get('debug'));

        // Auth
        if (!empty($username)) $this->username($username);
        if (!empty($password)) $this->passowrd($password);
        if (!empty($host)) $this->host($host);
        if (!empty($port)) $this->port($port);

        // Set defaults
        $this->mail->SMTPDebug = $SMTPDebug;
        $this->mail->isSMTP();
        $this->mail->SMTPAuth = true;
        $this->mail->SMTPSecure = $encryption;
        $this->mail->CharSet = 'UTF-8';
    }

    /**
     * Set host
     *
     * @param $host
     * @return object $this
     */
    public function host($host)
    {
        $this->mail->Host = $host;
        return $this;
    }

    /**
     * Get host
     *
     * @return string
     */
    public function getHost()
    {
        return $this->mail->Host;
    }

    /**
     * Set username
     *
     * @param $username
     * @return object $this
     */
    public function username($username)
    {
        $this->mail->Username = $username;
        return $this;
    }

    /**
     * Set password
     *
     * @param $password
     * @return object $this
     */
    public function passowrd($password)
    {
        $this->mail->Password = $password;
        return $this;
    }

    /**
     * Set password
     *
     * @param $port
     * @return object $this
     */
    public function port($port)
    {
        $this->mail->Port = $port;
        return $this;
    }

    /**
     * Set parameter manually
     *
     * @param $param
     * @param $value
     */
    public function rawParam($param, $value)
    {
        $this->mail->{$param} = $value;
    }

    /**
     * Set from
     *
     * @param string $address
     * @param string $name
     * @throws \PHPMailer\PHPMailer\Exception
     *
     * @return $this
     */
    public function from($address, $name = '')
    {
        $this->from = $address;
        $this->mail->setFrom($address, $name);
        return $this;
    }

    /**
     * Get from email address
     *
     * @return string
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Add recipient
     *
     * @param string $address
     * @param string $name
     * @return $this
     */
    public function addAddress($address, $name = '')
    {
        $this->mail->addAddress($address, $name);
        return $this;
    }

    /**
     * Reply to
     *
     * @param string $address
     * @param string $name
     * @return $this
     */
    public function addReplyTo($address, $name = '')
    {
        $this->mail->addReplyTo($address, $name);
        return $this;
    }

    /**
     * @param $address
     * @param string $name
     * @return $this
     */
    public function addCC($address, $name = '')
    {
        $this->mail->addCC($address, $name);
        return $this;
    }

    /**
     * @param $address
     * @param string $name
     * @return $this
     */
    public function addBCC($address, $name = '')
    {
        $this->mail->addBCC($address, $name);
        return $this;
    }

    /**
     * Add attachment
     *
     * @param $path
     * @param $name
     * @return $this
     * @throws \PHPMailer\PHPMailer\Exception
     */
    public function addAttachment($path, $name)
    {
        $this->mail->addAttachment($path, $name);
        return $this;
    }

    /**
     * Set subject
     *
     * @param $subject
     * @return $this
     */
    public function subject($subject)
    {
        $this->mail->Subject = $subject;
        return $this;
    }

    /**
     * Set body
     *
     * @param $body
     * @param bool $isHtml
     * @return $this
     */
    public function body($body, $isHtml = true)
    {
        $this->mail->Body = $body;
        $this->mail->isHTML($isHtml);
        return $this;
    }

    /**
     * Set alternative body
     *
     * @param $body
     * @return $this
     */
    public function altBody($body)
    {
        $this->mail->AltBody = $body;
        return $this;
    }

    /**
     * @throws \PHPMailer\PHPMailer\Exception
     */
    public function send()
    {
        return $this->mail->send();
    }


}
