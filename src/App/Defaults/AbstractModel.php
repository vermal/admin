<?php

namespace Vermal\Admin\Defaults;


use Vermal\Admin\Sanitizer;

abstract class AbstractModel
{

	/**
	 * Slugify value
	 *
	 * @param $value
	 * @return string|string[]|null
	 */
	public function slugify($value)
	{
		return Controller::slugify($value);
	}

	/**
	 * Sanitize value
	 *
	 * @param $rules
	 * @param $value
	 * @return mixed
	 */
	public function sanitize($rules, $value)
	{
		return Sanitizer::applyFilter($rules, $value);
	}

	/**
	 * Translate
	 *
	 * @param $key
	 * @return array|mixed
	 */
	public function _($key)
	{
		return Controller::__($key);
	}

	/**
	 * Alert
	 *
	 * @param $msg
	 * @param $type
	 * @param array $aditional
	 */
	public function alert($msg, $type = 'info', $aditional = [])
	{
		Controller::alertStatic($msg, $type, $aditional);
	}

}
