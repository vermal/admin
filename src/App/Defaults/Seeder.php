<?php

namespace Vermal\Admin\Defaults;

use Vermal\Admin\Modules\Auth\Entities\Permission\Permission;
use Vermal\Admin\Modules\Auth\Entities\Resource\Resource;
use Vermal\Admin\Modules\Auth\Entities\Role\Role;
use Vermal\Admin\Modules\Auth\Entities\User\User;
use Vermal\Admin\Modules\Menu\Entities\Menu;
use Vermal\Admin\Modules\Menu\Entities\MenuItem;
use Vermal\Admin\Modules\Multimedia\Entities\MultimediaFolder;
use Vermal\Admin\Modules\Settings\Entities\Setting;
use Vermal\Database\Database;
use Vermal\Database\Entity;

class Seeder
{

    /**
     * Main function
     */
    public static function main()
    {
        self::resources();
        self::roles();
        self::users();
        self::menus();
        self::multimediaFolders();
        self::settings();

        // Finally save all datas
        Database::flush();
    }

	/**
	 * Add default roles
	 */
	private static function roles()
	{
		// Create developer role
		/** @var $role Role */
		$role = Entity::getEntity('Role');

		$role->name = "developer";
		$role->displayName = "Developer";
		$role->authority = 0;

		// Get permission
		$permissions = Database::Model('Permission')->get();

		foreach ($permissions as $permission) {
			$role->addPermission($permission);
		}

		// Add role to permissions
		Database::saveAndFlush($role);
	}

    /**
     * Add all default resources
     */
    private static function resources()
    {
        self::addResource("Dashboard", 1);
        self::addResource("User", 1);
        self::addResource("Resource", 0);
        self::addResource("Role", 1);
        self::addResource("Menu", 1);
        self::addResource("Multimedia", 1);
		self::addResource("Content", 1);
        self::addResource("Article category", 1);
        self::addResource("Article", 1);
		self::addResource("Text", 1);
		self::addResource("Settings", 1);
		self::addResource("Localization Settings", 1);
        self::addResource("PDF Templates", 1);
        self::addResource("Form", 1);
		self::addResource("Faq", 1);
		self::addResource("Seo", 1);

        Database::flush();
    }

	/**
	 * Add resource
	 *
	 * @param $name
	 * @param $authority
	 */
	private static function addResource($name, $authority)
	{
		$resource = Entity::getEntity('Resource');
		$resource->name = Controller::slugify($name);
		$resource->displayName = $name;
		$resource->authority = $authority;

		Database::saveAndFlush($resource);
		self::addPermission($resource, 1, 1,1, 1);
	}

	/**
	 * Add permission
	 *
	 * @param $resource
	 * @param $c
	 * @param $r
	 * @param $u
	 * @param $d
	 */
	private static function addPermission($resource, $c, $r, $u, $d)
	{
		$permission = Entity::getEntity('Permission');
		$permission->resource = $resource;
		$permission->create = $c;
		$permission->read = $r;
		$permission->update = $u;
		$permission->delete = $d;

		Database::save($permission);
	}

    /**
     * Add developer account
     */
    private static function users()
    {
        $user = Entity::getEntity('User');

        $user->email = "admin@admin.com";
        $user->password = password_hash("password", PASSWORD_DEFAULT);
        $user->name = "Developer";
        $user->phone = "09000000";
        $user->role = Database::Model('Role')->where('r.name', 'developer')->first();

        Database::saveAndFlush($user);
    }

    /**
     * Add menus
     */
    private static function menus()
    {
        $menu = Entity::getEntity('Menu');

        $menu->name = "admin";
        $menu->displayName = "Admin";
        $menu->iconClass = "fas fa-home";

        $menu = Database::saveAndFlush($menu);
        self::menuItems($menu);
    }

    /**
     * Add menu items
     *
     * @param $menu
     */
    private static function menuItems($menu)
    {
		self::addMenuItem($menu, "__resources.dashboard", "static", '#', "", "", "_self", "fas fa-home", 'dashboard', 0);

		// Settings
        self::addMenuItem($menu, "__resources.settings", "static", "#", "", "", "_self", "fa fa-cog", 'settings', 7, [
			self::addMenuItem($menu, "__resources.settings", "dynamic", null, "admin.settings.index", "", "_self", "fa fa-cogs", 'settings', 0),
			self::addMenuItem($menu, "__resources.localizationSettings", "dynamic", null, "admin.localization-settings.index", "", "_self", "fa fas fa-globe", 'settings', 1),
			self::addMenuItem($menu, "__resources.user", "dynamic", null, "admin.user.index", "", "_self", "fas fa-users", 'user', 2),
			self::addMenuItem($menu, "__resources.role", "dynamic", null, "admin.role.index", "", "_self", "fas fa-lock", 'role', 3),
			self::addMenuItem($menu, "__menuItem.resource", "dynamic", null, "admin.resource.index", "", "_self", "fab fa-sourcetree", 'resource', 4),
			self::addMenuItem($menu, "__resources.menu_builder", "dynamic", null, "admin.menu.index", "", "_self", "fas fa-stream", 'menu', 5)
		]);

        // Multimedia
        self::addMenuItem($menu, "__resources.multimedia", "dynamic", null, "admin.multimedia.index", "", "_self", "fas fa-image", 'multimedia', 6);

        // Faq
		self::addMenuItem($menu, "__resources.faq", "dynamic", null, "admin.faq.index", "", "_self", "fas fa-question", 'faq', 6);

        // Content
        self::addMenuItem($menu, "__resources.content", "static", "#", "", "", "_self", "fa fa-table", 'content', 1, [
			self::addMenuItem($menu, "__resources.article", "dynamic", null, "admin.article.index", "", "_self", "fa fa-newspaper", 'article', 0),
			self::addMenuItem($menu, "__resources.article_category", "dynamic", null, "admin.article-category.index", "", "_self", "fa fa-tree", 'article-category', 1),
			self::addMenuItem($menu, "__resources.text", "dynamic", null, "admin.text.index", "", "_self", "fa fa-paragraph", 'text', 2),
			self::addMenuItem($menu, "__resources.seo", "dynamic", null, "admin.seo.index", "", "_self", "fas fa-eye", 'text', 3),
		]);

        // Form builder
        self::addMenuItem($menu, "__resources.form_builder", "dynamic", null, "admin.form.index", "", "_self", "fab fa-wpforms", 'form', 5);

        // PDF Templates
        self::addMenuItem($menu, "__resources.pdftemplate", "dynamic", null, "admin.pdftemplate.index", "", "_self", "fas fa-file-pdf", 'pdf-templates', 6);
        Database::flush();
    }

	/**
	 * Add menu item
	 *
	 * @param $menu
	 * @param $title
	 * @param $type
	 * @param $url
	 * @param $route
	 * @param $parameters
	 * @param $target
	 * @param $icon
	 * @param $resource
	 * @param $sort
	 * @param $children
	 *
	 * @return MenuItem
	 */
	private static function addMenuItem($menu, $title, $type, $url, $route, $parameters, $target, $icon, $resource, $sort, $children = [])
	{
		/** @var MenuItem $item */
		$item = Entity::getEntity('MenuItem');

		/** @var Resource $resource */
		$resource = Database::Model('Resource')->where('r.name', $resource)->first();
		if (empty($resource)) $resource = null;
		else $resource = $resource->getId();

		$item->menu = $menu;
		$item->title = $title;
		$item->type = $type;
		$item->url = $url;
		$item->route = $route;
		$item->parameters = $parameters;
		$item->target = $target;
		$item->iconClass = $icon;
		$item->note = $resource;
		$item->order = $sort;

		foreach ($children as $child) {
			/** @var $child MenuItem */
			$child->parent = $item;
			$item->addChild($child);
			Database::save($child);
		}

		return Database::save($item);
	}

    /**
     * Create folders for multimedia
     */
    private static function multimediaFolders()
    {
        $folder = Entity::getEntity('MultimediaFolder');
        $folder->name = "Root";
        $folder->path = "/";

        Database::saveAndFlush($folder);

        $folder = Entity::getEntity('MultimediaFolder');
        $folder->name = "posts";
        $folder->path = "/posts";

        Database::saveAndFlush($folder);
    }

    private static function settings()
    {
        $setting = Entity::getEntity('Setting');

        // Languages
        $setting->languages = ['sk' => 'SK', 'en' => 'EN'];

//        // Encryption
//        $setting->mailer_encryption = 'ssl';
//        $setting->mailer_port = 465;

        // Save settings
        Database::saveAndFlush($setting);
    }

}
