@extends('layout')

@section('body')

    <div class="container">
        @form($formName)
            <div class="col-12 card shadow-lg m-b-30 p-4">

                @if(isset($fieldsBefore))
                    <div class="row">
                        @foreach($fieldsBefore as $field)
                            @label("{$field}")
                            @control("{$field}")
                        @endforeach
                    </div>
                @endif

                <ul class="nav nav-tabs tab-line" id="myTab" role="tablist">
                    @foreach($languages as $lang)
                        <li class="nav-item">
                            <a class="nav-link {{ $loop->first ? "show active" : "" }}" id="tab-{{ $lang }}" data-toggle="tab" href="#{{ $lang }}" role="tab" aria-controls="{{ $lang }}" aria-selected="true">
                                {{ strtoupper($lang) }}
                            </a>
                        </li>
                    @endforeach
                </ul>

                <div class="tab-content pt-4" id="myTabContent">
                    @foreach($languages as $lang)

                        <div class="tab-pane row {{ $loop->first ? "show active" : "" }}" id="{{ $lang }}" role="tabpanel" aria-labelledby="tab-{{ $lang }}">
                            @foreach($fields as $field)
                                @label("{$lang}_{$field}")
                                @control("{$lang}_{$field}")
                            @endforeach
                        </div>

                    @endforeach
                </div>

                @if(isset($fieldsAfter))
                    <div class="row">
                        @foreach($fieldsAfter as $field)
                            @label("{$field}")
                            @control("{$field}")
                        @endforeach
                    </div>
                @endif

                <div class="text-right mt-4">
                    @control("submit")
                </div>
            </div>
        @endform
    </div>

    <script>
        $(document).ready(function() {
            $('.nav-tabs .nav-link').click(function() {
                setTimeout(function() {
                    initPlugins();
                }, 10);
            });
        });
    </script>

@endsection
