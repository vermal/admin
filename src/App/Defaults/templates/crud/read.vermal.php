@extends('layout')

@section('body')

    <div class="card shadow-lg m-b-30">
        <div class="card-body">
            {!! $datagrid !!}
        </div>
    </div>

@endsection
