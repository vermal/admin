@extends('layout')

@section('body')

    <main class="admin-main  bg-pattern">
        <div class="container">
            <div class="row m-h-100 ">
                <div class="col-md-8 col-lg-4  m-auto">
                    <div class="card shadow-lg p-t-20 p-b-20">
                        <div class="card-body text-center">
                            <img width="200" alt="image" v:admin:public:src="assets/img/404.svg">
                            <h1 class="display-1 fw-600 font-secondary">404</h1>
                            <h5>@__(error.404.heading)</h5>
                            <p class="opacity-75">@__(error.404.text)</p>
                            <div class="p-t-10">
                                <a href="{{ \Vermal\Router::link('admin.dashboard')  }}" class="btn btn-lg btn-primary">@__(error.404.button)</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection
