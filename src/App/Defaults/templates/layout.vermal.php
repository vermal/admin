<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" name="viewport">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <title>Vermal - CMS</title>
    <link rel="icon" type="image/x-icon" v:admin:public:href="assets/img/logo.png"/>
    <link rel="icon" v:admin:public:href="assets/img/logo.png" type="image/png" sizes="16x16">
    <link rel="stylesheet" v:admin:public:href="assets/vendor/pace/pace.css">
    <script v:admin:public:src="assets/vendor/pace/pace.min.js"></script>

    <!--jquery-->
    <script v:admin:public:src="assets/vendor/jquery/jquery.min.js"></script>
    <script v:admin:public:src="js/formBuilder.jquery.js"></script>

    <!--vendors-->
    <link rel="stylesheet" type="text/css" v:admin:public:href="assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">
    <link rel="stylesheet" type="text/css" v:admin:public:href="assets/vendor/jquery-scrollbar/jquery.scrollbar.css">
    <link rel="stylesheet" v:admin:public:href="assets/vendor/select2/css/select2.min.css">
    <link rel="stylesheet" v:admin:public:href="assets/vendor/jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet" v:admin:public:href="assets/vendor/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" v:admin:public:href="assets/vendor/timepicker/bootstrap-timepicker.min.css">
    <link href="https://fonts.googleapis.com/css?family=Hind+Vadodara:400,500,600" rel="stylesheet">
    <link rel="stylesheet" v:admin:public:href="assets/fonts/jost/jost.css">

    <!--Material Icons-->
    <link rel="stylesheet" type="text/css" v:admin:public:href="assets/fonts/materialdesignicons/materialdesignicons.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">

    <!--Bootstrap + atmos Admin CSS-->
    <link rel="stylesheet" type="text/css" v:admin:public:href="assets/css/atmos.css">

    <!-- Additional library for page -->
    @yield('head')
    <link rel="stylesheet" v:admin:public:href="assets/vendor/dropzone/dropzone.css">
    <link rel="stylesheet" v:admin:public:href="assets/vendor/trumbowyg/ui/trumbowyg.css">
    <link rel="stylesheet" v:admin:public:href="assets/vendor/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" v:admin:public:href="assets/vendor/timepicker/bootstrap-timepicker.min.css">


    <link rel="stylesheet" type="text/css" v:admin:public:href="css/app.min.css">

    <style>
        button:focus {
            outline: none !important;
        }
    </style>

</head>
<!--body with default sidebar pinned -->
<body class="@auth sidebar-pinned @elseauth jumbo-page @endauth
    @if(isset($jumboPage))
        jumbo-page
    @endif
    @yield("bodyClass")
">


    @if(!isset($jumboPage))
        @auth
            @include("parts/menu", [])

            <main class="admin-main">
                @include("parts/header", [])
                <section class="admin-content">
                    <div>
                        @include('parts/alert', ['alerts' => $alerts])

                        <div class="bg-dark m-b-30 bg-stars">
                            <div class="container{{ (isset($isFluid) && $isFluid ? '-fluid' : '') }}">
                                <div class="row p-t-40 p-b-90">
                                    <div class="col-md-8 text-white">
                                        <h1>{{ array_reverse($breadcrumb)[0]['title'] }}</h1>
                                        <p class="opacity-75">
                                            @foreach($breadcrumb as $item)
                                                <a href="{{ $item['link'] }}">{{ $item['title'] }}</a>
                                                @if($loop->remaining)
                                                    /
                                                @endif
                                            @endforeach
                                        </p>
                                    </div>
                                    @if(!isset($showHeaderActions) || $showHeaderActions)
                                        <div class="col-md-4 text-right">
                                            @if(isset($canCreate) && $canCreate)
                                                <a href="{{ routerLink(str_replace('index', 'create', $canCreate)) }}" class="btn m-b-15 ml-2 mr-2 btn-lg btn-rounded-circle btn-info">
                                                    <i class="mdi mdi-plus"></i>
                                                </a>
                                            @endif
                                            @if(isset($canGoBack) && $canGoBack)
                                                <a href="{{ routerLink($canGoBack) }}" class="btn btn-info">
                                                    <i class="fas fa-arrow-left mr-2"></i>
                                                    Back
                                                </a>
                                                <button id="saveForm" class="btn btn-success ml-2">
                                                    <i class="fas fa-save mr-2"></i>
                                                    Save
                                                </button>
                                            @endif
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="container{{ (isset($isFluid) && $isFluid ? '-fluid' : '') }} pull-up">
                            @yield('body')
                        </div>
                    </div>
                </section>
            </main>

            {{--@include("parts/search", [])--}}
        @elseauth
            @yield('body')
        @endauth
    @else
        @yield('body')
    @endif

    @yield('footer')
    @php(do_action('admin_layout_footer'))

    <div class="modal fade" id="modalConfirmationDatagridDelete" data-backdrop="static"  tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog  modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-body">
                    <div class="px-3 pt-3 text-center">
                        <div class="event-type warning">
                            <div class="event-indicator ">
                                <i class="mdi mdi-exclamation text-white" style="font-size: 60px"></i>
                            </div>
                        </div>
                        <h3 class="pt-3">@__(multimedia.confirm.heading)</h3>
                        <p class="text-muted">@__(multimedia.confirm.text)</p>
                    </div>
                </div>
                <div class="modal-footer ">
                    <a href="#" id="okay" class="btn btn-warning">@__(global.modal.okay)</a>
                    <a href="#" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">@__(global.modal.close)</a>
                </div>
            </div>
        </div>
    </div>

    <script v:admin:public:src="assets/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script v:admin:public:src="assets/vendor/popper/popper.js"></script>
    <script v:admin:public:src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script v:admin:public:src="assets/vendor/select2/js/select2.full.min.js"></script>
    <script v:admin:public:src="assets/vendor/select2/js/select2.sortable.js"></script>
    <script v:admin:public:src="assets/vendor/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <script v:admin:public:src="assets/vendor/listjs/listjs.min.js"></script>
    <script v:admin:public:src="assets/vendor/moment/moment.min.js"></script>
    <script v:admin:public:src="assets/vendor/daterangepicker/daterangepicker.js"></script>
    <script v:admin:public:src="assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script v:admin:public:src="assets/vendor/bootstrap-notify/bootstrap-notify.min.js"></script>
    <script v:admin:public:src="assets/js/atmos.min.js"></script>
    <script v:admin:public:src="assets/vendor/dropzone/dropzone.js"></script>
    <script v:admin:public:src="assets/vendor/trumbowyg/trumbowyg.js"></script>
    <script v:admin:public:src="assets/vendor/trumbowyg/plugins/upload/trumbowyg.upload.js"></script>
    <script v:admin:public:src="assets/vendor/blockui/jquery.blockUI.js"></script>
    <script v:admin:public:src="assets/js/blockui-data.js"></script>

    <script v:admin:public:src="assets/vendor/daterangepicker/daterangepicker.js"></script>
    <script v:admin:public:src="assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script v:admin:public:src="assets/vendor/timepicker/bootstrap-timepicker.min.js"></script>

	<script v:admin:public:src="assets/vendor/DataTables/datatables.js"></script>
    <script v:admin:public:src="assets/vendor/DataTables/DataTables-1.10.18/js/dataTables.buttons.min.js"></script>
    <script v:admin:public:src="assets/vendor/DataTables/DataTables-1.10.18/js/dataTables.flash.min.js"></script>
    <script v:admin:public:src="assets/vendor/DataTables/DataTables-1.10.18/js/dataTables.jszip.min.js"></script>
    <script v:admin:public:src="assets/vendor/DataTables/DataTables-1.10.18/js/dataTables.pdfmake.min.js"></script>
    <script v:admin:public:src="assets/vendor/DataTables/DataTables-1.10.18/js/dataTables.vfsfont.min.js"></script>
    <script v:admin:public:src="assets/vendor/DataTables/DataTables-1.10.18/js/dataTables.html5.min.js"></script>

    <script v:vermal:src="js/vermal.js"></script>
    <script v:admin:public:src="js/domenu.js"></script>
    <script v:admin:public:src="js/app.js"></script>

    <script>
        function initPlugins() {
            $('.text-editor').trumbowyg({
                btns: [
                    ['viewHTML'],
                    ['undo', 'redo'],
                    ['formatting'],
                    ['strong', 'em', 'del', 'underline'],
                    ['superscript', 'subscript'],
                    ['link'],
                    ['upload'],
                    ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                    ['unorderedList', 'orderedList'],
                    ['preformatted'],
                    ['horizontalRule'],
                    ['fullscreen'],
                    ['removeformat']
                ],
                removeformatPasted: true,
                resetCss: true,
                semantic: {
                    'b': 'strong',
                    'i': 'em',
                    's': 'del',
                    'strike': 'del',
                    'div': 'p'
                },
                plugins: {
                    upload: {
                        serverPath: '{{ routerLink('admin.text-editor.upload') }}',
                        fileFieldName: 'image'
                    }
                }
            });

            $(".js-select2").select2();
            $(".js-select2.allow-tags").select2({
                tags: true
            });
            $(".js-select2.search").select2({
                ajax: {
                    url: '{{ routerLink('admin.select2') }}',
                    method: 'POST',
                    dataType: 'json',
                    data: function(params) {
                        var query = {
                            query: params.term,
                            page: params.page || 1,
                            controller: $(this).data('controller'),
                            method: $(this).data('method'),
                            fields: $(this).data('fields')
                        };
                        return query;
                    }
                }
            });
            // TODO: sortable select2
            // $("ul.select2-selection__rendered").sortable({
            //     containment: 'parent'
            // });
            $('#saveForm').click(function() {
                $('form').each(function() {
                    if (typeof $(this).attr('id') !== "undefined") {
                        let name = $(this).attr('id').split('form-')[1];
                        $('#submit_' + name).click();
                    }
                });
            });
            $('input.date').datepicker({
                format: 'dd.mm.yyyy'
            });
            $('input.time').timepicker({
                showInputs: false,
                showMeridian: false
            });
            $('input.daterange').daterangepicker({
                locale: { format: 'DD.MM.YYYY' },
                autoUpdateInput: false
            }, function(start_date, end_date) {
                this.element.val(start_date.format('DD.MM.YYYY')+' - '+end_date.format('DD.MM.YYYY'));
                this.element.trigger('change');
            });
            $('.quickcopy').click(function(e) {
                let $tempInput = $("<input>");
                $("body").append($tempInput);
                $tempInput.val($(e.target).text()).select();
                document.execCommand("copy");
                $tempInput.remove();
            });

            $('.table-responsive table').DataTable({
	            paging: false,
	            searching: false,
	            ordering:  false,
	            "bInfo" : false,
                dom: 'Bfrtip',
                buttons: [
                    'excel', 'pdf'
                ],
	            responsive: {
		            details: false
	            },
                initComplete: function() {
                    $('.table-responsive').find('.row:first').remove();
                    $('.table-responsive').find('.row').removeClass('row');
                    $('.table-responsive div').removeClass(function (index, className) {
                        return (className.match (/(^|\s)col-\S+/g) || []).join(' ');
                    });
                    $('.table-responsive .dt-buttons button').addClass('btn btn-outline-secondary btn-sm mb-2');
                    @if(method_exists($presenter, 'dataGridExportExcelHandler'))
                        $('.dt-button.buttons-excel').remove();
                        $('.dt-buttons').append($('<a>')
                                .attr('href', "{{ routerLink('admin.datagrid-export', ['c' => str_replace('\\', '__', get_class($presenter)), 'type' => 'excel'], true, '', true) }}")
                                .addClass('btn btn-outline-secondary btn-sm mb-2 do-excel-custom').html('Excel'));
                    @endif
	            }
			});

          let datagrid = $('table.dataTable');
          if (datagrid.length > 0) {
            datagrid.find('a.action').click(function(e) {
              if ($(this).attr('href').indexOf('delete') !== -1) {
                e.preventDefault();
                $('#modalConfirmationDatagridDelete #okay').attr('href', $(this).attr('href'));
                $('#modalConfirmationDatagridDelete').modal()
              }
            });
          }

        }
        $(document).ready(function() {
            initPlugins();
        });
    </script>

</body>
</html>
