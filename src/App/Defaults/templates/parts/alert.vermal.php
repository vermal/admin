<div class="alert-container">
    @foreach($alerts as $key => $alert)
        <div id="notify-alert-{!! $key !!}" class="alert alert-{{ $alert["type"] }} alert-dismissible fade show">
            {!! $alert["msg"] !!}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
        </div>
    @endforeach
</div>
