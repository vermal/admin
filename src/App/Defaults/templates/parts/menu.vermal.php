<!--sidebar Begins-->
<aside class="admin-sidebar">
    <div class="admin-sidebar-brand">
        <!-- begin sidebar branding-->
        <img class="admin-brand-logo" v:admin:public:src="assets/img/logo.svg" width="40" alt="atmos Logo">
        <span class="admin-brand-content"><a v:href="vendor/vermal/admin/src/public/index.html">vermal</a></span>
        <!-- end sidebar branding-->
        <div class="ml-auto">
            <!-- sidebar pin-->
            <a v:href="vendor/vermal/admin/src/public/#" class="admin-pin-sidebar btn-ghost btn btn-rounded-circle"></a>
            <!-- sidebar close for mobile device-->
            <a v:href="vendor/vermal/admin/src/public/#" class="admin-close-sidebar"></a>
        </div>
    </div>
    <div class="admin-sidebar-wrapper js-scrollbar">
        @menu('admin', 'atmos')
    </div>

</aside>
<!--sidebar Ends-->
