@auth
    @if(!isset($_GET['liveeditor']))
        <style type="text/css">
            #frontend-admin-nav {
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                background: rgba(255,255,255,0.8);
                padding: 10px;
                text-align: right;
                z-index: 999999999999;
            }
            #frontend-admin-nav ul {
                padding: 0;
                margin: 0;
            }
            #frontend-admin-nav ul li {
                display: inline-block;
            }
            #frontend-admin-nav ul li a {
                color: #000;
                padding: 0 10px;
                text-decoration: none;
                transition: .3s;
                font-size: 14px;
            }
            #frontend-admin-nav ul li a:hover {
                opacity: .8;
            }
        </style>
        <div id="frontend-admin-nav">
            <ul>
                <li>
                    <a href="{{ currentLink() . (!isset($_GET['edit']) ? '?edit=true' : '') }}">Editácia obsahu</a>
                </li>
                @if(!empty($frontendNavLinks))
                    @foreach($frontendNavLinks as $title => $link)
                        <li>
                            <a href="{{ $link }}">{{ $title }}</a>
                        </li>
                    @endforeach
                @endif
                <li>
                    <a href="{{ routerLink('admin.dashboard') }}">Admin</a>
                </li>
                <li>
                    <a href="{{ routerLink('admin.logout') }}" style="color: red; font-weight: bold">Odhlásiť</a>
                </li>
            </ul>
        </div>
    @endif
@endauth
