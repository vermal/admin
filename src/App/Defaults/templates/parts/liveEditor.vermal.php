@auth
    @if(!isset($_GET['liveeditor']))
        <style>
            .content-edit-title, .content-edit-te {
                border: 2px dashed red;
            }
            .content-edit-title:focus {
                outline: none;
            }
            #content-editor-modal {
                position: fixed;
                width: 100%;
                height: 100vh;
                background-color: rgba(0,0,0,.8);
                top: 0;
                left: 0;
                z-index: 9999999999999;
            }
            #content-editor-modal .inner {
                position: relative;
                width: 60%;
                height: auto;
                top: 50%;
                margin: 0 auto;
                margin-top: -200px;
                background-color: #fff;
                padding: 10px 20px;
                box-shadow: 0 0 30px rgba(0,0,0,.15);
                border-radius: 4px;
            }
            #content-editor-modal .form-control {
                border-radius: 3px;
                margin-bottom: 10px;
            }
            #content-editor-modal .close-modal, #content-editor-modal .save-value {
                transition: .2s;
                border-radius: 3px;
            }
        </style>
        <script type="text/javascript">
            $('.content-edit-title, .content-edit-te').click(function(e) {
                e.preventDefault();
            });
            $('.content-edit-title').click(function(e) {
                e.preventDefault();
                let value = $(this).html();
                let id = $(this).data('id');
                //saveValue('title', value, id);
                createModal('title', id, value);
            });
            $('.content-edit-te').click(function(e) {
                e.preventDefault();
                let value = $(this).html();
                let id = $(this).data('id');
                createModal('te', id, value);
            });

            /**
             * Create modal
             *
             * @param type
             * @param id
             * @param value
             */
            function createModal(type, id, value) {
                let modal = $('#content-editor-modal');
                if (modal.length > 0) {
                    // Show modal
                    modal.fadeIn(150);
                    modal.find('.content-input').hide();
                    modal.find('.content-' + type + '-wrapper').show();
                    console.log(type);

                    // Set default value
                    if (type === 'te') {
                        modal.find('#content-editor').trumbowyg('html', value);
                    } else {
                        modal.find('.content-title').val(value);
                    }

                    // Set id
                    modal.attr('data-id', id);
                } else {

                    let te = $('<div class="content-input content-te-wrapper">').append(
                        $('<textarea id="content-editor" class="editor form-control content-te">').val(value)
                    );
                    let title = $('<input class="form-control content-input content-title content-title-wrapper">').val(value);

                    modal = $('<div id="content-editor-modal" data-id="' + id + '" data-type="' + type + '">').append(
                        $('<div class="inner">').append(
                            $('<div class="form-group">').append(
                                    title, te,
                                $('<div style="text-align: right">').append(
                                    $('<button class="btn btn-outline-secondary close-modal mr-2">').text('Zavrieť'),
                                    $('<button class="btn btn-primary save-value">').text('Uložiť')
                                )
                            )
                        )
                    );
                    modal.find('.content-input').hide();
                    modal.find('.content-' + type + '-wrapper').show();

                    $('body').append(modal);
                    modal.find('.close-modal').click(function() {
                        modal.fadeOut(150);
                    });
                    modal.find('.save-value').click(function() {
                        let type = modal.attr('data-type');
                        let value = modal.find('.content-' + type).val();
                        let id = modal.attr('data-id');
                        saveValue(type, value, id);

                        if (type === 'title')
                            $('.content-edit-title[data-id="' + id + '"]').html(value);
                        else
                            $('.content-edit-te[data-id="' + id + '"]').html(value);
                        modal.fadeOut(150);
                    });
                    $('#content-editor').trumbowyg({
                        removeformatPasted: true,
                        resetCss: true,
                        semantic: {
                            'b': 'strong',
                            'i': 'em',
                            's': 'del',
                            'strike': 'del',
                            'div': 'p'
                        }
                    });
                }
            }

            function saveValue(type, value, id) {
                $.ajax({
                    url: "{{ routerLink('admin.editSingle') }}",
                    method: 'POST',
                    data: {
                        locale: "{{ \Vermal\Router::$params['locale'] }}",
                        type: type,
                        value: value,
                        id: id
                    }
                });
            }
        </script>
    @endif
@endauth
