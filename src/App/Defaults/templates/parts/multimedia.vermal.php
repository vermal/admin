<div class="file d-none" id="image-template-{{ $name }}" style="flex: 0 0 120px; margin-right: 20px; margin-bottom: 20px; max-width: 120px">
    <div class="card overflow-hidden">
        <div class="card-body p-0">
            <a target="_blank" class="image border-bottom border-strong d-block">
                <input type="checkbox" class="d-none" name="{{ $name }}{{ (isset($multiple) && $multiple) ? '[]' : '' }}">
                <img src="" alt="" style="width: 100%; height: 100px; object-fit: cover">
            </a>
        </div>
    </div>
</div>

<div class="shadow-sm rounded p-3 overflow-auto images-wrapper" id="images-wrapper-{{ $name }}" style="max-height: 500px">
    <input type="hidden" name="main_image_{{ $name }}" id="main-image-{{ $name }}">
    <div class="inner d-flex flex-wrap {{ (isset($multiple) && $multiple) ? 'multiple' : '' }}">

    </div>
    <div class="text-center">
        <a class="btn btn-outline-primary mt-4" id="load-more-images-{{ $name }}" data-page="1">Load more</a>
    </div>
    <div class="mt-4" style="display: none" id="{{ $name }}-dropzone">
        <div class="dz-message">
            <h1 class="display-4">
                <i class=" mdi mdi-progress-upload"></i>
            </h1>
            Drop files here or click to upload.
        </div>
    </div>
</div>


<script>
    $(function() {

        let selectedImages = [];
        let mainImage = null;

        /**
         * get files from database
         *
         * @param page
         * @param cb
         */
        function getFiles(page, cb = null) {
            // Loading animation
            $("#images-wrapper-{{ $name }}").block({
                message:'<div class="spinner-border-sm spinner-border text-dark " role="status">\n' +
                    '  <span class="sr-only">Loading...</span>\n' +
                    '</div> <div class="text-muted">Loading</div>'
            });

            // Get new images
            $.get('{{ routerLink('admin.multimedia.images', ['folder' => (!isset($folder) ? 1 : $folder)])  }}/' + page, function(res) {
                let obj = JSON.parse(res);
                let images = obj.items;

                // Check if there are more items
                let loadMore = $('#load-more-images-{{ $name }}');
                if (obj.hasMore === true) {
                    loadMore.data('page', page + 1);
                } else {
                    loadMore.parent().remove();
                }

                // Do nothing if empty response
                if (typeof images === 'undefined') {
                    // Remove animation
                    $('#images-wrapper-{{ $name }}').unblock();
                    return;
                }

                // Create new items
                let imagesWrapper = $('#images-wrapper-{{ $name }} .inner');
                let isMultiple = imagesWrapper.hasClass('multiple');
                for (let i = 0; i < images.length; i++) {
                    let image = images[i];
                    let template = $('#image-template-{{ $name }}').clone();

                    // Edit template
                    template.removeClass('d-none');
                    template.find('img').attr('src', image.mini);
                    template.find('a').data('id', image.id).attr('data-id', image.id);
                    template.find('input').val(image.id);

                    // Add image to wrapper
                    imagesWrapper.append(template);

                    if (selectedImages.indexOf(image.id) !== -1) {
                        checkImage(image.id);
                    }

                    // Add event for images
                    template.find('a').on('click', function(e) {
                        if (!$(this).hasClass('checked')) {
                            // Image is not selected at all
                            let imagesWrapper = $(this).closest('#images-wrapper-{{ $name }}');
                            let input = $(this).find('input');
                            if (isMultiple) {
                                if (e.shiftKey) {
                                    $(this).closest('#images-wrapper-{{ $name }}').find('.image.main').removeClass('main');
                                    $(this).addClass('main');
                                    $('#main-image').val(input.val());
                                }
                            } else {
                                imagesWrapper.find('.image.checked').removeClass('checked')
                                    .find('input').prop('checked', false);
                                $('#main-image').val(input.val());
                            }
                            $(this).find('input').prop('checked', true)
                                .parent().addClass('checked');
                        } else if ($(this).hasClass('checked') && !$(this).hasClass('main') && e.shiftKey) {
                            // Image is selected but not main
                            $(this).closest('#images-wrapper-{{ $name }}').find('.image.main').removeClass('main');
                            $(this).addClass('main');
                            $('#main-image-{{ $name }}').val($(this).find('input').val());
                        } else {
                            // Image is selected
                            $(this).removeClass('checked');
                            $(this).removeClass('main');
                            $(this).find('input').prop('checked', false);
                        }
                    });
                }

                // Remove animation
                $('#images-wrapper-{{ $name }}').unblock();

                // Run callback
                if (cb !== null) {
                    cb();
                }
            });
        }

        /**
         * Check new image
         *
         * @param id
         * @param el
         */
        function checkImage(id, el = null) {
            let imagesWrapper = $('#images-wrapper-{{ $name }} .inner');
            let isMultiple = imagesWrapper.hasClass('multiple');

            // Get image
            let img;
            if (el === null) {
                img = $('#images-wrapper-{{ $name }} .image[data-id="' + id + '"]');
            } else {
                img = el;
            }

            // Uncheck all images
            if (!isMultiple) {
                imagesWrapper.find('.image.checked').removeClass('checked').removeClass('main')
                    .find('input').prop('checked', false);
            }

            // Check new image
            img.addClass('checked');
            if (id === mainImage) {
                img.addClass('main');
            }
            console.log(img);
            img.find('input').prop('checked', true);
        }

        $(document).ready(function() {
            // Selected images
            selectedImages = {{ (!empty($selected) ? json_encode($selected) : '[]') }};

            getFiles(1);
            let loadMoreImages = $('#load-more-images-{{ $name }}');
            loadMoreImages.click(function() {
                getFiles($(this).data('page'));
            });

            // Init dropzone
            let dropzone = new Dropzone("#{{ $name }}-dropzone", {
                url: "{{ routerLink('admin.multimedia.upload') }}",
                params: {
                    folder_id: "{{ (!isset($folder) ? 1 : $folder) }}"
                }
            });
            $('#{{ $name }}-dropzone').addClass('dropzone').show();

            // Save all selected images
            dropzone.on('drop', function() {
                selectedImages = [];
                $('#images-wrapper-{{ $name }} .image').each(function() {
                    let input = $(this).find('input');
                    if (input.prop('checked')) {
                        selectedImages.push(input.val());
                    }
                });
                mainImage = $('#main-image-{{ $name }}').val();
            });

            // Reload images
            dropzone.on('success', function() {
                for (let x = 1; x <= loadMoreImages.data('page'); x++) {
                    $('#images-wrapper-{{ $name }} .inner').html('');
                    getFiles(x, function() {
                        for (let i = 0; i < selectedImages.length; i++) {
                            let id = selectedImages[i];
                            checkImage(id);
                        }
                        mainImage = null;
                        if (x === loadMoreImages.data('page'))
                            selectedImages = [];
                        if (x === 1)
                            checkImage(0, $('#images-wrapper-{{ $name }} .inner .image:first'));

                    });
                }
            });
        });
    })
</script>
