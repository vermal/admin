<?php

namespace Vermal\Admin\Defaults;
use Vermal\App;
use Vermal\Ecommerce\Defaults\Currencies;
use Vermal\Admin\View;

/**
 * Class Controller
 * @package Vermal\Admin\Defaults
 */
trait ControllerExtension
{

	public $settings = [];
	public $settingsLocalized = [];
	public $currency;
	public static $currencyStatic;
	public $currencySymbol;
	public static $langs;


	public function settingsSetup()
	{
		Controller::getSettings($this->locale);
		$this->settings = Controller::$appSettings;
		$this->settingsLocalized = Controller::$appLocalizedSettings;
		$this->currency = !empty($this->settingsLocalized->currency) ? $this->settingsLocalized->currency : App::get('currency');
		self::$currencyStatic = $this->currency;
		$this->currencySymbol = Currencies::getSymbol($this->currency);
		Controller::$langs = $this->settings->languages;


		View::setVariable('currency', $this->currency);
		View::setVariable('currencySymbol', $this->currencySymbol);
	}

}
