<?php

namespace Vermal\Admin\Defaults;

use Vermal\Admin\Modules\Auth\Auth;
use Vermal\Admin\Sanitizer;
use Vermal\Admin\View;
use Vermal\Database\Database;
use Vermal\Router;

class Texts
{

    /**
     * @var array $texts
     */
    private static $texts = [];

    public function command()
    {
        View::addTag('text', [self::class, 'text'], true, false);
        View::addTag('liveEditor', [$this, 'liveEditor'], false, true);
        View::addTag('liveEditorResources', [$this, 'liveEditorResources'], false, true);
    }

    /**
     * Get text
     *
     * @param $id
     * @param $type
     * @param string $output
     *
     * @return string
     */
    public static function getText($id, $type, $output = 'editable')
    {
        return self::text([
            'id' => $id,
            'type' => $type,
            'output' => $output
        ]);
    }

    /**
     * Print text on screen
     *
     * @param $params
     * @param $filter
     *
     * @return string
     */
    public static function text($params, $filter = [])
    {
        $output = 'editable';

        // Get id and type
        if (!array_key_exists('id', $params)) {
            $params = View::filterMatch($params);
            $id = $params[0];
            $type = $params[1];
            if (isset($params[2])) {
                $filter_ = $params[2];
                $filter_ = explode('::', $filter_);

                $filter[$filter_[0]] = '';
                if (isset($filter_[1]))
                    $filter[$filter_[0]] = explode('|', $filter_[1]);
            }
        } else {
            $id = $params['id'];
            $type = $params['type'];
            $output = $params['output'];
        }

        // Find text in database by id
        if (!isset(self::$texts[$id]))
            self::$texts[$id] = Database::Model('Text')->find($id);

        // Output only if text was found
        if (!empty(self::$texts[$id])) {
            $value = self::$texts[$id]->getTr(Router::$params['locale'], $type);
            foreach ($filter as $filter_ => $options) {
                $value = Sanitizer::applyFilter($filter_, $value, $options);
            }
            return self::output($id, $value, $type, $output);
        }
    }

	/**
	 * Get simple text
	 *
	 * @param $id
	 * @param $type
	 * @return mixed
	 */
	public static function getSimpleText($id, $type)
	{
		// Find text in database by id
		if (!isset(self::$texts[$id]))
			self::$texts[$id] = Database::Model('Text')->find($id);
		return self::$texts[$id]->{$type};
    }

    /**
     * Output text
     *
     * @param $id
     * @param $text
     * @param $type
     * @param $output
     * @return string
     */
    private static function output($id, $text, $type, $output)
    {
        $authorized = Auth::isAuthorized('text') && isset(Router::$params['edit']);
        $text = str_replace('"', "'", $text);
        $out = '<?php echo "';
        if ($type == 'title') {
            if ($authorized && $output == 'editable') $out .= '<abbr data-id=\"' . $id . '\" class=\"content-edit-title\">';
            $out .= htmlspecialchars($text);
            if ($authorized && $output == 'editable') $out .= '</abbr>';
        } else {
            if ($authorized && $output == 'editable') $out .= '<div data-id=\"' . $id . '\" style=\"display: inline-block\" class=\"content-edit-te\">';
            $out .= $text;
            if ($authorized && $output == 'editable') $out .= '</div>';
        }
        $out .= '" ?>';
        return $out;
    }

    /**
     * Output live editor
     */
    public function liveEditor()
    {
        $authorized = Auth::isAuthorized('text') && isset(Router::$params['edit']);
        if ($authorized)
            return View::view('/vendor/vermal/admin/src/App/Defaults/templates/parts/liveEditor', [], true);
    }

    /**
     * Live editor
     */
    public function liveEditorResources()
    {
        $authorized = Auth::isAuthorized('text') && isset(Router::$params['edit']);
        if ($authorized)
            return '<script v:admin:public:src="assets/vendor/trumbowyg/trumbowyg.js"></script>
                  <link rel="stylesheet" v:admin:public:href="assets/vendor/trumbowyg/ui/trumbowyg.css">';
    }

}
