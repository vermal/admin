<?php

namespace Vermal\Admin\Defaults;
use Symfony\Component\Yaml\Yaml;
use Vermal\Admin\Modules\Auth\Auth;
use Vermal\Admin\Sanitizer;
use Vermal\App;
use Vermal\Database\Database;
use Vermal\Ecommerce\Defaults\Currencies;
use Vermal\Form\Form;
use Vermal\Router;
use Vermal\Admin\View;

/**
 * Class Controller
 * @package Vermal\Admin\Defaults
 */
class Controller extends \Vermal\Controller
{
    const SETTINGS = 1;

    protected $user = null;
    protected $CRUDAction = null;
    public $breadcrumb = [];
    protected $settings = [];

    protected $languages = [];
    public static $langs = [];
    public static $appSettings = [];
    public static $appLocalizedSettings = [];
	public static $appCurrency = '';
	public static $appCurrencySymbol = '';

    /**
     * @var Mailer
     */
    protected $mailer;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        parent::__construct();

        View::setVariable('publicURL', Router::$URL . 'vendor/vermal/admin/src/public/');
        View::setVariable('defaultDirView', '../../../Defaults/templates/');
        View::setVariable('currentUser', Auth::getUser());
        View::setVariable('currentUserName', (!empty(Auth::getUser()) ? Auth::getUser()->name : ''));

        // Get settings
        self::getSettings($this->locale);
        $this->settings = self::$appSettings;

        // Set languages variable
        $this->languages = [];
        foreach ($this->settings->languages as $k => $lang) {
            $this->languages[$k] = mb_strtolower($lang);
        }
        $langs = [];
        if (!empty(App::get('languagesOrder', []))) {
            // Sort languages by custom order
            foreach (App::get('languagesOrder', []) as $lang) {
                if (in_array($lang, $this->languages)) {
                    $langs[] = $lang;
                }
            }
        } else {
            $langs = $this->languages;
        }
        self::$langs = $langs;
        View::setVariable('languages', $langs);

        // Set currecnt locale for database
        Database::$locale = $this->locale;


        // Check if is login page
        if (Router::$currentName == 'admin.loginView' && Auth::isLoggedIn()) {
            Router::redirect('admin.dashboard');
        }

        // Redirect to login page if user is not logged in
        if (!in_array(Router::$currentName, [
                'admin.loginView',
                'admin.login',
                'admin.resetPasswordView',
                'admin.resetPasswordMail',
                'admin.resetPasswordFromMailView',
                'admin.resetPasswordFromMail'
            ]) && !Auth::isLoggedIn()) {
            Router::redirect('admin.loginView', [
                'r' => Router::$currentName
            ]);
        }

        // Add user property if is logged in
        if (Auth::isLoggedIn()) {
            $this->user = Auth::getUser();
        }

        // Set propper paths for View
        View::setExtendsPath(__DIR__ . '/templates/');
        if (isset($this->defaultViewPath) && !$this->defaultViewPath) {
            $this->setViewPath();
        } else {
            $this->setDefaultViewPath();
        }

        // Set current CRUD action
        $this->detectCRUDAction();

        // Add dashboard to breadcrum
        $this->addToBreadcrumb('dashboard', 'admin.dashboard');
        View::addBeforeRender(function () {
            View::setVariable('breadcrumb', $this->breadcrumb);
            if (isset($this->canCreate)) {
                View::setVariable('canCreate', $this->canCreate);
            }
            if (isset($this->canGoBack)) {
                View::setVariable('canGoBack', $this->canGoBack);
            }
        });

        // Check if locale is in system languages
        if (!in_array($this->locale, $this->languages)) {
            $this->locale = $this->languages[0];
        }
    }

    /**
     * Get admin settings
     *
     * @param string $locale
     */
    public static function getSettings($locale)
    {
        self::$appSettings = Database::Model('Setting')->find(self::SETTINGS);
        self::$appLocalizedSettings = Database::Model('LocalizationSetting')
            ->where('l.lang', $locale)->first();
		self::$appCurrency = !empty(self::$appLocalizedSettings->currency) ? self::$appLocalizedSettings->currency : App::get('currency');
		self::$appCurrencySymbol = Currencies::getSymbol(self::$appCurrency);
    }

    /**
     * Add item to breadcrum
     *
     * @param $key
     * @param $link
     * @param $params
     * @param $data
     */
    public function addToBreadcrumb($key, $link, $params = [], $data = [])
    {
        $this->breadcrumb[] = [
            'title' => printfArray($this->_('resources.' . $key), $data),
            'link' => routerLink($link, $params)
        ];
    }

    /**
     * Set default view path
     */
    public function setDefaultViewPath()
    {
        View::setModulePath('Defaults');
        View::setViewPath(__DIR__ . '/templates/');
    }

    /**
     * Set view path to specific module
     */
    private function setViewPath()
    {
        $currentModule = str_replace('\\', '/', get_called_class());
        $currentModule = substr($currentModule, 0, strrpos($currentModule, '/'));
        $currentModule = str_replace('Vermal/Admin/Modules/', '', $currentModule);
        $currentModule = str_replace('App/Admin/Modules/', '', $currentModule);
        View::setModulePath($currentModule);
        View::setViewPath(__DIR__ . '/../Modules/' . $currentModule . '/templates/');
    }

    /**
     * Chcek if user is required to this action
     *
     * @param $permission
     * @param $action
     * @return boolean
     */
    public function requiredPermission($permission, $action = null)
    {
        if (!Auth::isAuthorized($permission, $action) && Auth::isLoggedIn()) {
            $this->alert($this->_('global.low_permissions'), 'danger');
            if ($action === null || $action === 'read') {
                Router::redirect('admin.dashboard');
            } else return false;
        }
        return true;
    }

    /**
     * Detect current CRUD action
     */
    private function detectCRUDAction()
    {
        if (isset($_POST['_method'])) {
            if ($_POST['_method'] == 'PUT') {
                $this->CRUDAction = 'update';
            } else if ($_POST['_method'] == 'DELETE') {
                $this->CRUDAction = 'delete';
            } else if ($_POST['_method'] == 'POST') {
                $this->CRUDAction = 'create';
            }
        } else {
            if (preg_match('/admin.(.*?).index/', Router::$currentName)) {
                $this->CRUDAction = 'read';
                $this->canCreate = Router::$currentName;
            } else if (preg_match('/admin.(.*?).edit/', Router::$currentName)) {
                $this->CRUDAction = 'update';
                $this->canGoBack = str_replace('.edit', '.index', Router::$currentName);
            } else if (preg_match('/admin.(.*?).create/', Router::$currentName)) {
                $this->CRUDAction = 'create';
                $this->canGoBack = str_replace('.create', '.index', Router::$currentName);
            } else if (preg_match('/admin.(.*?).destroy/', Router::$currentName) || preg_match('/admin.(.*?).destroy_/', Router::$currentName)) {
                $this->CRUDAction = 'delete';
            }
        }
    }

    public function setFluidLayoutContainer()
    {
        View::setVariable('isFluid', true);
    }

    /**
     * Sanitize
     *
     * @param $filter
     * @param $value
     * @return mixed
     */
    protected function sanitize($filter, $value)
    {
        return Sanitizer::applyFilter($filter, $value);
    }

	/**
	 * Vue enabled
	 */
    protected function vue()
	{
		View::setVariable('VUE', true);
	}

	/**
	 * @param null $settings
	 * @return Mailer
	 */
	public static function mailer($settings = null)
	{
		return parent::mailer(self::$appSettings);
	}
}
