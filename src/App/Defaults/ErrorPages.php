<?php

namespace Vermal\Admin\Defaults;

use Vermal\Admin\View;

/**
 * Class Controller
 * @package Vermal\Admin\Defaults
 */
class ErrorPages extends Controller
{

    public function __construct()
    {
        parent::__construct();
        View::setVariable('jumboPage', 'ewfwef');
    }

    public function e404()
    {
        View::view('error.404');
    }

}
