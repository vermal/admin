<?php

namespace Vermal\Admin\Defaults;


use Vermal\Database\MagicAccessor;

/**
 * @ORM\HasLifecycleCallbacks
 */
class Model
{
    use MagicAccessor;

    /** @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue **/
    protected $id;

	/** @ORM\Column(type="string") **/
	protected $token;

    /** ... */

    /** @ORM\Column(type="datetime") **/
    protected $created_at;

    /** @ORM\Column(type="datetime") **/
    protected $updated_at;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    public function __construct()
    {
    }

    /**
     * Gets triggered only on insert
     *
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->created_at = new \DateTime("now");
        $this->updated_at = new \DateTime("now");
        $this->token = md5(uniqid(rand(), true));
    }

    /**
     * Gets triggered every time on update
     *
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->updated_at = new \DateTime("now");
        if (empty($this->token))
			$this->token = md5(uniqid(rand(), true));
    }

    /**
     * Convert to array
     *
     * @return array
     */
    public function toArray()
    {
        return get_object_vars($this);
    }

	/**
	 * @param string $format
	 * @return false|string
	 */
	public function getFormattedUpdatedAt($format = 'd.m.Y H:i:s')
	{
		return date($format, $this->updated_at->getTimestamp());
	}

	/**
	 * @param string $format
	 * @return false|string
	 */
	public function getFormattedCreatedAt($format = 'd.m.Y H:i:s')
	{
		return date($format, $this->created_at->getTimestamp());
    }
}
