<?php

use Vermal\Router;

Router::prefix('{locale::locale}?/admin/', function() {

    // Default namespace for all modules
    $namespace = '\\Vermal\\Admin\\Modules\\';

    Router::get('', $namespace . "Auth\\Auth@index")->name('dashboard');
    Router::get('login', $namespace . "Auth\\Auth@loginView")->name('loginView');
    Router::post('login', $namespace . "Auth\\Auth@login")->name('login');
    Router::get('logout', function() {
        unset($_SESSION['user']);
        Router::redirect('admin.loginView');
    })->name('logout');
    Router::get('reset-password', $namespace . "Auth\\Auth@resetPasswordView")->name('resetPasswordView');
    Router::post('reset-password', $namespace . "Auth\\Auth@resetPasswordMail")->name('resetPasswordMail');
    Router::get('reset-password/{token}', $namespace . "Auth\\Auth@resetPasswordFromMailView")->name('resetPasswordFromMailView');
    Router::post('reset-password/{token}', $namespace . "Auth\\Auth@resetPasswordFromMail")->name('resetPasswordFromMail');

    // Auth
    Router::resource('user', $namespace . 'Auth\\Users', 'user.');
    Router::resource('resource', $namespace . 'Auth\\Resources', 'resource.');
    Router::resource('role', $namespace . 'Auth\\Roles', 'role.');

    // Menu Builder
    Router::get('menu/builder/{id}', $namespace . 'Menu\\MenuBuilder@show')->name('menu-builder.show');
    Router::put('menu/builder/update', $namespace . 'Menu\\MenuBuilder@update')->name('menu-builder.update');
    Router::delete('menu/builder/delete', $namespace . 'Menu\\MenuBuilder@delete')->name('menu-builder.delete');
    Router::post('menu/builder/save', $namespace . 'Menu\\MenuBuilder@save')->name('menu-builder.save');

    // Menu
    Router::resource('menu', $namespace . 'Menu\\Menus', 'menu.');

    // Multimedias
    Router::get('multimedia', $namespace . 'Multimedia\\Multimedias@index')->name('multimedia.index');
    Router::get('multimedia/folder/{id}', $namespace . 'Multimedia\\Multimedias@folder')->name('multimedia.list-folder');
    Router::post('multimedia/upload', $namespace . 'Multimedia\\Multimedias@upload')->name('multimedia.upload');
    Router::get('multimedia/delete/{id}', $namespace . 'Multimedia\\Multimedias@delete')->name('multimedia.delete');
    Router::put('multimedia/rename', $namespace . 'Multimedia\\Multimedias@rename')->name('multimedia.rename');
    Router::get('multimedia/images/{folder}/{page}?', $namespace . 'Multimedia\\Multimedias@getImagesForMultimediaSelector')->name('multimedia.images');

    // Articles
    Router::resource('article', $namespace . 'Post\\Articles', 'article.');
    Router::resource('article-category', $namespace . 'Post\\ArticleCategories', 'article-category.');

    // Settings
    Router::get('settings', $namespace . 'Settings\\Settings@index')->name('settings.index');
    Router::post('settings', $namespace . 'Settings\\Settings@store')->name('settings.store');
    Router::get('localization-settings', $namespace . 'Settings\\LocalizationSettings@index')->name('localization-settings.index');
    Router::post('localization-settings', $namespace . 'Settings\\LocalizationSettings@store')->name('localization-settings.store');

    // Text
    Router::resource('text', $namespace . 'Text\\Texts', 'text.');
	Router::post('text-sort', $namespace . 'Text\\Texts@sort')->name('text.sort');
    Router::post('text-edit', $namespace . 'Text\\Texts@editSingle')->name('editSingle');

    // FAQ
	Router::resource('faq', $namespace . 'Faq\\Faqs', 'faq.');
	Router::post('faq-sort', $namespace . 'Faq\\Faqs@sort')->name('faq.sort');

	// Gallery
	Router::resource('gallery', $namespace . 'Gallery\\Galleries', 'gallery.');

	// SEO
	Router::resource('seo', $namespace . 'Seo\\Seos', 'seo.');

    // Form Builder
    Router::resource('form-builder', $namespace . 'FormBuilder\\Forms', 'form.');

    // Modules
    Router::resource('modules', $namespace . 'Modules\\Modules', 'module.');
    Router::resource('module-values', $namespace . 'Modules\\ModuleValues', 'module-values.');
    Router::get('module-values/generate/{id}', $namespace . 'Modules\\ModuleValues@generate')->name('module-values.generate');

    // PDF Templeates
    Router::resource('pdftemplate', $namespace . 'PDFTemplates\\PDFTemplates', 'pdftemplate.');

    // SELECT 2 SEARCH
	Router::post('select2-search', $namespace . 'Extensions\\Select2@select2')->name('select2');

	// Datagrid activate
	Router::get('datatable-switch/{id}/{c}', $namespace . 'Extensions\\Activate@activate')->name('switch');

	// Datagrid action
	Router::post('datagrid-action', $namespace . 'Extensions\\Datagrid@action')->name('datagrid-action');

	// Datagrid action
	Router::get('datagrid-export/{c}/{type}', $namespace . 'Extensions\\Datagrid@export')->name('datagrid-export');

	// Text editor upload image
	Router::post('text-editor-upload', $namespace . 'Extensions\\TextEditor@upload')->name('text-editor.upload');

}, 'admin.', ['locale']);
