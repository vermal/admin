<?php

namespace Vermal\Admin;


use Gregwar\Image\Image;
use Vermal\Admin\Modules\Multimedia\Entities\Multimedia;
use Vermal\Admin\Modules\Multimedia\Entities\Post;
use Vermal\Database\Database;
use Vermal\Database\Entity;
use Vermal\App;

class Upload
{
	public static $uploadPath = BASE_PATH . '/';

	public static function uploadFile($inputName, $folderID)
	{
		$folder = self::getFolder($folderID);
		$folderPath = $folder->getPathFullToParent();

		$uploadPath = self::$uploadPath . 'public/uploads' . $folderPath;
		$files = [];
		foreach (self::reArray($_FILES[$inputName]) as $key => $f) {

			$_FILES['input_' . $key] = $f;
			$upload = new \Delight\FileUpload\FileUpload();
			$upload->from('input_' . $key);

			// Set max upload size in MB
			$upload->withMaximumSizeInMegabytes(App::get('maxUploadSize', 20));

			// Extension
			$ext = self::getExtension($f['name']);

			// Check for image type
			$isImage = true;
			if($ext !== 'png' && $ext !== 'jpg' && $ext !== 'jpeg') {
				$isImage = false;
			}

			// Get mime or set image
			$fileType = 'file';
			if ($isImage) {
				$fileType = 'image';
			}

			if (!is_dir($uploadPath)) {
				mkdir($uploadPath, 0755, true);
			}

			// Save file to database
			$file = Entity::getEntity('Multimedia');
			$file->name = $f['name'];
			$file->encryptedName = 'placeholder';
			$file->type = $fileType;
			$file->extension = self::getExtension($f['name']);
			$file->size = filesize($f['tmp_name']);
			$file->folder = $folder;

			$file = Database::saveAndFlush($file);

			// Set encrypted name
			$file->encryptedName = $file->id . '_' . uniqid();

			// Full image path
			$fullPath = BASE_PATH . '/public/uploads' . $folderPath . $file->encryptedName . '.' . $file->extension;

			// Upload file
			$upload->withTargetDirectory(BASE_PATH . '/public/uploads' . $folderPath);
			$upload->withTargetFilename($file->encryptedName);

			// Upload image
			try {
				$upload->save();
				if ($file->extension == 'jpg' || $file->extension == 'jpeg')
					self::correctImageOrientation($fullPath);

				// Set dimesions if is image and resize if image is too big
				if ($isImage) {
					$savedImagePath = $upload->getTargetDirectory() . '/' . $upload->getTargetFilename() . '.' . strtolower($file->extension);
					list($width, $height, $type, $attr) = getimagesize($savedImagePath);
					$file->width = $width;
					$file->height = $height;

					// Resize image
					if ($width > 1920 || $height > 1080) {
						$img = Image::open($savedImagePath);
						$img->cropResize(1920, 1080);

						try {
							$img->save($savedImagePath);
						} catch (\Exception $e) {
							throw new \Exception('Image is too big');
						}

						// Set new file size
						$file->size = filesize($savedImagePath);
						list($width, $height, $type, $attr) = getimagesize($savedImagePath);
						$file->width = $width;
						$file->height = $height;
					}
				}

			} catch (\Exception $e) {
				Database::Model('Multimedia')->delete($file->id);
				throw new \Exception('Unable to upload file!');
			}

			// Update file
			$savedFile = Database::saveAndFlush($file);

			// Save id if is image
			$files[] = $savedFile->id;
		}
		return $files;
	}

	public static function cropImages($images)
	{
		foreach ($images as $image) {
			$image = Database::Model('Multimedia')->find($image);
			self::cropImage($image);
		}
	}

	/**
	 * @param $image
	 * @param int $width
	 * @param int $height
	 * @throws \Exception
	 */
	public static function cropImage($image, $width = 250, $height = 250)
	{
		if ($image->type !== 'image') return;

		$img = Image::open(self::$uploadPath . $image->getFile());
		if ($img->width() <= $width || $img->height() <= $height) return;
		$img->cropResize($width, $height);

		$newEncryptedFileName = $image->encryptedName . '_' . $width . '_' . $height;

		$newImagePath = self::$uploadPath .
			str_replace($image->encryptedName, $newEncryptedFileName, $image->getFile());
		try {
			$img->save($newImagePath);
		} catch (\Exception $e) {
			throw new \Exception('Unable to crop image');
		}

		// Save file to database
		$file = Entity::getEntity('Multimedia');
		$file->name = $image->name;
		$file->encryptedName = $newEncryptedFileName;
		$file->type = $image->type;
		$file->extension = $image->extension;
		$file->size = filesize($newImagePath);
		$file->folder = $image->folder;
		$file->parent = $image;

		Database::saveAndFlush($file);
	}

	private static function correctImageOrientation($filename) {
		if (function_exists('exif_read_data')) {
			$exif = exif_read_data($filename);
			if($exif && isset($exif['Orientation'])) {
				$orientation = $exif['Orientation'];
				if($orientation != 1){
					$img = imagecreatefromjpeg($filename);
					$deg = 0;
					switch ($orientation) {
						case 3:
							$deg = 180;
							break;
						case 6:
							$deg = 270;
							break;
						case 8:
							$deg = 90;
							break;
					}
					if ($deg) {
						$img = imagerotate($img, $deg, 0);
					}
					// then rewrite the rotated image back to the disk as $filename
					imagejpeg($img, $filename, 95);
				} // if there is some rotation necessary
			} // if have the exif orientation info
		} // if function exists
	}

	/**
	 * Get extension from filename
	 *
	 * @param $filename
	 * @return mixed
	 */
	public static function getExtension($filename)
	{
		return strtolower(pathinfo($filename, PATHINFO_EXTENSION));
	}

	/**
	 * Delete file
	 *
	 * @param $file
	 * @return bool
	 */
	public static function deleteFile($file)
	{
		$file = BASE_PATH . DIRECTORY_SEPARATOR . $file;
		if (is_file($file)) {
			return unlink($file);
		} else return false;
	}

	/**
	 * Rearrange array from html
	 *
	 * @param $files
	 * @return array
	 */
	public static function reArray($files) {
		if (!is_array($files['name'])) {
			return [$files];
		}
		$file_array = array();
		$file_count = count($files["name"]);
		$file_keys = array_keys($files);
		for ($i = 0 ; $i < $file_count ; $i++) {
			foreach ($file_keys as $key) {
				$file_array[$i][$key] = $files[$key][$i];
			}
		}
		return $file_array;
	}

	/**
	 * Get folder id
	 *
	 * @param string|int $folderId
	 * @return mixed
	 */
	private static function getFolder($folderId) {
		$folder = $folderId;
		if (!is_numeric($folderId)) {
			return Database::Model('MultimediaFolder')
				->where('m.name', $folder)->first();
		} else {
			return Database::Model('MultimediaFolder')->find($folderId);
		}
	}

}
