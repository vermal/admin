<?php

namespace Vermal\Admin;


class View extends \Vermal\View
{
    private static $moduleName = "";

    public static function view($view, $variables = [], $return = false)
    {
        $myView = parent::handleFileName($view);
        $file = BASE_PATH . '/App/Admin/Modules/' . self::$moduleName . '/templates/' . $myView;
        if (file_exists($file)) {
            parent::$viewPath = BASE_PATH . '/App/Admin/Modules/' . self::$moduleName . '/templates/';
        }
        return parent::view($view, $variables, $return);
    }

    /**
     * Set module name
     *
     * @param $name
     */
    public static function setModulePath($name)
    {
        self::$moduleName = $name;
    }

	/**
	 * Get view path
	 *
	 * @return string
	 */
	public static function getViewPath()
	{
		return self::$viewPath;
	}

}
