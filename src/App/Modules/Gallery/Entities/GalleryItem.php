<?php

namespace Vermal\Admin\Modules\Gallery\Entities;

/**
 * @ORM\Entity @ORM\Table(name="gallery_item")
 **/
class GalleryItem
{

	/** @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue **/
	protected $id;

	/**
	 * @ORM\ManyToOne(targetEntity="\Multimedia")
	 * @ORM\JoinColumn(name="multimedia_id", referencedColumnName="id", nullable=true)
	 */
	protected $multimedia;

	/**
	 * @ORM\ManyToOne(targetEntity="\Gallery", inversedBy="items")
	 * @ORM\JoinColumn(name="gallery_id", referencedColumnName="id", onDelete="CASCADE")
	 */
	protected $gallery;
}
