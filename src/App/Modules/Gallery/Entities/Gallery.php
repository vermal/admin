<?php

namespace Vermal\Admin\Modules\Gallery\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Entities\Translatable;
use Vermal\Admin\Defaults\Model;
use Vermal\Ecommerce\Modules\Product\Entities\Extensions\Attributes;
use Vermal\Ecommerce\Modules\Product\Entities\Extensions\Categories;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="gallery")
 * @ORM\HasLifecycleCallbacks
 **/
class Gallery extends Model
{

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", nullable=true)
     */
    protected $title;

	/**
	 * @ORM\ManyToMany(targetEntity="\Multimedia", cascade={"persist"})
	 * @ORM\JoinTable(name="gallery_multimedia",
	 *      joinColumns={@ORM\JoinColumn(name="gallery_id", referencedColumnName="id")},
	 *      inverseJoinColumns={@ORM\JoinColumn(name="multimedia_id", referencedColumnName="id", unique=true)}
	 *      )
	 */
	protected $multimedia;
//	/**
//	 * @ORM\OneToMany(
//	 *   targetEntity="\GalleryItem",
//	 *   mappedBy="gallery",
//	 *   cascade={"persist", "remove"},
//	 *   orphanRemoval=true
//	 * )
//	 */
//	protected $items;

	public function __construct()
	{
		parent::__construct();
//		$this->items = new ArrayCollection();
		$this->multimedia = new ArrayCollection();
	}

	/**
	 * Add new image
	 *
	 * @param $multimedia
	 */
	public function addMultimedia($multimedia)
	{
		$this->multimedia[] = $multimedia;
	}

	/**
	 * Clear multimedia
	 */
	public function clearMultimedia()
	{
		$this->multimedia->clear();
	}
}
