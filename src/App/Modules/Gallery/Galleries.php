<?php

namespace Vermal\Admin\Modules\Gallery;


use Vermal\Admin\Defaults\Controller;
use Vermal\Admin\Defaults\Languages;
use Vermal\Database\Entity;
use Vermal\DataGrid;
use Vermal\Admin\Modules\Gallery\Entities\Gallery;
use Vermal\Form\Form;
use Vermal\Admin\View;
use Vermal\Database\Database;
use Vermal\Router;
use Vermal\Traits\DataGridSort;


class Galleries extends Controller
{
	public $defaultViewPath = false;

    public function __construct()
    {
        parent::__construct();
//        $this->requiredPermission('gallery', $this->CRUDAction);
        $this->addToBreadcrumb('gallery', 'admin.gallery.index');
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
    	$this->setDefaultViewPath();

        $data = Database::Model('Gallery');
        $datagrid = new DataGrid($data);

        $datagrid->addColumn('id', 'ID');
        $datagrid->addColumn('title', $this->_('gallery.title'));

        // Add actions
        $datagrid->addEdit(['admin.gallery.edit', ['id']]);
        $datagrid->addDelete(['admin.gallery.destroy_', ['id']]);

        // Return view
        View::view('crud.read', [
            'datagrid' => $datagrid->build()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
	    $form = $this->form();
	    View::view('gallery', [
		    "form" => $form->build(),
			"gallery" => ''
	    ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \stdClass $post
     * @throws
     */
    public function store($post)
    {
        $form = $this->form();
        if ($form->hasError()) {
            Router::redirect('admin.gallery.create');
        }

        $gallery = Entity::getEntity('Gallery');
        $this->hydrate($gallery, $post);

        Database::saveAndFlush($gallery);

        // Redirect with message
        $this->alert($this->_('gallery.created'));
        Router::redirect('admin.gallery.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @throws
     */
    public function edit($id)
    {
        $form = $this->form();
        $form->setAjax(true);
        $form->setMethod('PUT');
        $form->setAction(routerLink('admin.gallery.update', ['id' => $id]));

        /** @var Gallery $address */
        $gallery = Database::Model('Gallery')->find($id);

	    // Set values content
	    $form->setValues($gallery, ['multimedia']);

		// Gallery
		if (!$gallery->multimedia->isEmpty()) {
			$multimedia = [];
			foreach ($gallery->multimedia as $multimedia_) {
				$multimedia[] = $multimedia_->id;
			}
			View::setVariable('gallery', $multimedia);
		} else View::setVariable('gallery', []);

        View::view('gallery', [
	        "form" => $form->build(),
			"gallery" => ''
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \stdClass $post
     * @param  int  $id
     * @throws
     */
    public function update($post, $id)
    {
        $form = $this->form();
        if ($form->hasError()) {
            $this->alert($this->_('error.form.empty'), 'danger');
            die();
        }

	    $gallery = Database::Model('Gallery')->find($id);
		$this->hydrate($gallery, $post, true);

        $this->alert($this->_('gallery.edited'));
        Database::saveAndFlush($gallery);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
    	$gallery = Database::Model('Gallery')->find($id);
    	$gallery->clearMultimedia();
    	Database::saveAndFlush($gallery);
        Database::Model('Gallery')->delete($id);

        // Redirect
        $this->alert($this->_('gallery.deleted'));
        Router::redirect('admin.gallery.index');
    }

	/**
	 * Hydrate database object
	 *
	 * @param Gallery $entity
	 * @param $post
	 * @param $update
	 * @return object
	 * @throws \Exception
	 */
	private function hydrate($entity, $post, $update = false)
	{
		// Add translation to content
		if ($update) {
			$entity->clearMultimedia();
			Database::saveAndFlush($entity);
		}

		$entity->title = $this->sanitize('stip_tags|trim|escape', $post->title);

		// Add images to gallery
		if (isset($post->gallery) && !empty($post->gallery)) {
			foreach ($post->gallery as $image) {
				$entity->addMultimedia(Database::Model('Multimedia')->find($image));
			}
		}

		return $entity;
	}

    /**
     * Create register form
     *
     * @return Form
     */
    private function form()
    {
        $form = new Form('gallery', routerLink('admin.gallery.store'));

	    // Data
	    $form->addText('title', $this->_('gallery.title'))->min(2);

        // Add submit button
        $form->addButton('submit', $this->_('global.form.save'))->setClass('btn btn-success');

        return $form;
    }
}
