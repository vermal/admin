@extends('layout')

@section('body')


    <div class="container">
        <div class="card shadow-lg m-b-30 p-4">
            @form("gallery")

                @label("title")
                @control("title")

                <div class="col-12 my-4">
                    <label>@__(gallery.image)</label>
                    @include("{$defaultDirView}parts/multimedia", ["name" => "gallery", "folder" => 'gallery', "selected" => $gallery, 'multiple' => true])
                </div>

                <div class="col-12 text-right mt-4">
                    @control("submit")
                </div>
            @endform
        </div>
    </div>

@endsection
