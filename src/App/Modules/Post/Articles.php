<?php

namespace Vermal\Admin\Modules\Post;


use Vermal\Admin\Defaults\Controller;
use Vermal\Admin\Defaults\Languages;
use Vermal\Admin\Modules\Auth\Auth;
use Vermal\Admin\Modules\Post\Entities\Post;
use Vermal\Admin\Modules\Post\Entities\PostMeta;
use Vermal\Admin\Modules\Post\Entities\PostTr;
use Vermal\Admin\Sanitizer;
use Vermal\App;
use Vermal\Database\Entity;
use Vermal\DataGrid;
use Vermal\Form\Form;
use Vermal\Router;
use Vermal\Admin\View;
use Vermal\Admin\Upload;
use Vermal\Database\Database;


class Articles extends Controller
{
    public $defaultViewPath = false;

    public function __construct()
    {
        parent::__construct();
        $this->requiredPermission('article', $this->CRUDAction);
        $this->addToBreadcrumb('article', 'admin.article.index');
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // Set default view path
        $this->setDefaultViewPath();

        $data = Database::Model('Post')->where('p.type', 'article')->order('p.id', 'DESC');
        $datagrid = new DataGrid($data);

        $datagrid->addColumn('id', 'ID');
        $datagrid->addColumn('title', $this->_('article.title'))->setRenderer(function($post) {
            return $post->getTr($this->locale)->title;
        });

        // Add actions
        $datagrid->addEdit(['admin.article.edit', ['id']]);
        $datagrid->addDelete(['admin.article.destroy_', ['id']]);

        // Return view
        View::view('crud.read', [
            'datagrid' => $datagrid->build()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $form = $this->form();
        View::view('articleForm', [
            "form" => $form->build(),
            'simpleMultimedia' => [],
            'gallery' => []
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \stdClass $post
     */
    public function store($post)
    {
        $form = $this->form();
        if ($form->hasError()) {
            Router::redirect('admin.article.create');
        }

        $article = Entity::getEntity('Post');

        $article->sort = 0;
        $article->type = 'article';
        $article->user = Database::Model('User')->find(Auth::getUser()->id);

        foreach (Languages::get() as $lang) {
            $articleTr = Entity::getEntity('PostTr');

			if ($lang === App::get('locale') && empty($post->{$lang . '_title'})) {
				$this->alert($this->_('error.form.empty'), 'danger');
				Router::redirect('admin.article.create');
			}

            $articleTr->lang = $lang;
            $articleTr->title = Sanitizer::applyFilter('trim|escape|strip_tags', $post->{$lang . '_title'});
            $articleTr->headline = Sanitizer::applyFilter('trim|escape|strip_tags', $post->{$lang . '_headline'});
            $articleTr->te = Sanitizer::applyFilter('trim', $post->{$lang . '_te'});
            $articleTr->keywords = Sanitizer::applyFilter('trim|escape|strip_tags', $post->{$lang . '_keywords'});
            $articleTr->slug = self::slugify($articleTr->title);

            $articleTr->post = $article;
            $article->addTr($articleTr);

            Database::save($articleTr);
        }

        // Add categories to article
        foreach ($post->categories as $category) {
        	if (is_object($category) || is_array($category)) $category = current((array)$category);
            $newCategory = Database::Model('PostMeta')->find($category);
            $article->addCategory($newCategory);
            Database::save($newCategory);
        }

        // Set image
        if (isset($post->image) && !empty($post->image)) {
            $article->simpleMultimedia = Database::Model('Multimedia')->find($post->image);
        }

        // Add images to gallery
        if (isset($post->gallery) && !empty($post->gallery)) {
            foreach ($post->gallery as $image) {
                $article->addMultimedia(Database::Model('Multimedia')->find($image));
            }
        }

        Database::saveAndFlush($article);

        // Redirect with message
        $this->alert($this->_('article.created'));
        Router::redirect('admin.article.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @throws
     */
    public function edit($id)
    {
        $form = $this->form();
        $form->setAjax(true);
        $form->setMethod('PUT');
        $form->setAction(routerLink('admin.article.update', ['id' => $id]));

        $article = Database::Model('Post')->find($id);

        // Add to breadcrumb
        $this->addToBreadcrumb('dyn', 'admin.article.edit', ['id' => $id], [$article->getTr($this->locale)->title]);

        // Set values
        foreach (Languages::get() as $lang) {
            $tr = $article->getTr($lang);
            $form->getComponent($lang . '_title')->setValue($tr->title);
            $form->getComponent($lang . '_headline')->setValue($tr->headline);
            $form->getComponent($lang . '_te')->setValue($tr->te);
            $form->getComponent($lang . '_keywords')->setValue($tr->keywords);
        }

        // Categories
        $categories = [];
        foreach ($article->getCategories() as $category) {
            $categories[$category->id] = $category->id;
        }
        $form->getComponent('categories[]')->setValue($categories);

        // Simple image
        if ($article->simpleMultimedia !== null) {
            View::setVariable('simpleMultimedia', [$article->simpleMultimedia->id]);
        } else View::setVariable('simpleMultimedia', []);

        // Gallery
        if (!$article->multimedia->isEmpty()) {
            $multimedia = [];
            foreach ($article->multimedia as $multimedia_) {
                $multimedia[] = $multimedia_->id;
            }
            View::setVariable('gallery', $multimedia);
        } else View::setVariable('gallery', []);

        View::view('articleForm', [
            "form" => $form->build()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \stdClass $post
     * @param  int  $id
     */
    public function update($post, $id)
    {
        $form = $this->form();
        if ($form->hasError()) {
            $this->alert($this->_('error.form.empty'), 'danger');
            die();
        }

        $article = Database::Model('Post')->find($id);

        $article->user = Database::Model('User')->find(Auth::getUser()->id);

        $article->clearTr();
        foreach (Languages::get() as $lang) {
            $articleTr = Entity::getEntity('PostTr');

            if ($lang === App::get('locale') && empty($post->{$lang . '_title'})) {
				$this->alert($this->_('error.form.empty'), 'danger');
				die();
			}

            $articleTr->lang = $lang;
            $articleTr->title = Sanitizer::applyFilter('trim|escape|strip_tags', $post->{$lang . '_title'});
            $articleTr->headline = Sanitizer::applyFilter('trim|escape|strip_tags', $post->{$lang . '_headline'});
            $articleTr->te = Sanitizer::applyFilter('trim', $post->{$lang . '_te'});
            $articleTr->keywords = Sanitizer::applyFilter('trim|escape|strip_tags', $post->{$lang . '_keywords'});
            $articleTr->slug = self::slugify($articleTr->title);

            $articleTr->post = $article;
            $article->addTr($articleTr);

            Database::save($articleTr);
        }

        // Add categories to article
        $article->clearCategories();
        if (isset($post->categories) && !empty($post->categories)) {
            foreach ($post->categories as $category) {
				if (is_object($category) || is_array($category)) $category = current((array)$category);
                $newCategory = Database::Model('PostMeta')->find($category);
                $article->addCategory($newCategory);
                Database::save($newCategory);
            }
        }

        // Set image
        if (isset($post->simpleMultimedia) && !empty($post->simpleMultimedia)) {
            $article->simpleMultimedia = Database::Model('Multimedia')->find($post->simpleMultimedia);
        }

        // Add images to gallery
        if (isset($post->gallery) && !empty($post->gallery)) {
            $article->clearMultimedia();
            foreach ($post->gallery as $image) {
                $article->addMultimedia(Database::Model('Multimedia')->find($image));
            }
        } else $article->clearMultimedia();

        $this->alert($this->_('article.edited'));
        Database::saveAndFlush($article);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        $article = Database::Model('Post')->find($id);
	    $article->clearTr();
	    $article->clearMultimedia();
	    $article->clearMeta();
	    $article->simpleMultimedia = null;
        Database::saveAndFlush($article);
        Database::Model('Post')->delete($id);

        // Redirect
        $this->alert($this->_('article.deleted'));
        Router::redirect('admin.article.index');
    }

    /**
     * Create register form
     *
     * @return Form
     */
    private function form()
    {
        $form = new Form('article', routerLink('admin.article.store'));

        // Add categories to form
        $categories = [];
        foreach (Database::Model('PostMeta')->where('p.key', 'category')->get() as $category) {
            $categories[$category->id] = $category->getTr($this->locale)->value;
        }
        $form->addSelect('categories[]', $categories, $this->_('article.categories'))
            ->addAttribute('multiple', 'true')
            ->setClass('js-select2')
            ->required();

        // Article Data
        foreach (Languages::get() as $lang) {
            $form->addText($lang . '_title', $this->_('article.title'));
            $form->addText($lang . '_headline', $this->_('article.headline'));
            $form->addTextEditor($lang . '_te', $this->_('article.te'));
            $form->addTextArea($lang . '_keywords', $this->_('article.keywords'));
        }

        // Add submit button
        $form->addButton('submit', $this->_('global.form.save'))->setClass('btn btn-success');

        return $form;
    }
}
