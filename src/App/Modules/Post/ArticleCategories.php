<?php

namespace Vermal\Admin\Modules\Post;


use Vermal\Admin\Defaults\Controller;
use Vermal\Admin\Defaults\Languages;
use Vermal\Admin\Modules\Auth\Auth;
use Vermal\Admin\Modules\Post\Entities\Post;
use Vermal\Admin\Modules\Post\Entities\PostMeta;
use Vermal\Admin\Modules\Post\Entities\PostMetaTr;
use Vermal\Admin\Modules\Post\Entities\PostTr;
use Vermal\Admin\Sanitizer;
use Vermal\Database\Entity;
use Vermal\DataGrid;
use Vermal\Form\Form;
use Vermal\Router;
use Vermal\Admin\View;
use Vermal\Admin\Upload;
use Vermal\Database\Database;


class ArticleCategories extends Controller
{
    public $defaultViewPath = false;

    public function __construct()
    {
        parent::__construct();
        $this->requiredPermission('article-category', $this->CRUDAction);
        $this->addToBreadcrumb('article_category', 'admin.article-category.index');
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // Set default view path
        $this->setDefaultViewPath();

        $data = Database::Model('PostMeta')->where('p.key', 'category');
        $datagrid = new DataGrid($data);

        $datagrid->addColumn('id', 'ID');
        $datagrid->addColumn('title', $this->_('article.title'))->setRenderer(function($postMeta) {
            return $postMeta->getTr($this->locale)->value;
        });

        // Add actions
        $datagrid->addEdit(['admin.article-category.edit', ['id']]);
        $datagrid->addDelete(['admin.article-category.destroy_', ['id']]);

        // Return view
        View::view('crud.read', [
            'datagrid' => $datagrid->build()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $form = $this->form();
        View::view('articleCategoryForm', [
            "form" => $form->build()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \stdClass $post
     */
    public function store($post)
    {
        $form = $this->form();
        if ($form->hasError()) {
            Router::redirect('admin.article.create');
        }

        $meta = Entity::getEntity('PostMeta');

        $meta->key = 'category';
        $meta->value = slugify($post->{Languages::get()[0] . '_title'});

        // Set value for language
        foreach (Languages::get() as $lang) {
            $metaTr = Entity::getEntity('PostMetaTr');

            $metaTr->lang = $lang;
            $metaTr->value = Sanitizer::applyFilter('trim|escape|strip_tags', $post->{$lang . '_title'});

            $metaTr->postMeta = $meta;
            $meta->addTr($metaTr);

            Database::save($metaTr);
        }

        // Save
        Database::saveAndFlush($meta);

        // Redirect with message
        $this->alert($this->_('articleCategory.created'));
        Router::redirect('admin.article-category.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @throws
     */
    public function edit($id)
    {
        $form = $this->form();
        $form->setAjax(true);
        $form->setMethod('PUT');
        $form->setAction(routerLink('admin.article-category.update', ['id' => $id]));

        $meta = Database::Model('PostMeta')->find($id);

        // Add to breadcrumb
        $this->addToBreadcrumb('dyn', 'admin.article-category.edit', ['id' => $id], [$meta->getTr($this->locale)->value]);

        // Set values
        foreach (Languages::get() as $lang) {
            $tr = $meta->getTr($lang);
            $form->getComponent($lang . '_title')->setValue($tr->value);
        }

        View::view('articleCategoryForm', [
            "form" => $form->build()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \stdClass $post
     * @param  int  $id
     */
    public function update($post, $id)
    {
        $meta = Database::Model('PostMeta')->find($id);

        $form = $this->form();
        if ($form->hasError()) {
            $this->alert($this->_('error.form.empty'), 'danger');
            die();
        }

        $meta->clearTr();
        foreach (Languages::get() as $lang) {
            $metaTr = Entity::getEntity('PostMetaTr');

            $metaTr->lang = $lang;
            $metaTr->value = Sanitizer::applyFilter('trim|escape|strip_tags', $post->{$lang . '_title'});

            $metaTr->postMeta = $meta;
            $meta->addTr($metaTr);

            Database::save($metaTr);
        }

        $this->alert($this->_('articleCategory.edited'));
        Database::saveAndFlush($meta);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        $meta = Database::Model('PostMeta')->find($id);
        $meta->clearTr();
        Database::saveAndFlush($meta);
        Database::Model('PostMeta')->delete($id);

        // Redirect
        $this->alert('Category was deleted sucesfully');
        Router::redirect('admin.article-category.index');
    }

    /**
     * Create article category form
     *
     * @return Form
     */
    private function form()
    {
        $form = new Form('article-category', routerLink('admin.article-category.store'));

        foreach (Languages::get() as $lang) {
            $form->addText($lang . '_title', 'Category name')->min(2);
        }

        // Add submit button
        $form->addButton('submit', $this->_('global.form.save'))->setClass('btn btn-success');

        return $form;
    }
}
