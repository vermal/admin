@extends('layout')

@section('body')


    <div class="container">
        <div class="card shadow-lg m-b-30 p-4">
            @form("article")

            @label("categories[]")
            @control("categories[]")

            <div class="col-12 mt-2">
                <ul class="nav nav-tabs tab-line" id="myTab" role="tablist">
                    @foreach($languages as $lang)
                        <li class="nav-item">
                            <a class="nav-link {{ $loop->first ? "show active" : "" }}" id="tab-{{ $lang }}" data-toggle="tab" href="#{{ $lang }}" role="tab" aria-controls="{{ $lang }}" aria-selected="true">
                                {{ strtoupper($lang) }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>

            <div class="col-12 tab-content pt-4" id="myTabContent">

                @foreach($languages as $lang)

                    <div class="tab-pane fade {{ $loop->first ? "show active" : "" }} row" id="{{ $lang }}" role="tabpanel" aria-labelledby="tab-{{ $lang }}">
                        @label("{$lang}_title")
                        @control("{$lang}_title")

                        @label("{$lang}_headline")
                        @control("{$lang}_headline")

                        @label("{$lang}_te")
                        @control("{$lang}_te")

                        @label("{$lang}_keywords")
                        @control("{$lang}_keywords")
                    </div>

                @endforeach

            </div>

            <div class="col-12 my-4">
                <label>@__(article.image)</label>
                @include("{$defaultDirView}parts/multimedia", ["name" => "simpleMultimedia", "folder" => 2, "selected" => $simpleMultimedia])
            </div>

            <div class="col-12 my-4">
                <label>@__(article.gallery)</label>
                @include("{$defaultDirView}parts/multimedia", ["name" => "gallery", "folder" => 2, "selected" => $gallery, 'multiple' => true])
            </div>

            <div class="col-12 text-right mt-4">
                @control("submit")
            </div>
            @endform
        </div>
    </div>

@endsection
