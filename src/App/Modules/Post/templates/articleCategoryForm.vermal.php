@extends('layout')

@section('body')


    <div class="container">
        <div class="card shadow-lg m-b-30 p-4">
            <ul class="nav nav-tabs tab-line" id="myTab" role="tablist">
                @foreach($languages as $lang)
                    <li class="nav-item">
                        <a class="nav-link {{ $loop->first ? "show active" : "" }}" id="tab-{{ $lang }}" data-toggle="tab" href="#{{ $lang }}" role="tab" aria-controls="{{ $lang }}" aria-selected="true">
                            {{ strtoupper($lang) }}
                        </a>
                    </li>
                @endforeach
            </ul>
            @form("article-category")
                <div class="col-12 tab-content pt-4" id="myTabContent">

                    @foreach($languages as $lang)

                        <div class="tab-pane fade {{ $loop->first ? "show active" : "" }}" id="{{ $lang }}" role="tabpanel" aria-labelledby="tab-{{ $lang }}">
                            <div class="form-group">
                                @label("{$lang}_title")
                                @control("{$lang}_title")
                            </div>
                        </div>

                    @endforeach

                </div>

                <div class="col-12 text-right mt-4">
                    @control("submit")
                </div>
            @endform
        </div>
    </div>

@endsection
