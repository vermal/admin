<?php

namespace Vermal\Admin\Modules\Post\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Model;
use Vermal\Admin\Modules\Post\Entities\Extensions\Article;

/**
 * @ORM\Entity @ORM\Table(name="content_post")
 * @ORM\HasLifecycleCallbacks
 **/
class Post extends Model
{
    use Article;

    /** @ORM\Column(type="string") **/
    protected $sort;

    /** @ORM\Column(type="string") **/
    protected $type;

    /**
     * @ORM\OneToMany(targetEntity="\PostTr", mappedBy="post", orphanRemoval=true, cascade={"persist", "remove"})
     */
    protected $tr;

    /**
     * @ORM\ManyToOne(targetEntity="\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     */
    protected $user;

    /**
     * @ORM\ManyToMany(targetEntity="\PostMeta", inversedBy="post")
     * @ORM\JoinTable(name="content_post_postmeta")
     */
    protected $meta;

    /**
     * @ORM\ManyToOne(targetEntity="\Multimedia")
     * @ORM\JoinColumn(name="multimedia_id", referencedColumnName="id", nullable=true)
     */
    protected $simpleMultimedia;

    /**
     * @ORM\ManyToMany(targetEntity="\Multimedia", cascade={"persist"})
     * @ORM\JoinTable(name="content_post_multimedia",
     *      joinColumns={@ORM\JoinColumn(name="post_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="multimedia_id", referencedColumnName="id", unique=true)}
     *      )
     */
    protected $multimedia;

    public function __construct() {
        parent::__construct();
        $this->meta = new ArrayCollection();
        $this->multimedia = new ArrayCollection();
        $this->tr = new ArrayCollection();
    }

    /**
     * Clear translation
     */
    public function clearTr()
    {
        $this->tr->clear();
    }

    /**
     * @param PostTr $tr
     */
    public function addTr(PostTr $tr)
    {
        $this->tr[] = $tr;
    }

    /**
     * @param $lang
     * @throws \ErrorException
     * @return PostTr
     */
    public function getTr($lang)
    {
        foreach ($this->tr as $tr) {
            if ($tr->lang == $lang) return $tr;
        }
        return new PostTr();
        //throw new \ErrorException('Oops');
    }

    /**
     * Clear Meta data
     *
     * @param $key
     */
    public function clearMetaKeys($key)
    {
        foreach ($this->meta as $meta) {
            if ($meta->key === $key) {
                $this->meta->removeElement($meta);
            }
        }
    }

    /**
     * @param $postMeta
     */
    public function addMeta($postMeta)
    {
        $this->meta[] = $postMeta;
    }

    /**
     * Get meta data
     *
     * @param $key
     * @return array $metas
     */
    public function getMeta($key)
    {
        $metas = [];
        foreach ($this->meta as $meta) {
            if ($meta->key === $key) {
                $metas[] = $meta;
            }
        }
        return $metas;
    }

    /**
     * Add new image
     *
     * @param $multimedia
     */
    public function addMultimedia($multimedia)
    {
        $this->multimedia[] = $multimedia;
    }

    /**
     * Clear multimedia
     */
    public function clearMultimedia()
    {
        $this->multimedia->clear();
    }

}
