<?php

namespace Vermal\Admin\Modules\Post\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Database\MagicAccessor;

/**
 * @ORM\Entity @ORM\Table(name="content_postmeta_tr")
 **/
class PostMetaTr
{
    use MagicAccessor;

    /** @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue **/
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $lang;

    /** @ORM\Column(type="string") **/
    protected $value;

    /**
     * @ORM\ManyToOne(targetEntity="\PostMeta", inversedBy="tr", cascade={"persist"})
     * @ORM\JoinColumn(name="postmeta_id", referencedColumnName="id")
     */
    protected $postMeta;

}
