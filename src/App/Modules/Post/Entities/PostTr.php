<?php

namespace Vermal\Admin\Modules\Post\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Database\MagicAccessor;

/**
 * @ORM\Entity @ORM\Table(name="content_post_tr")
 **/
class PostTr
{
    use MagicAccessor;

    /** @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue **/
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $lang;

    /** @ORM\Column(type="string", nullable=true) **/
    protected $title;

    /** @ORM\Column(type="string", nullable=true) **/
    protected $headline;

    /** @ORM\Column(type="text", nullable=true) **/
    protected $te;

    /** @ORM\Column(type="string", nullable=true) **/
    protected $keywords;

    /** @ORM\Column(type="string", nullable=true) **/
    protected $slug;

    /**
     * @ORM\ManyToOne(targetEntity="\Post", inversedBy="tr", cascade={"persist"})
     * @ORM\JoinColumn(name="post_id", referencedColumnName="id")
     */
    protected $post;

}
