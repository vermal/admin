<?php

namespace Vermal\Admin\Modules\Post\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Database\MagicAccessor;

/**
 * @ORM\Entity @ORM\Table(name="content_postmeta")
 **/
class PostMeta
{
    use MagicAccessor;

    /** @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue **/
    protected $id;

    /** @ORM\Column(type="string", name="key_") **/
    protected $key;

    /** @ORM\Column(type="string") **/
    protected $value;

    /**
     * @ORM\OneToMany(targetEntity="\PostMetaTr", mappedBy="postMeta", orphanRemoval=true, cascade={"persist", "remove"})
     */
    protected $tr;

    /**
     * @ORM\ManyToMany(targetEntity="\Post", mappedBy="meta", orphanRemoval=true, cascade={"persist", "remove"})
     */
    protected $post;

    public function __construct() {
        $this->post = new ArrayCollection();
    }

    /**
     * Clear translation
     */
    public function clearTr()
    {
        $this->tr->clear();
    }

    /**
     * @param PostMetaTr $tr
     */
    public function addTr(PostMetaTr $tr)
    {
        $this->tr[] = $tr;
    }

    /**
     * @param $lang
     * @throws \ErrorException
     * @return PostTr
     */
    public function getTr($lang)
    {
        foreach ($this->tr as $tr) {
            if ($tr->lang == $lang) return $tr;
        }
        throw new \ErrorException('Oops');
    }
}
