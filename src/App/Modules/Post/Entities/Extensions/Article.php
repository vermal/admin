<?php

namespace Vermal\Admin\Modules\Post\Entities\Extensions;

use Vermal\Admin\Modules\Post\Entities\PostMeta;

trait Article
{

    /**
     * Remove all categories
     */
    public function clearCategories()
    {
        $this->clearMetaKeys('category');
    }

    /**
     * Return all categories for article
     *
     * @return mixed
     */
    public function getCategories()
    {
        return $this->getMeta('category');
    }

    /**
     * Add new category
     *
     * @param $category
     */
    public function addCategory($category)
    {
        $this->addMeta($category);
    }

}
