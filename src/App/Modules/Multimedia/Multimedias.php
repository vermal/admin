<?php

namespace Vermal\Admin\Modules\Multimedia;

use Symfony\Component\Console\CommandLoader\FactoryCommandLoader;
use Uploadable\Fixture\Entity\Image;
use Vermal\Admin\Defaults\Controller;
use Vermal\Router;
use Vermal\Admin\View;
use Vermal\Admin\Upload;
use Vermal\Database\Database;


class Multimedias extends Controller
{
    public $defaultViewPath = false;
    private $breadCrumb = [];
    private $breadCrumbPath = "";

    public function __construct()
    {
        parent::__construct();
        $this->requiredPermission('multimedia', $this->CRUDAction);
        $this->addToBreadcrumb('multimedia', 'admin.multimedia.index');
    }

    /**
     * Browse multimedias
     */
    public function index()
    {
        $this->folder(1);
    }

    /**
     * List folder
     *
     * @param $id
     */
    public function folder($id)
    {
        $data = $this->listFodlerContent($id);
        $this->generateBreadCrumb($data['folder']);
        $this->breadCrumb = array_reverse($this->breadCrumb);

        foreach ($this->breadCrumb as $breadCrumbItem) {
            $this->breadCrumbPath .= $breadCrumbItem['path'];
        }

        View::view('multimedia', [
            'folders' => $data['folders'],
            'currentFolder' => $data['folder'],
            'items' => $data['items'],
            'breadCrumb' => $this->breadCrumb,
            'breadCrumbPath' => $this->breadCrumbPath . '/'
        ]);
    }

    /**
     * @param $post
     * @throws
     */
    public function upload($post)
    {
        $images = Upload::uploadFile('file', $post->folder_id);
        if (!empty($images)) {
            Upload::cropImages($images);
        }
    }

    /**
     * Delete file
     *
     * @param $id
     */
    public function delete($id)
    {
        // Get file
        $file = Database::Model('Multimedia')->find($id);

        // Set required permission
        if (!$this->requiredPermission('multimedia', 'delete')) {
            Router::redirect('admin.multimedia.list-folder', ['id' => $file->folder->id]);
        }

        // Delete children
        foreach ($file->children as $child) {
            Upload::deleteFile($child->getFile());
        }
        $file->clearChildren();
        Database::saveAndFlush($file);

        // Delete main file
        $deleted = Upload::deleteFile($file->getFile());

        // Delete record from database
        if ($deleted) {
            Database::Model('Multimedia')->delete($id);
        }

        // Redirect back to directory with message
        $this->alert($this->_('multimedia.deleted'), 'success');
        Router::redirect('admin.multimedia.list-folder', ['id' => $file->folder->id]);
    }

    /**
     * Rename file
     *
     * @param $post
     */
    public function rename($post)
    {
        // Get file
        $file = Database::Model('Multimedia')->find($post->id);

        // Set new data
        $file->name = $post->title;

        // Save
        Database::saveAndFlush($file);

        // Message
        $this->alert($this->_('multimedia.renamed'));
    }

    private function generateBreadCrumb($folder)
    {
        if ($folder->hasParent()) {
            $this->breadCrumb[] = [
                'name' => $folder->parent->name,
                'id' => $folder->parent->id,
                'path' => $folder->parent->path
            ];
            $this->generateBreadCrumb($folder->parent);
        }
    }

    /**
     * Return folder content
     *
     * @param integer $folder
     * @return array
     */
    public function listFodlerContent($folder)
    {
    	$folder = $this->getFolderId($folder);
        return [
            'folders' => Database::Model('MultimediaFolder')
                ->where('m.parent', '=', $folder)
                ->where('m.id', '!=', $folder)
                ->get(),
            'folder' => Database::Model('MultimediaFolder')
                ->where('m.id', '=', $folder)
                ->first(),
            'items' => Database::Model('Multimedia')
                ->where('m.folder', $folder)
                ->where('m.parent', '=', null)
                ->get()
        ];
    }


    /**
     * Get all images
     *
     * @param $folder
     * @param $limit
     * @return object
     */
    public function getImages($folder, $limit = 15)
    {
        return Database::Model('Multimedia')
            ->where('m.folder', $folder)
            ->where('m.type', 'image')
            ->where('m.parent', '=', null)
            ->order('m.id', 'DESC')
            ->paginate($limit);
    }

    public function getImagesForMultimediaSelector($folder)
    {
        $folder = $this->getFolderId($folder);
        $limit = 20;
        $images = $this->getImages($folder, $limit);

        // Check if has more
        $hasMore = false;
        $i = 0;
        foreach ($images as $image) {
            $i++;
        }
        if ($i === $limit) $hasMore = true;

        // Convert objects to array
        $images_ = [];
        foreach ($images as $key => $image) {
            $images_['items'][] = [
                'mini' => $image->getMiniFile(),
                'big' => $image->getFile(),
                'id' => $image->id
            ];
            if ($key === ($limit - 1)) break;
        }
        $images_['hasMore'] = $hasMore;

        // Return json
        echo json_encode($images_);
    }

	/**
	 * Get folder id
	 *
	 * @param string|int $folderId
	 * @return mixed
	 */
    private function getFolderId($folderId) {
    	$folder = $folderId;
		if (!is_numeric($folderId)) {
			$folder = Database::Model('MultimediaFolder')
				->where('m.name', $folder)->first();
			if (!empty($folder)) return $folder->id;
			else return null;
		} else return $folder;
	}

}
