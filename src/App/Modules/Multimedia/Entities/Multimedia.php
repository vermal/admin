<?php

namespace Vermal\Admin\Modules\Multimedia\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Model;
use Vermal\Database\Entity;

/**
 * @ORM\Entity @ORM\Table(name="multimedia")
 * @ORM\HasLifecycleCallbacks
 **/
class Multimedia extends Model
{
    /** @ORM\Column(type="string", nullable=true) **/
    protected $name;

    /** @ORM\Column(type="string", name="encrypted_name", nullable=true) **/
    protected $encryptedName;

    /** @ORM\Column(type="string", nullable=true) **/
    protected $extension;

    /** @ORM\Column(type="string", nullable=true) **/
    protected $type;

    /** @ORM\Column(type="integer", nullable=true) **/
    protected $size;

    /** @ORM\Column(type="integer", nullable=true) **/
    protected $width;

    /** @ORM\Column(type="integer", nullable=true) **/
    protected $height;

    /**
     * @ORM\ManyToOne(targetEntity="\MultimediaFolder", inversedBy="items")
     * @ORM\JoinColumn(name="folder_id", referencedColumnName="id")
     */
    protected $folder;

    /**
     * @ORM\OneToMany(targetEntity="\Multimedia", mappedBy="parent", orphanRemoval=true, cascade={"persist", "remove"})
     */
    protected $children;

    /**
     * @ORM\ManyToOne(targetEntity="\Multimedia", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    protected $parent;

    public function __construct()
    {
        parent::__construct();
        $this->children = new ArrayCollection();
        $this->folder = Entity::getEntity('MultimediaFolder');
    }

    /**
     * @param Multimedia $multimedia
     */
    public function addChildren(Multimedia $multimedia)
    {
        $this->children[] = $multimedia;
    }

    /**
     * Clear children
     */
    public function clearChildren()
    {
        $this->children->clear();
    }


    /**
     * @return string
     */
    public function getFile()
    {
        $this->findAllFolderParents($this->folder);
        return 'public/uploads' . $this->folder->getPathFullToParent() . $this->encryptedName . '.' . $this->extension;
    }

	/**
	 * @return string
	 */
	public function getPath()
	{
		return $this->getFile();
	}

    /**
     * Get mini file
     *
     * @return string
     */
    public function getMiniFile()
    {
        if ($this->type === 'image' && !$this->children->isEmpty()) {
            foreach ($this->children as $child) {
                $backup = $child;
                if ($child->width === 250 && $child->heigh === 250) {
                    return $child->getFile();
                }
            }
            return $backup->getFile();
        } else return $this->getFile();
    }

    /**
     * Find all parents
     *
     * @param $folder
     */
    public function findAllFolderParents($folder)
    {
        if ($folder->hasParent()) {
            $this->findAllFolderParents($folder->parent);
        }
    }
}
