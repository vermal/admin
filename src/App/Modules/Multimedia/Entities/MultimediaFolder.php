<?php

namespace Vermal\Admin\Modules\Multimedia\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Model;

/**
 * @ORM\Entity @ORM\Table(name="multimedia_folder")
 * @ORM\HasLifecycleCallbacks
 **/
class MultimediaFolder extends Model
{
    /** @ORM\Column(type="string") **/
    protected $name;

    /** @ORM\Column(type="string") **/
    protected $path;

    /**
     * @ORM\OneToMany(targetEntity="\MultimediaFolder", mappedBy="folder")
     */
    protected $items;

    /**
     * @ORM\OneToMany(targetEntity="\MultimediaFolder", mappedBy="parent")
     */
    protected $children;

    /**
     * @ORM\ManyToOne(targetEntity="\MultimediaFolder", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true)
     */
    protected $parent;

    public function __construct()
    {
        parent::__construct();
        $this->children = new ArrayCollection();
        $this->items = new ArrayCollection();
    }

    /**
     * @return bool
     */
    public function hasChildren()
    {
        if (!$this->children->isEmpty()) return true;
        else return false;
    }

    /**
     * @return bool
     */
    public function hasItems()
    {
        if (!$this->items->isEmpty()) return true;
        else return false;
    }

    /**
     * @return bool
     */
    public function hasParent()
    {
        if ($this->parent !== null) return true;
        return false;
    }

    public function getStructure()
    {
        if (!isset($this->structure)) {
            $this->createStructure($this);
            $this->structure = array_reverse($this->structure);
        }
        return $this->structure;
    }

    public function getPathFullToParent()
    {
        if (!isset($this->structure)) {
            $this->createStructure($this);
            $this->structure = array_reverse($this->structure);
        }
        return implode('', $this->structure) . '/';
    }

    /**
     * @param $folder
     */
    public function createStructure($folder)
    {
        if (!isset($this->structure)) {
            $this->structure = [];
            if ($folder->id !== 1)
                $this->structure[] = $folder->path;
        }
        if ($folder->hasParent() && $folder->parent->id !== 1) {
            $this->structure[] = $folder->parent->path;
            $this->createStructure($folder->parent);
        }
    }
}
