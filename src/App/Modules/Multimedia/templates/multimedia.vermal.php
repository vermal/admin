@extends('layout')

@section('body')


    <div class="container">
        <div class="card shadow-lg m-b-30 p-4 bg-gray-200">
            <div class="card-body">
                <div class="row" id="multimedia-wrapper">

                    {{-- breadcrumb --}}
                    <div class="col-12 p-0 mb-3">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb bg-gray-400">
                                @foreach($breadCrumb as $breadCrumbItem)
                                    <li class="breadcrumb-item">
                                        <a href="{{ \Vermal\Router::link('admin.multimedia.list-folder', ['id' => $breadCrumbItem["id"]]) }}">
                                            {{ $breadCrumbItem["name"] }}
                                        </a>
                                    </li>
                                @endforeach
                                <li class="breadcrumb-item active" aria-current="page">{{ $currentFolder->name }}</li>
                            </ol>
                        </nav>
                    </div>

                    {{-- List folders --}}
                    @foreach($folders as $key => $folder)
                        <div class="folder" style="flex: 0 0 120px; margin-right: 20px; max-width: 120px">
                            <div class="card m-b-30">
                                <a href="{{ routerLink('admin.multimedia.list-folder', ['id' => $folder->id]) }}"
                                   class="card-body text-center read-folder" data-folder="{{ $folder->id }}">
                                    <i class="fas fa-folder mb-2" style="font-size: 20px;"></i>
                                    <br>
                                    {{ $folder->name }}
                                    @auth('developer')
                                        <code>ID:{{ $folder->id }}</code>
                                    @endauth
                                </a>
                                <button id="btn__{{ $key }}" class="context-menu-button dropdown-toggle p-0 btn btn-link" style="position: absolute; top: 8px; right: 10px" data-toggle="dropdown">
                                    <i class="fas fa-ellipsis-v"></i>
                                </button>
                                <div class="dropdown-menu text-center" style="min-width: 130px" aria-labelledby="btn__{{ $key }}">
                                    <a class="dropdown-item text-danger" href="{{ routerLink('admin.multimedia.delete', ['id' => $folder->id]) }}">
                                        <i class="fas fa-trash mr-2"></i> delete
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    {{-- List files --}}
                    @foreach($items as $key => $item)
                        <div class="file" style="flex: 0 0 120px; margin-right: 20px; max-width: 120px; margin-bottom: 20px">
                            <div class="card bg-center" {!! "style='background: url(\"" . $item->getMiniFile() . "\") center;min-height: 80px'"  !!}>
                                <div class="position-absolute" style="height: 100%; width: 100%; background-color: rgba(0,0,0,.35)"></div>
                                <div class="card-body text-center read-folder position-relative text-white" style="z-index: 2; height: 100%">
                                    <a href="{!! $item->getFile() !!}"  target="_blank">
                                        @if($item->type !== 'image')
                                            <i class="fas fa-file mb-2" style="font-size: 20px;"></i>
                                            <br>
                                        @endif
                                        {!! $item->name !!}
                                    </a>
                                    <button id="btn_{{ $key }}" class="context-menu-button dropdown-toggle p-0 btn btn-link" style="position: absolute; top: 8px; right: 10px" data-toggle="dropdown">
                                        <i class="fas fa-ellipsis-v"></i>
                                    </button>
                                    {{-- TODO: multiple items selection --}}
                                    {{--<div class="custom-control custom-checkbox" style="position: absolute; top: 35px; right: 3px">--}}
                                        {{--<input type="checkbox" class="custom-control-input" id="customCheck1">--}}
                                        {{--<label class="custom-control-label" for="customCheck1"></label>--}}
                                    {{--</div>--}}
                                    <div class="dropdown-menu text-center" style="min-width: 130px" aria-labelledby="btn_{{ $key }}">
                                        <button class="dropdown-item edit-item"
                                                data-toggle="modal" data-target="#editMultimediaItem" data-id="{{ $item->id }}" data-title="{{ $item->name }}">
                                            <i class="fas fa-edit"></i> rename
                                        </button>
                                        <button type="button" class="dropdown-item delete-item"
                                                data-toggle="modal" data-target="#modalConfirmation" data-href="{{ routerLink('admin.multimedia.delete', ['id' => $item->id]) }}">
                                            <i class="fas fa-trash mr-2"></i> delete
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        {{-- Upload --}}
        <div class="card shadow-lg m-b-30 p-4 bg-gray-200">
            <div class="card-header">
                <h5 class="m-b-0">
                    Upload your files
                </h5>
                <p class="m-b-0 text-muted">
                    Currently you are uploading files to &nbsp;&nbsp;
                    <code>
                        @foreach($breadCrumb as $breadCrumbItem)
                            {{ $breadCrumbItem["name"] }} /
                        @endforeach
                        {{ $currentFolder->name }} /
                    </code>
                </p>
            </div>
            <div class="card-body">
                <form class="dropzone" action="{{ \Vermal\Router::link('admin.multimedia.upload') }}">
                    <input type="hidden" name="folder_id" value="{{ $currentFolder->id }}">
                    <div class="dz-message">
                        <h1 class="display-4">
                            <i class=" mdi mdi-progress-upload"></i>
                        </h1>
                        Drop files here or click to upload.<br>
                        <div class="p-t-5">
                            <a class="btn btn-lg btn-primary">Upload File</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- Rename --}}
    <div class="modal fade" id="editMultimediaItem" data-backdrop="static"  tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form id="multimediaForm" class="ajax" action="{!! routerLink('admin.multimedia.rename') !!}" method="POST">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" id="id" name="id">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">
                            <i class="fas fa-edit"></i>
                            @__(multimedia.modal.heading)
                        </h5>
                        <a class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </a>
                    </div>
                    <div class="modal-body">
                        {{-- Title --}}
                        <div class="form-group">
                            <label for="title">@__(multimedia.modal.title)</label>
                            <input type="text" id="title" name="title" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="#" class="btn btn-secondary modal-close" data-dismiss="modal">@__(global.modal.close)</a>
                        <button type="button" class="btn btn-primary save">@__(global.modal.save)</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- Confirm delete --}}
    <div class="modal fade" id="modalConfirmation" data-backdrop="static"  tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog  modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-body">
                    <div class="px-3 pt-3 text-center">
                        <div class="event-type warning">
                            <div class="event-indicator ">
                                <i class="mdi mdi-exclamation text-white" style="font-size: 60px"></i>
                            </div>
                        </div>
                        <h3 class="pt-3">@__(multimedia.confirm.heading)</h3>
                        <p class="text-muted">@__(multimedia.confirm.text)</p>
                    </div>
                </div>
                <div class="modal-footer ">
                    <a href="#" id="okay" class="btn btn-warning">@__(global.modal.okay)</a>
                    <a href="#" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">@__(global.modal.close)</a>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('.edit-item').click(function() {
                let modal = $('#editMultimediaItem');
                modal.find('#id').val($(this).data('id'));
                modal.find('#title').val($(this).data('title'));
            });
            $('.delete-item').click(function() {
                 let modal = $('#modalConfirmation');
                 modal.find('#okay').attr('href', $(this).data('href'));
            });
            $(".file").contextmenu(function(e) {
                e.preventDefault();
                $(e.target).closest('.file').find('.context-menu-button').click();
            });
        });
    </script>

@endsection
