<?php

namespace Vermal\Admin\Modules\Menu;

use Vermal\Admin\Defaults\Controller;
use Vermal\Admin\Modules\Auth\Auth;
use Vermal\Admin\Modules\Menu\Entities\MenuItem;
use Vermal\Database\Database;
use Vermal\Database\Entity;
use Vermal\Router;
use Vermal\Admin\View;


class MenuBuilder extends Controller
{
    protected $defaultViewPath = false;

    public function __construct()
    {
        parent::__construct();
        $this->requiredPermission('menu', 'update');
        $this->addToBreadcrumb('menu', 'admin.menu.index');
    }

    /**
     * Show menu builder
     *
     * @param integer $id
     * @throws
     */
    public function show($id)
    {
        $menu = Database::Model('Menu')->find($id);

        // Edit admin menu is allowed only for developer
        if ($menu->name === 'admin' && !Auth::hasRole('developer')) {
            Router::redirect('admin.menu.index');
        }

        $items_ = Database::Model('MenuItem')
            ->order('m.order', 'ASC')
            ->where('m.parent', '=', null)
            ->where('m.menu', $id)
            ->get();
        $items = self::toArray($items_);

        View::view('builder', [
            'menu' => $menu,
            'data' => json_encode($items),
            'resources' => Database::Model('Resource')->get()
        ]);
    }

    /**
     * Save order
     *
     * @param $post
     * @throws
     */
    public function save($post)
    {
        // Recursive save menu items
        $this->saveMenu(json_decode($post->data));
        // Flush changes to database
        Database::$em->flush();
        // Pass alert
        $this->alert($this->_('menu.updated_order'), 'success');
    }

    /**
     * Recursive function to save menu items
     *
     * @param $data
     * @param null $parent
     */
    private function saveMenu($data, $parent = null)
    {
        $order = -1;
        foreach($data as $k => $v) {
            $order++;
            $item = Database::MenuItem()->find($v->id_);

            // Check if item has children
            if (!empty($v->children)) {
                $item->order = $order;

                // Set parent if exist
                if ($parent !== null) {
                    $item->parent = $parent;
                } else {
                    $item->parent = null;
                }

                Database::save($item);
                $this->saveMenu($v->children, $item);
            } else {
                $item->order = $order;

                // Set parent if exist
                if ($parent !== null) {
                    $item->parent = $parent;
                } else {
                    $item->parent = null;
                }

                Database::save($item);
            }
        }
    }

    /**
     * Update menu item
     *
     * @param $post
     */
    public function update($post)
    {
        if ($post->id !== "") {
            $menuItem = Database::MenuItem()->find($post->id);
        } else {
            $menuItem = Entity::getEntity('MenuItem');
        }

        if (empty($post->title) ||
            $post->type == 'static' && empty($post->url) ||
            $post->type == 'dynamic' && empty($post->route)) {
            $this->alert($this->_('menu.empty'), 'danger');
            die();
        }

        $menuItem->title = $post->title;
        $menuItem->type = $post->type;
        $menuItem->url = $post->url;
        $menuItem->route = $post->route;
        $menuItem->parameters = $post->parameters;
        $menuItem->iconClass = $post->iconClass;
        $menuItem->target = $post->target;
        $menuItem->note = (isset($post->note) ? $post->note : '');

        if ($post->id !== "") {
            Database::saveAndFlush($menuItem);
        } else {
            $menu = Database::Menu()->find($post->menu_id);
            $menuItem->menu = $menu;
            $menuItem->order = 0;
            $menuItem = Database::saveAndFlush($menuItem);
        }

        $this->alert($this->_('menu.updated'),  'success', ['id' => $menuItem->id]);
    }

    /**
     * Array to object
     *
     * @param $items
     * @return array
     */
    private static $ids = [];
    private static function toArray($items) {
        $array = [];
        foreach($items as $k => $v) {
            if(!$v->children->isEmpty() && !in_array($v->id, self::$ids)) {
                self::$ids[] = $v->id;
                self::setData($v, $array[$k]);
                $array[$k]['children'] = self::toArray($v->children);
            } else if (!in_array($v->id, self::$ids)) {
                self::setData($v, $array[$k]);
                self::$ids[] = $v->id;
            }
        }
        return $array;
    }

    /**
     * Helper method to setting data
     *
     * @param $item
     * @param $array
     */
    private static function setData($item, &$array)
    {
        $data = [];
        $data['id_'] = $item->id;
        $data['title'] = $item->title;
        $data['type'] = $item->type;
        $data['url'] = $item->url;
        $data['route'] = $item->route;
        $data['parameters'] = $item->parameters;
        $data['icon'] = $item->iconClass;
        $data['target'] = $item->target;
        $data['note'] = $item->note;
        $array= $data;
    }

    /**
     * Delete menu item
     *
     * @param $post
     */
    public function delete($post)
    {
        Database::MenuItem()->delete($post->id);
        $this->alert($this->_('menu.deleted'),  'success');
    }

    /**
     * Build menu
     *
     * @param $menu
     * @return string
     */
    public static function buildMenu($menu)
    {
        return View::view('menu', [
            'menu' => $menu
        ], true);
    }

}
