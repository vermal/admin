<?php

namespace Vermal\Admin\Modules\Menu\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Model;

/**
 * @ORM\Entity @ORM\Table(name="menu_item")
 * @ORM\HasLifecycleCallbacks
 **/
class MenuItem extends Model
{
    /** @ORM\Column(type="string") **/
    protected $title;

    /** @ORM\Column(type="string", name="type_") **/
    protected $type;

    /** @ORM\Column(type="string", nullable=true) **/
    protected $url;

    /** @ORM\Column(type="string", nullable=true) **/
    protected $route;

    /** @ORM\Column(type="text", nullable=true) **/
    protected $parameters;

    /** @ORM\Column(type="string") **/
    protected $target;

    /** @ORM\Column(type="string", name="icon_class", nullable=true) **/
    protected $iconClass;

    /** @ORM\Column(type="string", nullable=true) **/
    protected $note;

    /** @ORM\Column(type="integer", name="sort") **/
    protected $order;

    /**
     * @ORM\OneToMany(targetEntity="\MenuItem", mappedBy="parent")
     * @ORM\OrderBy({"order" = "ASC"})
     */
    protected $children;

    /**
     * @ORM\ManyToOne(targetEntity="\MenuItem", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true)
     */
    protected $parent;

    /**
     * @ORM\ManyToOne(targetEntity="\Menu", inversedBy="items", cascade={"persist"})
     * @ORM\JoinColumn(name="menu_id", referencedColumnName="id")
     */
    protected $menu;

    public function __construct()
    {
        parent::__construct();
        $this->children = new ArrayCollection();
    }

    /**
     * Add child
     *
     * @param $child
     */
    public function addChild(MenuItem $child)
    {
        $this->children[] = $child;
    }
}
