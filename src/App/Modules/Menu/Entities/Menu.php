<?php

namespace Vermal\Admin\Modules\Menu\Entities;

use Vermal\Admin\Defaults\Model;
use Vermal\Database\MagicAccessor;

/**
 * @ORM\Entity @ORM\Table(name="menu")
 * @ORM\HasLifecycleCallbacks
 **/
class Menu extends Model
{
    /** @ORM\Column(type="string") **/
    protected $name;

    /** @ORM\Column(type="string", name="display_name") **/
    protected $displayName;

    /** @ORM\Column(type="string", name="icon_class") **/
    protected $iconClass;

    /**
     * @ORM\OneToMany(targetEntity="\MenuItem", mappedBy="menu")
     * @ORM\OrderBy({"order" = "ASC"})
     */
    protected $items;

    /**
     * Add items to menu
     *
     * @param $item
     */
    public function addItem($item)
    {
        $this->items[] = $item;
    }
}
