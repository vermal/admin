<?php

namespace Vermal\Admin\Modules\Menu;


use Vermal\Admin\Defaults\Controller;
use Vermal\Admin\Modules\Auth\Auth;
use Vermal\Admin\Modules\Menu\Entities\Menu;
use Vermal\Database\Database;
use Vermal\Database\Entity;
use Vermal\DataGrid;
use Vermal\Form\Form;
use Vermal\Router;
use Vermal\Admin\View;


class Menus extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->requiredPermission('menu', $this->CRUDAction);
        $this->addToBreadcrumb('menu', 'admin.menu.index');
    }

    /**
     * Edit admin menu is allowed only for developer
     * @param $menu
     */
    private function menuOnlyForDeveloper($menu)
    {
        if ($menu->name === 'admin' && !Auth::hasRole('developer')) {
            Router::redirect('admin.menu.index');
        }
    }

    /**
     * Browse all users
     */
    public function index()
    {
        // Select menus but if role is not developer disable admin menu
        $menus = Database::Menu()->order('m.id', 'DESC');
        if (!Auth::hasRole('developer')) $menus->where('m.name', '!=', 'admin');

        // Create new data grid
        $datagrid = new DataGrid($menus);

        // Add columns
        $datagrid->addColumn('id', 'ID');
        $datagrid->addColumn('displayName', $this->_('menu.name'));

        // Add actions
        $datagrid->addAction('builder', ['admin.menu-builder.show', ['id']], 'fas fa-stream', $this->_('menu.menu_builder'));
        $datagrid->addEdit(['admin.menu.edit', ['id']]);
        $datagrid->addDelete(['admin.menu.destroy_', ['id']]);

        // Return view
        View::view('crud.read', [
            'datagrid' => $datagrid->build()
        ]);
    }

    /**
     * Show login page
     */
    public function create()
    {
        $form = $this->form();
        View::view('crud.create', [
            "form" => $form->build()
        ]);
    }

    /**
     * Login user based on his role
     *
     * @param array $post
     */
    public function store($post)
    {
        $form = $this->form();
        if ($form->hasError()) {
            Router::redirect('admin.menu.create');
        }

        // Create new menu
        $menu = Entity::getEntity('Menu');
        $menu = $form->hydrate($menu);

        Database::saveAndFlush($menu);

        // Redirect to browse view
        Router::redirect('admin.menu.index');
    }

    /**
     * Show use dit page
     *
     * @param $id
     */
    public function edit($id)
    {
        $menu = Database::Menu()->find($id);
        $this->menuOnlyForDeveloper($menu);
        $form = $this->form();

        // Add to breadcrumb
        $this->addToBreadcrumb('dyn', 'admin.menu.edit', ['id' => $id], [$menu->displayName]);

        // Set values
        $form->setValues($menu);

        // Set edit action
        $form->setAction(Router::link('admin.menu.update', ['id' => $id]));
        $form->setMethod('PUT');

        View::view('crud.edit', [
            "form" => $form->build()
        ]);
    }

    /**
     * Update user
     *
     * @param array $post
     * @param $id
     */
    public function update($post, $id)
    {
        $form = $this->form();

        // Check if for has some errors
        if ($form->hasError()) {
            Router::redirect('admin.menu.edit', ['id' => $id]);
        }

        // Create new user
        $menu = Database::Menu()->find($id);
        $this->menuOnlyForDeveloper($menu);
        $menu = $form->hydrate($menu);

        // Update user
        Database::saveAndFlush($menu);

        // Redirect to user edit
        Router::redirect('admin.menu.edit', ['id' => $id]);
    }

    /**
     * Delete menu
     *
     * @param $id
     */
    public function destroy($id)
    {
        $menu = Database::Menu()->find($id);
        $this->menuOnlyForDeveloper($menu);

        Database::MenuItem()->where('m.menu', $id)->delete();
        Database::Menu()->delete($id);
        Router::redirect('admin.menu.index');
    }

    /**
     * Create register form
     *
     * @return Form
     */
    private function form()
    {
        $form = new Form('menu', Router::link('admin.menu.store'));

        $form->addText('name', $this->_('menu.name'))->min(2);
        $form->addText('displayName', $this->_('menu.displayName'))->min(2);
        $form->addText('iconClass', $this->_('menu.iconClass'))->min(2);
        $form->addButton('submit', $this->_('menu.save'))->setClass('btn btn-success');

        return $form;
    }

}
