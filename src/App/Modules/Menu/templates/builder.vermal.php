@extends('layout')

@section('body')

    <div class="card shadow-lg m-b-30">
        <div class="card-body">
            <div class="dd" id="domenu-0" data-data='{!! $data !!}'>
                <button class="dd-new-item">+</button>
                <li class="dd-item-blueprint">
                    <div class="dd-handle dd3-handle">&nbsp;</div>
                    <div class="dd3-content">
                        <span class="item-name"></span>
                        <div class="dd-button-container">
                            <button class="btn btn-sm btn-primary edit-item" data-toggle="modal" data-target="#editItem">
                                <i class="fas fa-edit"></i> @__(menu.edit)
                            </button>
                            <button class="item-remove btn-sm btn btn-danger">
                                <i class="fas fa-trash-alt"></i> @__(menu.delete)
                            </button>
                        </div>
                        <div class="dd-edit-box" style="display: none;">
                            <input type="text" name="title">
                            <input type="hidden" name="type" value="static">
                            <input type="hidden" name="url">
                            <input type="hidden" name="route">
                            <input type="hidden" name="parameters">
                            <input type="hidden" name="icon">
                            <input type="hidden" name="target" value="_self">
                            <input type="hidden" name="note" value="">
                            <input type="hidden" name="id_">
                            <i class="end-edit">save</i>
                        </div>
                    </div>
                </li>
                <form class="ajax" id="save-order-form" action="{!! routerLink('admin.menu-builder.save') !!}" method="POST">
                    <input type="hidden" name="data" id="save-order-data">
                    <input type="hidden" id="menu_id" name="menu_id" value="{!! $menu->id !!}">
                    <button id="save-order" class="d-none"></button>
                </form>
                <ol class="dd-list"></ol>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="editItem" tabindex="-1" role="dialog" aria-labelledby="editItem" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="menuBuilderForm" class="ajax" action="{!! routerLink('admin.menu-builder.update') !!}" method="POST">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" id="id" name="id">
                    <input type="hidden" id="menu_id" name="menu_id" value="{!! $menu->id !!}">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">
                            <i class="fas fa-edit"></i>
                            @__(menu.modal_title)
                        </h5>
                        <a class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </a>
                    </div>
                    <div class="modal-body">
                        {{-- Title --}}
                        <div class="form-group">
                            <label for="title">@__(menu.title)</label>
                            <input type="text" id="title" name="title" class="form-control">
                        </div>

                        {{-- Link type --}}
                        <div class="form-group">
                            <label for="type">@__(menu.type)</label>
                            <select name="type" id="type" class="conditional form-control">
                                <option value="static">@__(menu.typeSelect.static)</option>
                                <option value="dynamic">@__(menu.typeSelect.dynamic)</option>
                            </select>
                        </div>

                        <div class="type" id="type_static">
                            <div class="form-group">
                                <label for="url">@__(menu.url)</label>
                                <input type="text" id="url" name="url" class="form-control">
                            </div>
                        </div>
                        <div class="type" id="type_dynamic">
                            <div class="form-group">
                                <label for="route">@__(menu.route)</label>
                                <input type="text" id="route" name="route" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="parameters">@__(menu.params)</label>
                            <textarea id="parameters" name="parameters" class="form-control" placeholder="{{ "{\n\t\"key\": \"value\"\n}" }}"></textarea>
                            </div>
                        </div>

                        {{-- Icon --}}
                        <div class="form-group">
                            <label for="iconClass">@__(menu.icon)</label>
                            <input type="text" id="iconClass" name="iconClass" class="form-control">
                        </div>

                        {{-- Open in --}}
                        <div class="form-group">
                            <label for="target">@__(menu.open)</label>
                            <select name="target" id="target" class="conditional form-control">
                                <option value="_self">@__(menu.openSelect._self)</option>
                                <option value="_blank">@__(menu.openSelect._blank)</option>
                            </select>
                        </div>

                        {{-- Note\Resource --}}
                        <div class="form-group">
                            <label for="note">
                                @if($menu->name !== 'admin')
                                    @__(menu.note)
                                @else
                                    @__(menu.noteAdmin)
                                @endif
                            </label>
                            @if($menu->name !== 'admin')
                                <input type="text" id="note" name="note" class="form-control">
                            @else
                                <select name="note" id="note" class="form-control">
                                    @foreach($resources as $resource)
                                        <option value="{{ $resource->id }}">{{ $resource->displayName }}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="#" class="btn btn-secondary modal-close" data-dismiss="modal">@__(global.modal.close)</a>
                        <button type="button" class="btn btn-primary save">@__(global.modal.save)</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- Delete menu item --}}
    <form id="deleteItemForm" method="POST" action="{!! routerLink('admin.menu-builder.delete') !!}" class="ajax d-none">
        <input type="hidden" name="_method" value="DELETE">
        <input type="hidden" name="id">
    </form>

@endsection
