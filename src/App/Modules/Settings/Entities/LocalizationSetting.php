<?php

namespace Vermal\Admin\Modules\Settings\Entities;

use Vermal\Admin\Defaults\Model;

/**
 * @ORM\Entity @ORM\Table(name="localization_setting")
 * @ORM\HasLifecycleCallbacks
 **/
class LocalizationSetting extends Model
{
    /** @ORM\Column(type="string") **/
    protected $lang;

    /** @ORM\Column(type="integer") **/
    protected $active;
}
