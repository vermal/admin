<?php

namespace Vermal\Admin\Modules\Settings\Entities;

use Vermal\Admin\Defaults\Model;

/**
 * @ORM\Entity @ORM\Table(name="setting")
 * @ORM\HasLifecycleCallbacks
 **/
class Setting extends Model
{
    /** @ORM\Column(type="array", nullable=true) **/
    protected $languages;

    /** @ORM\Column(type="string", nullable=true) **/
    protected $mailer_host;

    /** @ORM\Column(type="string", nullable=true) **/
    protected $mailer_port;

    /** @ORM\Column(type="string", nullable=true) **/
    protected $mailer_name;

    /** @ORM\Column(type="string", nullable=true) **/
    protected $mailer_username;

    /** @ORM\Column(type="string", nullable=true) **/
    protected $mailer_password;

    /** @ORM\Column(type="string", nullable=true) **/
    protected $mailer_encryption;
}
