@extends('layout')

@section('body')

    <div class="container">
        <div class="card shadow-lg m-b-30 p-4">
            <ul class="nav nav-tabs tab-line" id="myTab" role="tablist">
                @foreach($tabs as $key => $tab)
                    <li class="nav-item">
                        <a class="nav-link {{ $loop->first ? 'active' : '' }}" id="tab-{{ $key }}" data-toggle="tab" href="#{{ $key }}" role="tab" aria-controls="{{ $key }}" aria-selected="true">
                            {{ $tab }}
                        </a>
                    </li>
                @endforeach
            </ul>
            @form("settings")
                <div class="col-12 tab-content pt-4" id="myTabContent">

                    @foreach($tabs as $key => $tab)

                        <div class="tab-pane {{ $loop->first ? 'show active' : '' }}" id="{{ $key }}" role="tabpanel" aria-labelledby="tab-{{ $key }}">
                            <div class="row">

                                @if(isset($fields[$key]))
                                    @foreach($fields[$key] as $field)
                                        @label($field)
                                        @control($field)
                                    @endforeach
                                @endif

                            </div>
                        </div>

                    @endforeach

                </div>

                <div class="col-12 text-right mt-4">
                    @control("submit")
                </div>
            @endform
        </div>
    </div>


    <script>
        $(document).ready(function() {
            $('#form-settings').on('fields-error', function(res, data) {
                let tabId = data.fGroup.closest('.tab-pane').attr('id');
                $('#tab-' + tabId).click();
            });
            $('.nav-tabs .nav-link').click(function() {
                setTimeout(function() {
                    initPlugins();
                }, 10);
            });
        });
    </script>

@endsection
