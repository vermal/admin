<?php

namespace Vermal\Admin\Modules\Settings;


use Vermal\Admin\Defaults\Controller;
use Vermal\Admin\Defaults\Currencies;
use Vermal\Admin\Defaults\Languages;
use Vermal\Database\Database;
use Vermal\Database\Entity;
use Vermal\DataGrid;
use Vermal\Form\Form;
use Vermal\Router;
use Vermal\Admin\View;


class Settings extends Controller
{
    protected $defaultViewPath = false;

    public function __construct()
    {
        parent::__construct();
        $this->requiredPermission('settings', $this->CRUDAction);
        $this->addToBreadcrumb('settings', 'admin.settings.index');
        do_action('Settings');
    }

    /**
     * Browse all settings
     */
    public function index()
    {
        // Return view
        $form = $this->form();

        // Set default values
        $settings = Database::Model('Setting')->find(1);
        $form->setValues($settings, apply_filters('settings_fields_exclude', []));
		$form = apply_filters('settings_form_values', $form, $settings);

        // Define tabs
        $tabs = [
            'default' => 'Default settings',
            'mailer' => 'SMTP Mailer'
        ];
        $tabs = apply_filters('settings_tabs', $tabs);

        // Define fields
        $fields = [];
        $fields['default'] = ['languages'];
        $fields['mailer'] = [
            'mailer_host', 'mailer_port', 'mailer_name', 'mailer_encryption', 'mailer_username', 'mailer_password'
        ];
        $fields = apply_filters('settings_fields', $fields);

        // Build form
        $form->build();
        $this->canCreate = false;
        View::view('settings', [
            'tabs' => $tabs,
            'fields' => $fields
        ]);
    }


    /**
     * Update settings
     *
     * @param array $post
     */
    public function store($post)
    {
        $form = $this->form();

        // Check if for has some errors
        if ($form->hasError()) {
            $this->alert($this->_('error.form.wrong'), 'warning');
            die();
        }

        // Get setting
        $settings = Database::Model('Setting')->find(1);

        // Store mailer settings
        $settings->mailer_host = $post->mailer_host;
        $settings->mailer_port = $post->mailer_port;
        $settings->mailer_name = $post->mailer_name;
        $settings->mailer_encryption = $post->mailer_encryption;
        $settings->mailer_username = $post->mailer_username;
        $settings->mailer_password = $post->mailer_password;

        // Set arrays
        $settings->languages = (array)$post->languages;

        // Store custom fields
        $settings = apply_filters('settings_store', $settings, $post);

        // Update settings
        Database::saveAndFlush($settings);
        $this->alert($this->_('settings.edited'), 'success');

        // Create/Update localization settings based on active languages
        $this->localizationSettings($post);
    }

    /**
     * @param $post
     */
    private function localizationSettings($post)
    {
        // Create localiazionSettings based on languages
        $localizationSettings = Database::Model('LocalizationSetting')->get();

        // Disable/Enable localization
        foreach ($localizationSettings as $localizationSetting) {
            if (!in_array($localizationSetting->lang, (array)$post->languages)) {
                $localizationSetting->active = 0;
            } else {
                $localizationSetting->active = 1;
            }
            Database::saveAndFlush($localizationSetting);
        }

        // Create new localization
        foreach ((array)$post->languages as $lang) {
            $localizationSetting = null;
            foreach ($localizationSettings as $row) {
                if ($row->lang == $lang) {
                    $localizationSetting = $row;
                }
            }

            // Create new localization setting
            if (empty($localizationSetting)) {
                $localizationSetting = Entity::getEntity('LocalizationSetting');
                $localizationSetting->lang = $lang;
                $localizationSetting->active = 1;
                Database::saveAndFlush($localizationSetting);
            }
        }
    }

    /**
     * Create register form
     *
     * @return Form
     */
    private function form()
    {
        $form = new Form('settings', Router::link('admin.setting.store'));
        $form->setAjax(true);

        // Default settings
        $form->addSelect('languages', Languages::available(), 'Languages')
            ->addAttribute('multiple', 'multiple')
            ->setClass('js-select2 allow-tags');

        // Mailer
        $form->addText('mailer_host', 'Host')->setCol('col-md-6');
        $form->addText('mailer_port', 'Port')->setCol('col-md-2');
        $form->addText('mailer_name', 'Name')->setCol('col-md-4');
        $form->addSelect('mailer_encryption', ['ssl' => 'SSL', 'tls' => 'TLS'], 'Encryption');
        $form->addText('mailer_username', 'Username');
        $form->addText('mailer_password', 'Password');

        // Allow form extension
        $form = apply_filters('settings_form', $form);

        // Button
        $form->addButton('submit', $this->_('menu.save'))->setClass('btn btn-success');

        return $form;
    }

    /**
     * @param array $array
     * @return array
     */
    private function addValueToArrayKeys($array)
    {
        $newArray = [];
        foreach ($array as $key => $value) {
            $newArray[slugify($value)] = $value;
        }
        return $newArray;
    }

}
