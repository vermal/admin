<?php

namespace Vermal\Admin\Modules\Settings;


use Vermal\Admin\Defaults\Controller;
use Vermal\Admin\Defaults\Currencies;
use Vermal\Admin\Defaults\Languages;
use Vermal\Database\Database;
use Vermal\DataGrid;
use Vermal\Form\Form;
use Vermal\Router;
use Vermal\Admin\View;


class LocalizationSettings extends Controller
{
//    protected $defaultViewPath = false;

    public function __construct()
    {
        parent::__construct();
        $this->requiredPermission('settings', $this->CRUDAction);
        $this->addToBreadcrumb('localizationSettings', 'admin.localization-settings.index');
        do_action('LocalizationSettings');
    }

    /**
     * Browse all settings
     */
    public function index()
    {
        // Return view
        $form = $this->form();

        // Get settings
        $settings_ = Database::Model('LocalizationSetting')->where('l.active', 1)->get();
        $settings = [];
        foreach ($settings_ as $setting) {
            $settings[$setting->lang] = $setting;
        }

        // Define tabs
        $tabs = $this->addValueToArrayKeys(Languages::get());
        $tabs = apply_filters('localization_settings_tabs', $tabs);

        // Define fields
        $fields = [];
        $fields = apply_filters('localization_settings_fields', $fields);

        // Set values
		foreach ($fields as $field) {
			foreach (Languages::get() as $lang) {
				if ($settings[$lang]->{$field} !== null)
					$form->getComponent($lang . '_' . $field)->setValue($settings[$lang]->{$field});
			}
		}

        // Build form
//        $form->build();
        View::view('crud.createWithTr', [
			"form" => $form->build(),
        	'formName' => $form->getName(),
            'fields' => $fields
        ]);
    }


    /**
     * Update settings
     *
     * @param array $post
     */
    public function store($post)
    {
        $form = $this->form();

        // Check if for has some errors
        if ($form->hasError()) {
            $this->alert($this->_('error.form.wrong'), 'warning');
            die();
        }

        // Get setting
        $settings = Database::Model('LocalizationSetting')->where('l.active', 1)->get();

        // Divide post data by language
        $data = [];
        foreach (Languages::get() as $lang) {
            $data[$lang] = new \stdClass();
            foreach ($post as $key => $value) {
                if (strpos($key, $lang . '_') !== false) {
                    $k = str_replace($lang . '_', '', $key);
                    $data[$lang]->{$k} = $value;
                }
            }
        }

        // Store settings
        foreach ($settings as $setting) {
            // Store custom fields
            $setting = apply_filters('localization_settings_store', $setting, $data[$setting->lang]);
            Database::save($setting);
        }

        // Update settings
        Database::flush();
        $this->alert($this->_('settings.edited'), 'success');
    }

    /**
     * Create register form
     *
     * @return Form
     */
    private function form()
    {
        $form = new Form('settings', Router::link('admin.localization-settings.store'));
        $form->setAjax(true);

        // Allow form extension
        $form = apply_filters('localization_settings_form', $form);

        // Assign languages for components
        foreach ($form->components as $name => $component) {
            foreach (Languages::get() as $lang) {
                $c = $component;
                $c['name'] = $lang . '_' . $c['name'];
                $c['attr']['id'] = $lang . '_' . $c['attr']['id'];
                $form->components[$lang . '_' . $name] = $c;
            }
            unset($form->components[$name]);
        }

        // Button
        $form->addButton('submit', $this->_('menu.save'))->setClass('btn btn-success');

        return $form;
    }

    /**
     * @param array $array
     * @return array
     */
    private function addValueToArrayKeys($array)
    {
        $newArray = [];
        foreach ($array as $key => $value) {
            $newArray[slugify($value)] = strtoupper($value);
        }
        return $newArray;
    }

}
