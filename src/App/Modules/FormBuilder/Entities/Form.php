<?php

namespace Vermal\Admin\Modules\FormBuilder\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Model;

/**
 * @ORM\Entity @ORM\Table(name="form")
 * @ORM\HasLifecycleCallbacks
 **/
class Form extends Model
{
    /** @ORM\Column(type="string") **/
    protected $title;

    /** @ORM\Column(type="string") **/
    protected $slug;

    /**
     * @ORM\OneToMany(targetEntity="\FormItem", mappedBy="form", orphanRemoval=true, cascade={"persist", "remove"})
     * @ORM\OrderBy({"order" = "ASC"})
     */
    protected $items;

    /** @ORM\Column(type="text", name="form_builder") **/
    protected $formBuilder;

    public function __construct() {
        parent::__construct();
        $this->items = new ArrayCollection();
    }

    /**
     * Clear items
     */
    public function clearItems()
    {
        $this->items->clear();
    }

    /**
     * @param FormItem $formItem
     */
    public function addTr(FormItem $formItem)
    {
        $this->items[] = $formItem;
    }

}
