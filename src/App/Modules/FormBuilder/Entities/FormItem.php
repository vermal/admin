<?php

namespace Vermal\Admin\Modules\FormBuilder\Entities;

use Vermal\Database\MagicAccessor;

/**
 * @ORM\Entity @ORM\Table(name="form_item")
 * @ORM\HasLifecycleCallbacks
 **/
class FormItem
{
    use MagicAccessor;

    /** @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue **/
    protected $id;

    /** @ORM\Column(type="string") **/
    protected $uid;

    /** @ORM\Column(type="string") **/
    protected $el;

    /** @ORM\Column(type="string") **/
    protected $title;

    /** @ORM\Column(type="string", nullable=true) **/
    protected $placeholder;

    /** @ORM\Column(type="integer") **/
    protected $col;

    /** @ORM\Column(type="string") **/
    protected $name;

    /** @ORM\Column(type="array", nullable=true) **/
    protected $choices;

    /** @ORM\Column(type="array", name="choice_values", nullable=true) **/
    protected $choiceValues;

    /** @ORM\Column(type="string", name="choice_with_custom_value", nullable=true) **/
    protected $choiceWithCustomValue;

    /** @ORM\Column(type="text", nullable=true) **/
    protected $value;

    /** @ORM\Column(type="text", nullable=true) **/
    protected $description;

    /** @ORM\Column(type="integer", nullable=true, name="sort") **/
    protected $order;

    /**
     * @ORM\ManyToOne(targetEntity="\Form", inversedBy="items")
     * @ORM\JoinColumn(name="form_id", referencedColumnName="id")
     */
    protected $form;

}
