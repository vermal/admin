<?php

namespace Vermal\Admin\Modules\FormBuilder;


use Vermal\Admin\Defaults\Controller;
use Vermal\Admin\Defaults\Languages;
use Vermal\Admin\Modules\FormBuilder\Entities\Form;
use Vermal\Admin\Modules\FormBuilder\Entities\FormItem;
use Vermal\Admin\Sanitizer;
use Vermal\Database\Database;
use Vermal\Database\Entity;
use Vermal\DataGrid;
use Vermal\Router;
use Vermal\Admin\View;


class Forms extends Controller
{
    protected $defaultViewPath = false;

    public function __construct()
    {
        parent::__construct();
//        $this->requiredPermission('form', $this->CRUDAction);
        $this->addToBreadcrumb('form_builder', 'admin.form_builder.index');
    }

    /**
     * Browse all users
     */
    public function index()
    {
        $this->setDefaultViewPath();

        // Create new data grid
        $datagrid = new DataGrid(Database::Model('Form')->order('f.id', 'DESC'));

        // Add columns
        $datagrid->addColumn('id', 'ID');
        $datagrid->addColumn('title', $this->_('form.title'));

        // Add actions
        $datagrid->addEdit(['admin.form.edit', ['id']]);
        $datagrid->addDelete(['admin.form.destroy_', ['id']]);

        // Return view
        View::view('crud.read', [
            'datagrid' => $datagrid->build()
        ]);
    }

    /**
     * Show login page
     */
    public function create()
    {
        View::view('form');
    }

    /**
     * Login user based on his role
     *
     * @param array $post
     */
    public function store($post)
    {
        $newForm = !isset($post->form_id);
        if ($newForm) {
            $form = Entity::getEntity('Form');
        } else {
            $post->form_id = Sanitizer::applyFilter('digit', $post->form_id);
            $form = Database::Model('Form')->find($post->form_id);
        }

        $form->title = Sanitizer::applyFilter('strip_tags|trim|escape', $post->form_name);
        $form->slug = self::slugify($form->title);
        $form->formBuilder = $post->data;

        // Add form items
        $formData = json_decode($post->data);
        $foundItems = [];
        foreach ($formData as $el) {
            $item = null;
            if (!$newForm) {
                $item = Database::Model('FormItem')
                    ->join('f.form', 'form')
                    ->where('f.uid', $el->id)
                    ->where('form.id', $post->form_id)
                    ->first();
                if ($item !== null)
                    $foundItems[] = $item->id;
            }
            if ($item == null) {
                $item = Entity::getEntity('FormItem');
            }

            $item->uid = Sanitizer::applyFilter('trim|strip_tags|escape', $el->id);
            $item->el = Sanitizer::applyFilter('trim|strip_tags|escape', $el->el);
            $item->title = Sanitizer::applyFilter('trim|strip_tags|escape', $el->title);
            $item->placeholder = Sanitizer::applyFilter('trim|strip_tags|escape', $el->placeholder);
            $item->value = Sanitizer::applyFilter('trim|strip_tags|escape', $el->value);
            $item->description = Sanitizer::applyFilter('trim|strip_tags|escape', $el->description);
            $item->col = Sanitizer::applyFilter('digit', $el->col);
            $item->name = Sanitizer::applyFilter('trim|strip_tags|escape', $el->name);
            $item->choices = Sanitizer::applyFilter('cast:array', $el->choices);
            $item->choiceValues = Sanitizer::applyFilter('cast:array', $el->choiceValues);
            $item->choiceWithCustomValue = Sanitizer::applyFilter('trim|strip_tags|escape', $el->choiceWithCustomValue);
            $item->order = Sanitizer::applyFilter('trim|strip_tags|escape', $el->order);

            $item->form = $form;
            Database::save($item);
        }
        if (!$newForm) {
            $items = [];
            foreach ($form->items as $item) {
                if (!in_array($item->id, $foundItems)) {
                    $items[] = $item->id;
                }
            }
            Database::Model('ModuleValue')
                ->where('m.formItem', $items)
                ->delete();
            Database::Model('FormItem')
                ->where('f.id', $items)
                ->delete();
        }

        // Save form
        Database::saveAndFlush($form);

        // Redirect with message
        if ($newForm) {
            $this->alert($this->_('form.created'));
        } else $this->alert($this->_('form.edited'));
        Router::redirect('admin.form.index');
    }

    /**
     * Show use dit page
     *
     * @param $id
     * @throws
     */
    public function edit($id)
    {
        $form = Database::Model('Form')->find($id);

        // Add to breadcrumb
        $this->addToBreadcrumb('dyn', 'admin.form.edit', ['id' => $id], [$form->title]);

        View::view('form', [
            "formBuilder" => $form->formBuilder,
            "id" => $form->id,
            "formName" => $form->title
        ]);
    }

    /**
     * Delete menu
     *
     * @param $id
     */
    public function destroy($id)
    {
        $form = Database::Model('Form')->find($id);
        $form->clearItems();
        Database::saveAndFlush($form);
        Database::Model('Form')->delete($id);

        // Redirect
        $this->alert($this->_('form.deleted'));
        Router::redirect('admin.form.index');
    }

}
