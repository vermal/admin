<?php

namespace Vermal\Admin\Modules\Faq;


use Vermal\Admin\Defaults\Controller;
use Vermal\Admin\Defaults\Languages;
use Vermal\Database\Entity;
use Vermal\DataGrid;
use Vermal\Admin\Modules\Faq\Entities\Faq;
use Vermal\Form\Form;
use Vermal\Admin\View;
use Vermal\Database\Database;
use Vermal\Router;
use Vermal\Traits\DataGridSort;


class Faqs extends Controller
{
	use DataGridSort;

	private $availableFields = ['title', 'te'];
	private $fieldsAfter = ['category'];

    public function __construct()
    {
        parent::__construct();
        $this->requiredPermission('faq', $this->CRUDAction);
        $this->addToBreadcrumb('faq', 'admin.faq.index');
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Database::Model('Faq')
			->order('f.order', 'ASC');
        $datagrid = new DataGrid($data);

        $datagrid->addColumn('id', 'ID');
        $datagrid->addColumn('title', $this->_('faq.title'));
	    $datagrid->addColumn('te', $this->_('faq.te'))->setRenderer(function($entity) {
	    	return substr($entity->te, 0, 100) . ' ...';
	    });
		$datagrid->addColumn('category', $this->_('faq.category'))->setRenderer(function($entity) {
			$categories = apply_filters('admin_faq_categories', []);
			return !empty($categories[$entity->category]) ? $categories[$entity->category] : '';
		});

        // Add actions
		$datagrid->addSortable(['admin.faq.sort']);
        $datagrid->addEdit(['admin.faq.edit', ['id']]);
        $datagrid->addDelete(['admin.faq.destroy_', ['id']]);
		$datagrid->addSwitch(['admin.switch', ['id'], ['c' => str_replace('\\', '__', self::class)]], 'active');

        // Return view
        View::view('crud.read', [
            'datagrid' => $datagrid->build()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
	    $form = $this->form();
	    View::view('crud.createWithTr', [
		    "form" => $form->build(),
		    "formName" => $form->getName(),
		    "fields" => apply_filters('admin_faq_fields', $this->availableFields),
		    "fieldsAfter" => apply_filters('admin_faq_fields_after', $this->fieldsAfter),
	    ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \stdClass $post
     * @throws
     */
    public function store($post)
    {
        $form = $this->form();
        if ($form->hasError()) {
            Router::redirect('admin.faq.create');
        }

        $faq = Entity::getEntity('Faq');
		$faq->order = $this->getMaxValue('Faq');
		$faq->active = 1;
        $this->hydrate($faq, $post);

        Database::saveAndFlush($faq);

        // Redirect with message
        $this->alert($this->_('faq.created'));
        Router::redirect('admin.faq.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @throws
     */
    public function edit($id)
    {
        $form = $this->form();
        $form->setAjax(true);
        $form->setMethod('PUT');
        $form->setAction(routerLink('admin.faq.update', ['id' => $id]));

        /** @var Faq $address */
        $faq = Database::Model('Faq')->find($id);

	    // Set values content
	    foreach (Languages::get() as $lang) {
		    $form->getComponent($lang . '_title')->setValue($faq->getTr($lang, 'title'));
		    $form->getComponent($lang . '_te')->setValue($faq->getTr($lang, 'te'));
	    }

	    $form->setValues([
	    	'category' => $faq->category
		]);

        View::view('crud.createWithTr', [
	        "form" => $form->build(),
	        "formName" => $form->getName(),
	        "fields" => apply_filters('admin_faq_fields', $this->availableFields),
	        "fieldsAfter" => apply_filters('admin_faq_fields_after', $this->fieldsAfter)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \stdClass $post
     * @param  int  $id
     * @throws
     */
    public function update($post, $id)
    {
        $form = $this->form();
        if ($form->hasError()) {
            $this->alert($this->_('error.form.empty'), 'danger');
            die();
        }

	    $faq = Database::Model('Faq')->find($id);
		$this->hydrate($faq, $post, true);

        $this->alert($this->_('faq.edited'));
        Database::saveAndFlush($faq);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
    	$faq = Database::Model('Faq')->find($id);
    	$faq->clearTr();
    	Database::saveAndFlush($faq);
        Database::Model('Faq')->delete($id);

        // Redirect
        $this->alert($this->_('faq.deleted'));
        Router::redirect('admin.faq.index');
    }

	/**
	 * Hydrate database object
	 *
	 * @param Faq $entity
	 * @param $post
	 * @param $update
	 * @return object
	 * @throws \Exception
	 */
	private function hydrate($entity, $post, $update = false)
	{
		// Add translation to content
		if ($update) {
			$entity->clearTr();
			Database::saveAndFlush($entity);
		}
		foreach (Languages::get() as $lang) {
			$entityTr = 'FaqTr';
			Database::translate($entity, $entityTr, 'title', $lang, $this->sanitize('trim|escape|strip_tags', $post->{$lang . '_title'}));
			Database::translate($entity, $entityTr, 'te', $lang, $this->sanitize('trim', $post->{$lang . '_te'}));
		}

		$entity->category = $this->sanitize('trim|escape|strip_tags', $post->category);

		return $entity;
	}

    /**
     * Create register form
     *
     * @return Form
     */
    private function form()
    {
        $form = new Form('faq', routerLink('admin.faq.store'));

	    // Data
	    foreach (Languages::get() as $lang) {
		    $form->addText($lang . '_title', $this->_('faq.title'));
		    $form->addTextEditor($lang . '_te', $this->_('faq.te'));
	    }

	    $form->addSelect('category',
			apply_filters('admin_faq_categories', ['' => $this->_('faq.category')]), $this->_('faq.category'));

        // Add submit button
        $form->addButton('submit', $this->_('global.form.save'))->setClass('btn btn-success');

        return $form;
    }

	/**
	 * @param $post
	 */
	public function sort($post)
	{
		$prev_id = isset($post->prev_id) ? $post->prev_id : false;
		$next_id = isset($post->next_id) ? $post->next_id : false;
		$this->sortHelper('Faq', $post->item_id, $prev_id, $next_id);
	}

	/**
	 * @param $id
	 */
	public static function switchItem($id)
	{
		$entity = Database::Model('Faq')->find($id);
		if (!empty($entity)) {
			$entity->active = !$entity->active;
			Database::saveAndFlush($entity);
		}
		Router::redirect('admin.faq.index');
	}
}
