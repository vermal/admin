<?php

namespace Vermal\Admin\Modules\Faq\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Entities\Translatable;
use Vermal\Admin\Defaults\Model;
use Vermal\Ecommerce\Modules\Product\Entities\Extensions\Attributes;
use Vermal\Ecommerce\Modules\Product\Entities\Extensions\Categories;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="faq")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\TranslationEntity(class="\FaqTr")
 **/
class Faq extends Model
{

	use Translatable;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", nullable=true)
     */
    protected $title;

	/**
	 * @Gedmo\Translatable
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $te;

	/** @ORM\Column(type="integer", name="sort") **/
	protected $order;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $category;

	/**
	 * @ORM\Column(type="integer")
	 */
	protected $active;

	/**
	 * @ORM\OneToMany(
	 *   targetEntity="\FaqTr",
	 *   mappedBy="object",
	 *   cascade={"persist", "remove"},
	 *   orphanRemoval=true
	 * )
	 */
	protected $translations;

	public function __construct()
	{
		parent::__construct();
		$this->translations = new ArrayCollection();
	}
}
