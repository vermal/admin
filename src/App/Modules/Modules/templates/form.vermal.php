@extends('layout')

@section('body')

    <div class="container">
        <form id="form-builder-form" action="{{ routerLink('admin.form.store') }}" method="POST">
            <div class="card shadow-lg m-b-30 p-4">
                @if(isset($id))
                    <input type="hidden" name="form_id" value="{{ $id }}">
                @endif
                <input type="hidden" name="data" value="">
                <div class="form-group">
                    <label for="form_name">Názov formulára</label>
                    <input type="text" class="form-control" id="form_name" name="form_name" placeholder="Názov formulára" value="{{{ $formName }}}" required>
                </div>
            </div>

            <div class="card shadow-lg m-b-30 p-4">
                <div class="text-right mb-4">
                    <button class="btn btn-outline-primary" id="preview"><i class="fas fa-toggle-on mr-2"></i> Náhľad</button>
                </div>
                <div id="form-builder"></div>
                <div class="mt-4">
                    <button name="submit" class="btn btn-success">@__(global.form.save)</button>
                </div>
            </div>
        </form>
    </div>

    <script>
        let formBuilder = $('#form-builder').formBuilder({
            locale: 'sk'
        });
        @if(isset($formBuilder))
            let data = JSON.parse(<?php echo json_encode($formBuilder, JSON_PRETTY_PRINT) ?>);
            formBuilder.setData(data);
        @endif
        $('body').click(function () {
            $('#form-builder-form input[name="data"]').val(JSON.stringify(formBuilder.json));
        });
        $('#preview').click(function (e) {
            e.preventDefault();
            formBuilder.preview();
        })
    </script>

@endsection
