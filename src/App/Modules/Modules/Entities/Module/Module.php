<?php

namespace Vermal\Admin\Modules\Modules\Entities\Module;

use Vermal\Admin\Defaults\Model;

/**
 * @ORM\Entity @ORM\Table(name="module")
 * @ORM\HasLifecycleCallbacks
 **/
class Module extends Model
{
    use ModuleTrait;
}
