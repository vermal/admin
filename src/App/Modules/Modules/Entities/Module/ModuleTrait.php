<?php

namespace Vermal\Admin\Modules\Modules\Entities\Module;

use Doctrine\Common\Collections\ArrayCollection;


trait ModuleTrait
{
    /** @ORM\Column(type="string") **/
    protected $title;

    /** @ORM\Column(type="string") **/
    protected $slug;

    /**
     * @ORM\ManyToOne(targetEntity="\Form")
     * @ORM\JoinColumn(name="form_builder_id", referencedColumnName="id")
     */
    protected $form;


    /** @ORM\Column(type="string") **/
    protected $template;

    /**
     * @ORM\ManyToMany(targetEntity="\User", mappedBy="modules")
     */
    protected $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getUsersPair($key1, $key2)
    {
        $data = [];
        foreach ($this->users as $user) {
            $data[$user->{$key1}] = $user->{$key2};
        }
        return $data;
    }

    /**
     * Remove Users
     *
     * @param $user
     */
    public function removeUsers($user)
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            $user->removeModules($this);
        }
    }

    /**
     * @param $user
     */
    public function addUser($user)
    {
        $this->users[] = $user;
    }
}
