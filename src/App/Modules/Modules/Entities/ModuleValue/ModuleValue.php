<?php

namespace Vermal\Admin\Modules\Modules\Entities\ModuleValue;

use Vermal\Database\MagicAccessor;

/**
 * @ORM\Entity @ORM\Table(name="module_value")
 * @ORM\HasLifecycleCallbacks
 **/
class ModuleValue
{
    use MagicAccessor;

    /** @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue **/
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="\FormItem")
     * @ORM\JoinColumn(name="form_item_id", referencedColumnName="id")
     */
    protected $formItem;

    /**
     * @ORM\ManyToOne(targetEntity="\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /** @ORM\Column(type="text") **/
    protected $value;

    /**
     * @ORM\ManyToOne(targetEntity="\Module", inversedBy="fields")
     * @ORM\JoinColumn(name="module_id", referencedColumnName="id")
     */
    protected $module;

}
