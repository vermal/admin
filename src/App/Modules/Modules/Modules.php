<?php

namespace Vermal\Admin\Modules\Modules;


use Vermal\Admin\Defaults\Controller;
use Vermal\Admin\Defaults\Languages;
use Vermal\Admin\Modules\Modules\Entities\Module\Module;
use Vermal\Admin\Sanitizer;
use Vermal\Database\Database;
use Vermal\Database\Entity;
use Vermal\DataGrid;
use Vermal\Form\Form;
use Vermal\Router;
use Vermal\Admin\View;


class Modules extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->requiredPermission('modules', $this->CRUDAction);
        $this->addToBreadcrumb('modules', 'admin.module.index');
    }

    /**
     * Browse all users
     */
    public function index()
    {
        // Create new data grid
        $datagrid = new DataGrid(Database::Model('Module')->order('m.id', 'DESC'));

        // Add columns
        $datagrid->addColumn('id', 'ID');
        $datagrid->addColumn('title', $this->_('module.title'));

        // Add actions
        $datagrid->addEdit(['admin.module.edit', ['id']]);
        $datagrid->addDelete(['admin.module.destroy_', ['id']]);

        // Return view
        View::view('crud.read', [
            'datagrid' => $datagrid->build()
        ]);
    }

    /**
     * Show login page
     */
    public function create()
    {
        $form = $this->form();
        View::view('crud.create', [
            "form" => $form->build()
        ]);
    }

    /**
     * Login user based on his role
     *
     * @param array $post
     */
    public function store($post)
    {
        $form = $this->form();
        if ($form->hasError()) {
            Router::redirect('admin.module.create');
        }

        $module = Entity::getEntity('Modules');

        // Hydrate data
        $form->hydrate($module, ['form']);
        $module->slug = self::slugify($module->title);
        $module->form = Database::Model('Form')->find(Sanitizer::applyFilter('strip_tags|trim|escape', $post->form));
        // $module->template = Database::Model('PDFTemplate')->find(Sanitizer::applyFilter('strip_tags|trim|escape', $post->template));
        $module->template = Sanitizer::applyFilter('strip_tags|trim|escape', $post->template);

        foreach ($post->users as $user) {
            $user = Database::Model('User')->find($user);
            $module->addUser($user);
            $user->addModule($module);
            Database::save($user);
        }

        // Save form
        Database::saveAndFlush($module);

        // Redirect with message
        $this->alert($this->_('module.created'));
        Router::redirect('admin.module.index');
    }

    /**
     * Show use dit page
     *
     * @param $id
     * @throws
     */
    public function edit($id)
    {
        $form = $this->form();
        $form->setAjax(true);
        $form->setMethod('PUT');
        $form->setAction(routerLink('admin.module.update', ['id' => $id]));

        $module = Database::Model('Module')->find($id);

        // Add to breadcrumb
        $this->addToBreadcrumb('dyn', 'admin.module.edit', ['id' => $id], [$module->title]);

        $form->setValues($module, ['form', 'template', 'users']);
        $form->getComponent('form')->setValue($module->form->id);
        $form->getComponent('template')->setValue($module->template);
        $form->getComponent('users')->setValue($module->getUsersPair('id', 'name'));

        View::view('crud.create', [
            "form" => $form->build()
        ]);
    }

    /**
     * Update
     *
     * @param array $post
     * @param $id
     */
    public function update($post, $id)
    {
        $form = $this->form();
        if ($form->hasError()) {
            Router::redirect('admin.module.create');
        }

        // Find
        $module = Database::Model('Module')->find($id);

        // Hydrate data
        $form->hydrate($module, ['form', 'users']);
        $module->slug = self::slugify($module->title);
        $module->form = Database::Model('Form')->find(Sanitizer::applyFilter('strip_tags|trim|escape', $post->form));
        // $module->template = Database::Model('PDFTemplate')->find(Sanitizer::applyFilter('strip_tags|trim|escape', $post->template));
        $module->template = Sanitizer::applyFilter('strip_tags|trim|escape', $post->template);


        Database::deleteItems($module, 'users');
        foreach ($post->users as $user) {
            $user = Database::Model('User')->find($user);
            $module->addUser($user);
            $user->addModule($module);
            Database::save($user);
        }

        // Save form
        Database::saveAndFlush($module);

        // Redirect with message
        $this->alert($this->_('module.edited'));
    }

    /**
     * Delete menu
     *
     * @param $id
     */
    public function destroy($id)
    {
        $form = Database::Model('Form')->find($id);
        $form->clearItems();
        Database::saveAndFlush($form);
        Database::Model('Form')->delete($id);

        // Redirect
        $this->alert($this->_('form.deleted'));
        Router::redirect('admin.form.index');
    }

    /**
     * Create register form
     *
     * @return Form
     */
    private function form()
    {
        $form = new Form('module', Router::link('admin.module.store'));

        $forms = Database::Model('Form')->setPair('id', 'title')->get();
        // $templates = Database::Model('PDFTemplate')->setPair('id', 'title')->get();
        $users = Database::Model('User')->setPair('id', 'name')->get();

        $form->addText('title', $this->_('module.title'))->setCol('col-lg-5')->required();
        // $form->addText('price', 'Price')->setCol('col-lg-4')->int();
        $form->addSelect('form', $forms, $this->_('module.form'))->setCol('col-lg-4')->setClass('js-select2');
        $form->addText('template', $this->_('module.template'))->addDescription('www.pdfgeneratorapi.com')->setCol('col-lg-3')->required();
        // $form->addSelect('template', $templates, $this->_('module.template'))->setCol('col-lg-3')->setClass('js-select2');
        $form->addSelect('users', $users, $this->_('module.users'))
            ->addAttribute('multiple', 'multiple')
            ->setCol('col-lg-3')->setClass('js-select2');

        // Add submit button
        $form->addButton('submit', $this->_('global.form.save'))->setClass('btn btn-success');

        return $form;
    }

}
