<?php

namespace Vermal\Admin\Modules\Modules;


use Vermal\Admin\Defaults\Controller;
use Vermal\Admin\Defaults\Languages;
use Vermal\Admin\Modules\Auth\Auth;
use Vermal\Admin\Modules\Modules\Entities\Module\Module;
use Vermal\Admin\Modules\Modules\Entities\ModuleValue\ModuleValue;
use Vermal\Admin\Modules\PDFTemplates\Entities\PDFTemplate;
use Vermal\Admin\Modules\PDFTemplates\PDFTemplates;
use Vermal\Admin\Sanitizer;
use Vermal\Database\Database;
use Vermal\Database\Entity;
use Vermal\DataGrid;
use Vermal\Form\Form;
use Vermal\Router;
use Vermal\Admin\View;


class ModuleValues extends Controller
{
    protected $defaultViewPath = false;

    public function __construct()
    {
        parent::__construct();
        $this->requiredPermission('module-values', $this->CRUDAction);
        $this->addToBreadcrumb('module-values', 'admin.module-values.index');
        // Hide header actions
        View::setVariable('showHeaderActions', false);
    }

    /**
     * Browse all users
     */
    public function index()
    {
        $this->setDefaultViewPath();

        $data = Database::Model('Module')->order('m.id', 'DESC');
        if (Auth::hasRole('customer')) {
            $data->join('m.users', 'users');
            $data->where('users.id', [Auth::getUser()->id]);
        }

        // Create new data grid
        $datagrid = new DataGrid($data);

        // Add columns
        $datagrid->addColumn('id', 'ID');
        $datagrid->addColumn('title', $this->_('module.title'));

        // Add actions
        $datagrid->addAction('fill', ['admin.module-values.show', ['id']], '', 'Vyplniť modul', 'btn btn-outline-secondary mr-2');
        $datagrid->addAction('download', ['admin.module-values.generate', ['id']], '', 'Vygenerovať PDF súbor', 'btn btn-primary');

        // Return view
        View::view('crud.read', [
            'datagrid' => $datagrid->build()
        ]);
    }

    public function show($id)
    {
        // Get module and set title
        $module = Database::Model('Module')->find($id);
        $this->addToBreadcrumb('dyn', 'admin.module-values.show', ['id' => $id], [$module->title]);

        // Get form
        $form = $this->form($module);

        // Return view
        View::view('form-values', [
            'form' => $form->build()
        ]);
    }

    /**
     * Login user based on his role
     *
     * @param array $post
     */
    public function store($post)
    {
        $module = Database::Model('Module')->find($post->module);

        // Validate form
        $form = $this->form($module);
        if ($form->hasError()) {
            Router::redirect('admin.module-values.show', ['id' => $post->module]);
        }

        $this->saveValues($post, $module);
        Database::saveAndFlush($module);

        // Redirect with message
        $this->alert($this->_('module-values.created'));
        Router::redirect('admin.module-values.index');
    }

    /**
     * Helper function to save values for each form field
     *
     * @param $post
     * @param $module
     */
    private function saveValues($post, $module)
    {
        $user = Database::Model('User')->find(Auth::getUser()->id);
        foreach ($post as $key => $value) {
            if (in_array($key, ['submit', '_method', 'module'])) continue;
            $item = Database::Model('FormItem', 'f')->where('f.uid', $key)->first();

            $moduleValue = Database::Model('ModuleValue')
                ->where('m.formItem', $item->id)
                ->where('m.user', $user->id)
                ->where('m.module', $module->id)
                ->first();
            if ($moduleValue === null) $moduleValue = Entity::getEntity('ModuleValue');
            $value = Sanitizer::applyFilter('stip_tags|trim|escape', $value);
            if (is_object($value)) {
                $value = (array)$value;
                $value = serialize($value);
            }

            $moduleValue->formItem = $item;
            $moduleValue->user = $user;
            $moduleValue->value = $value;
            $moduleValue->module = $module;

            Database::save($moduleValue);
        }
    }

    /**
     * Generate PDF from template
     *
     * @param $id
     */
    public function generate($id)
    {
        $module = Database::Model('Module')->find($id);

        // Get values
        $moduleValues_ = Database::Model('ModuleValue')
            ->where('m.user', Auth::getUser()->id)
            ->where('m.module', $module->id)
            ->get();


        $moduleValues = [];
        foreach ($moduleValues_ as $k => $row) {
            $value = $row->value;
            if ($this->is_serialized($value)) {
                $value = unserialize($value);
            }
            $moduleValues[$row->formItem->name] = $value;
        }

        $outputData = [];
        foreach ($module->form->items as $formItem) {
            if ($formItem->name === '' || !isset($moduleValues[$formItem->name])) continue;
            $value = $moduleValues[$formItem->name];
            if (!is_array($value)) {
                $outputData[$formItem->name] = $value;
            } else {
                if (is_array($formItem->choices)) {
                    $values = [];
                    foreach ($formItem->choices as $k => $choice) {
                        $v = null;
                        if (in_array($formItem->choiceValues[$k], $value)) {
                            $v = $formItem->choiceValues[$k];
                        } else if (in_array($choice, $value)) {
                            $v = $choice;
                        }
                        if ($v !== null) $values[] = '- ' . $v;
                        $outputData[$formItem->name][self::slugify($choice)] = $v;
                    }
                    $outputData[$formItem->name . '_d'] = implode("\n", $values);
                }
            }
        }


        $outputData['date'] = date('d.m.Y');
        file_put_contents(__DIR__ . '/data.json', json_encode($outputData, JSON_PRETTY_PRINT));
//        die();

        // PDFTemplates::generatePDF($module->template->methods, $moduleValues);
        // PDFTemplates::createPDF($moduleValues, $module->template->methods);
        $response = PDFTemplates::mergePDF($outputData, $module->template);

        header('Content-Type: application/pdf');
        echo base64_decode($response->response);
    }


    /**
     * Create register form
     *
     * @param $module
     *
     * @return Form
     */
    private function form($module)
    {
        $moduleForm = $module->form;
        $moduleValues_ = Database::Model('ModuleValue')
            ->where('m.user', Auth::getUser()->id)
            ->where('m.module', $module->id)
            ->get();

        $moduleValues = [];
        foreach ($moduleValues_ as $k => $row) {
            $value = $row->value;
            if ($this->is_serialized($value)) {
                $value = unserialize($value);
            }
            $moduleValues[$row->formItem->uid] = $value;
        }


//        var_dump($moduleValues);
//        die();

        $form = new Form('module-values', Router::link('admin.module-values.store'));
        $form->addHidden('module', $module->id);

        foreach ($moduleForm->items as $k => $item) {
            $el = null;
            if ($item->el === 'input') {
                $el = $form->addText($item->uid, $item->title);
                $el->addAttribute('placeholder', $item->placeholder);
                if (isset($moduleValues[$item->uid]))
                    $el->setValue($moduleValues[$item->uid]);
            } else if ($item->el === 'date') {
                $el = $form->addText($item->uid, $item->title);
                $el->addAttribute('placeholder', $item->placeholder);
                $el->setClass('date');
                if (isset($moduleValues[$item->uid]))
                    $el->setValue($moduleValues[$item->uid]);
            } else if ($item->el === 'textarea') {
                $el = $form->addTextArea($item->uid, $item->title);
                $el->addAttribute('placeholder', $item->placeholder);
                if (isset($moduleValues[$item->uid]))
                    $el->setValue($moduleValues[$item->uid]);
            } else if ($item->el === 'select') {
                $choices = [];
                foreach ($item->choices as $key => $choice) {
                    $choices[$item->choiceValues[$key]] = $choice;
                }
                $el = $form->addSelect($item->uid, $choices, $item->title);
                if (isset($moduleValues[$item->uid]))
                    $el->setValue($moduleValues[$item->uid]);
                else if (isset($moduleValues[$item->uid]))
                    $el->setValue($moduleValues[$item->uid]);
            } else if ($item->el === 'checkbox' || $item->el === 'radio') {
                $choices = [];
                foreach ($item->choices as $key => $choice) {
                    $v = null;
                    $choices[] = [
                        'value' => (!empty($item->choiceValues[$key]) ? $item->choiceValues[$key] : $choice),
                        'label' => $choice
                    ];
                }
                if ($item->choiceWithCustomValue) {
                    if (isset($moduleValues[$item->uid])) {
                        $lastValue = $moduleValues[$item->uid][count($moduleValues[$item->uid]) - 1];
                        if ($this->in_array_r($lastValue, $choices, true)) $lastValue = '';
                        $choices[count($choices)] = [
                            'value' => $lastValue,
                            'label' => ''
                        ];
                    } else {
                        $choices[count($choices)] = [
                            'value' => '',
                            'label' => ''
                        ];
                    }
                }
                $el = $form->{'add' . ucfirst($item->el) . 'Group'}($item->uid, $choices, $item->title, $item->choiceWithCustomValue);
                if (isset($moduleValues[$item->uid]))
                    $el->setValue($moduleValues[$item->uid]);
            } else if ($item->el === 'heading') {
                $el = $form->addHeading('3', $item->value);
                $el->setClass('col-lg-' . $item->col);
            } else if ($item->el === 'paragraph') {
                $el = $form->addHtml($item->uid, 'p', $item->value);
            } else if ($item->el === 'margin') {
                $el = $form->addHtml($item->uid, 'div')->setClass('mb-4');
            } else {
                continue;
            }
            $el->setCol('col-lg-' . $item->col);
            $el->addDescription($item->description);
        }

        // Add submit button
        $form->addButton('submit', $this->_('global.form.save'))->setClass('btn btn-success');

        return $form;
    }

    private function is_serialized( $data ) {
        // if it isn't a string, it isn't serialized
        if ( !is_string( $data ) )
            return false;
        $data = trim( $data );
        if ( 'N;' == $data )
            return true;
        if ( !preg_match( '/^([adObis]):/', $data, $badions ) )
            return false;
        switch ( $badions[1] ) {
            case 'a' :
            case 'O' :
            case 's' :
                if ( preg_match( "/^{$badions[1]}:[0-9]+:.*[;}]\$/s", $data ) )
                    return true;
                break;
            case 'b' :
            case 'i' :
            case 'd' :
                if ( preg_match( "/^{$badions[1]}:[0-9.E-]+;\$/", $data ) )
                    return true;
                break;
        }
        return false;
    }

    private function in_array_r($needle, $haystack, $strict = false) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
                return true;
            }
        }

        return false;
    }

}
