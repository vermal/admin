<?php

namespace Vermal\Admin\Modules\Extensions;


use Doctrine\ORM\Tools\Pagination\Paginator;
use Vermal\Admin\Defaults\Controller;
use Vermal\Router;


class Activate extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

	/**
	 * Select2 handler
	 *
	 * @param $id
	 * @param $controller
	 */
	public function activate($id, $controller)
	{
		$controller = str_replace('__', '\\', $controller);
		// Check if class and method exists
		if (!class_exists($controller) || !method_exists($controller, 'switchItem')) {
			Router::redirectToURL(Router::prevLink());
		}
		$controller::switchItem($id);
	}

}
