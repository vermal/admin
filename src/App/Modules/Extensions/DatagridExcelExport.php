<?php

namespace Vermal\Admin\Modules\Extensions;


use Doctrine\ORM\Tools\Pagination\Paginator;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Worksheet\SheetView;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Vermal\Admin\Defaults\Controller;
use Vermal\App;
use Vermal\Router;


class DatagridExcelExport
{

	/** @var array $data */
	private $data;

	/** @var array $columns */
	private $columns;

	/** @var array $skipColumns */
	private $skipColumns;

	/** @var string $sheetTitle */
	private $sheetTitle;

	/** @var Worksheet $sheet */
	private $sheet;

	/** @var array $alphas */
	private $alphas = [];

	/** @var array $dimensions */
	private $dimensions = [];

	/**
	 * Action handler
	 *
	 * @param \Vermal\DataGrid $datagrid
	 * @param string $sheetTitle
	 */
	public function __construct($datagrid, $sheetTitle = 'Sheet 1')
	{
		$datagrid->doFilter();
		$this->data = $datagrid->getData()->get();
		$this->columns = $datagrid->getColumns();
		$this->sheetTitle = $sheetTitle;
		$this->alphas = range('A', 'Z');
	}

	/**
	 * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
	 */
	public function doExport()
	{
		$this->initTable();
	}

	/**
	 * @param array $skipColumns
	 */
	public function skipColumns(array $skipColumns)
	{
		$this->skipColumns = $skipColumns;
	}

	/**
	 * @param array $dimensions
	 */
	public function setColumnDimensions(array $dimensions)
	{
		$this->dimensions = $dimensions;
	}

	/**
	 * Create table
	 *
	 * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
	 */
	private function initTable()
	{
		$spreadsheet = new Spreadsheet();
		$this->sheet = $spreadsheet->getActiveSheet();
		$this->sheet->setTitle($this->sheetTitle);

		$this->createHeader();
		$this->createContent();

		$writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="export.xlsx"');
		$writer->save('php://output');
	}

	/**
	 * Create sheet header
	 */
	private function createHeader()
	{
		$i = -1;
		foreach ($this->columns as $columnKey => $column) {
			if (in_array($columnKey, $this->skipColumns)) continue;
			$i++;
			$this->sheet->setCellValue($this->alphas[$i] . '1', $column['label']);
			$this->sheet->getStyle($this->alphas[$i] . '1')->getFont()->setBold(true);
			$this->sheet->getStyle($this->alphas[$i] . '1')->getAlignment()
				->setHorizontal(Alignment::HORIZONTAL_CENTER)
				->setVertical(Alignment::VERTICAL_CENTER);

			if ($columnKey === 'id') {
				$this->sheet->getColumnDimension($this->alphas[$i])
					->setWidth(5);
			}

			// Set column dimensions
			if (array_key_exists($columnKey, $this->dimensions)) {
				$this->sheet->getColumnDimension($this->alphas[$i])
					->setWidth($this->dimensions[$columnKey]);
			}
		}
	}

	/**
	 * Create content
	 */
	private function createContent()
	{
		foreach ($this->data as $key => $row) {
			$rowId = $key + 2;
			$i = -1;
			foreach ($this->columns as $column => $value) {
				if (in_array($column, $this->skipColumns)) continue;
				$i++;
				$this->sheet->setCellValue($this->alphas[$i] . $rowId, \Vermal\DataGrid::renderValue($column, $value, $row));
				$this->sheet->getStyle($this->alphas[$i] . $rowId)->getAlignment()
					->setVertical(Alignment::VERTICAL_CENTER);
			}
		}
	}

}
