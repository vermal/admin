<?php

namespace Vermal\Admin\Modules\Extensions;


use Doctrine\ORM\Tools\Pagination\Paginator;
use Vermal\Admin\Defaults\Controller;
use Vermal\App;
use Vermal\Router;


class Datagrid extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

	/**
	 * Action handler
	 *
	 * @param $post
	 */
	public function action($post)
	{
		$actions = App::sessionGet('datagrid.bulkActions');
		if (!empty($actions) && !empty($post->keys)) {
			foreach ($actions as $action) {
				if ($action['action'] == $post->bulkActions) {
					$callback = $action['callback'];
					if (!empty($callback[1])) {
						$class = new $callback[0];
						$callback = [$class, $callback[1]];
					}
					call_user_func_array($callback, [$post->keys]);
					die();
				}
			}
		} else Router::redirectToURL(Router::prevLink());
	}

	/**
	 * Action handler
	 *
	 * @param $c
	 * @param $type
	 */
	public function export($c, $type)
	{
		$controller = str_replace('__', '\\', $c);
		$method = '';
		switch ($type) {
			case('excel'):
				$method = 'dataGridExportExcelHandler';
		}

		$controller::{$method}();
	}

}
