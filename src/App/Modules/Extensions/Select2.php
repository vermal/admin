<?php

namespace Vermal\Admin\Modules\Extensions;


use Doctrine\ORM\Tools\Pagination\Paginator;
use Vermal\Admin\Defaults\Controller;


class Select2 extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

	/**
	 * Select2 handler
	 *
	 * @param $post
	 */
	public function select2($post)
	{
		foreach (['query', 'page', 'controller'] as $key) {
			if (!isset($post->{$key})) {
				echo $this->getResult([]);
				die();
			}
		}

		$query = $post->query;
		$page = $post->page;
		$controller = $post->controller;
		$method = !isset($post->method) ? 'select2' : $post->method;

		// Check if class and method exists
		if (!class_exists($controller) || !method_exists($controller, $method)) {
			echo $this->getResult([]);
			die();
		}
		$limit = 10;
		$method = $controller::{$method}($query);
		$properties = ['id', 'text'];
		if (is_array($method)) {
			$properties = $method[1];
			$method = $method[0];
		}
		$query = $method
			->limit($limit, ($page - 1) * $limit)
			->get(false, 'QUERY');

		$paginator = new Paginator($query, $fetchJoinCollection = true);
		$results = [];
		foreach ($paginator as $post) {
			$results[] = [
				'id' => $post->{'get' . ucfirst($properties[0])}(),
				'text' => $post->{'get' . ucfirst($properties[1])}()
			];
		}
		echo $this->getResult($results, count($paginator) > ($limit * $page) ? true : false);
		die();
	}

	/**
	 * Get reuslts
	 *
	 * @param $results
	 * @param $more
	 * @return false|string
	 */
	public function getResult($results, $more = false)
	{
		return json_encode([
			'results' => $results,
			'pagination' => [
				'more' => $more
			]
		]);
	}

}
