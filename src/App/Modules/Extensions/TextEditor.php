<?php

namespace Vermal\Admin\Modules\Extensions;


use Doctrine\ORM\Tools\Pagination\Paginator;
use Vermal\Admin\Defaults\Controller;
use Vermal\Admin\Upload;
use Vermal\App;
use Vermal\Database\Database;
use Vermal\Router;


class TextEditor extends Controller
{

	/**
	 * Upload image
	 *
	 * @param $post
	 * @throws \Exception
	 */
	public function upload($post)
	{
		// Upload file
		if (!empty($_FILES['image']['name'])) {
			$files = Upload::uploadFile('image', 1);
			Upload::cropImages($files);
			foreach ($files as $file) {
				$uploadedFile = Database::Model('Multimedia')->find($file);
			}
		}
		if (!empty($uploadedFile)) {
			echo json_encode([
				'success' => true,
				'file' => Router::$URL . $uploadedFile->getFile()
			]);
		} else echo json_encode([
			'success' => false,
			'file' => null
		]);
		die();
	}

}
