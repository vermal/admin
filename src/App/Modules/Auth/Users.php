<?php

namespace Vermal\Admin\Modules\Auth;


use Vermal\Admin\Defaults\Controller;
use Vermal\Admin\Modules\Auth\Entities\User\User;
use Vermal\Database\Database;
use Vermal\Database\Entity;
use Vermal\DataGrid;
use Vermal\Form\Form;
use Vermal\Router;
use Vermal\Admin\View;


class Users extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->requiredPermission('user', $this->CRUDAction);
        $this->addToBreadcrumb('user', 'admin.user.index');
    }

    private function getRoles()
    {
        return Database::Model('Role')
            ->where('r.authority', '>=', Auth::getUser()->authority)
            ->get();
    }

    /**
     * Browse all users
     */
    public function index()
    {
        // Get all available roles
        $roles = Database::Role()->setPair('id', 'displayName')->get();

        // Create new data grid
        $datagrid = new DataGrid(Database::Model('User')->order('u.id', 'DESC')
            ->join('role', 'ur')
            ->where('ur.authority', '>=', Auth::getUser()->authority));

        // Add columns
        $datagrid->addColumn('id', 'ID');
        $datagrid->addColumn('email', 'E-Mail');

        // Add filter by role
        $datagrid->addFilter('role', $roles);

        // Add actions
        $datagrid->addEdit(['admin.user.edit', ['id']]);
        $datagrid->addDelete(['admin.user.destroy_', ['id']]);

        // Return view
        View::view('crud.read', [
            'datagrid' => $datagrid->build()
        ]);
    }

    /**
     * Show login page
     */
    public function create()
    {
        $form = $this->form();
        View::view('crud.create', [
            "form" => $form->build()
        ]);
    }

    /**
     * Login user based on his role
     *
     * @param array $post
     */
    public function store($post)
    {
        $form = $this->form();
        if ($form->hasError()) {
            Router::redirect('admin.user.create');
        }

        // Check if user already exist
        $checkUser = Database::User()->where('u.email', $post->email)->find();
        if (!empty((array)$checkUser)) {
            $form->setGlobalError($this->_('auth.user.user_exist'));
            Router::redirect('admin.user.create');
        }

        // Create new user
        $user = Entity::getEntity('User');
        $user = $form->hydrate($user, ['role', 'password']);

        $user->password = password_hash($post->password, ADMIN_HASH);
        $user->role = Database::Role()->where('r.id', $post->role)->find();

        Database::saveAndFlush($user);

        // Redirect to browse view
        Router::redirect('admin.user.index');
    }

    /**
     * Show use dit page
     *
     * @param $id
     */
    public function edit($id)
    {
        $user = Database::User()->find($id);
        if ($user->role->authority < Auth::getUser()->authority) {
            $this->alert($this->_('global.low_permissions'), 'warning');
            Router::redirect('admin.user.index');
        }

        $form = $this->form();

        // Add to breadcrumb
        $this->addToBreadcrumb('dyn', 'admin.user.edit', ['id' => $id], [$user->name]);

        // Change components
        $form->getComponent('password')->unsetRule('min');

        // Set values
        $form->setValues($user, ['role', 'password']);
        $form->setValue($user->role->id, 'role');

        // Set edit action
        $form->setAction(Router::link('admin.user.update', ['id' => $id]));
        $form->setMethod('PUT');

        View::view('crud.edit', [
            "form" => $form->build()
        ]);
    }

    /**
     * Update user
     *
     * @param array $post
     * @param $id
     */
    public function update($post, $id)
    {
        $form = $this->form();
        $form->getComponent('password')->unsetRule('min');

        // Check if for has some errors
        if ($form->hasError()) {
            Router::redirect('admin.user.edit', ['id' => $id]);
        }

        // Create new user
        $user = Database::User()->find($id);
        if ($user->role->authority < Auth::getUser()->authority) {
            $this->alert($this->_('global.low_permissions'), 'warning');
            Router::redirect('admin.user.index');
        }

        $user = $form->hydrate($user, ['role', 'password']);

        // Set new password if not empty
        if (!empty($post->password)) {
            $user->password = password_hash($post->password, ADMIN_HASH);
        }
        $user->role = Database::Role()->where('r.id', $post->role)->find();

        // Update user
        Database::saveAndFlush($user);

        // Redirect to user edit
        Router::redirect('admin.user.edit', ['id' => $id]);
    }

    public function destroy($id)
    {
        if ($id == Auth::getUser()->id) {
            $this->alert("You can't delete yourself", 'danger');
            Router::redirect('admin.user.index');
        }
        Database::User()->delete($id);
        Router::redirect('admin.user.index');
    }

    /**
     * Create register form
     *
     * @return Form
     */
    private function form()
    {
        $form = new Form('user', 'admin/user');

        // Login info
        $form->addEmail('email', $this->_('auth.user.email'))->setCol('col-lg-6')
            ->email();
        $form->addPassword('password', $this->_('auth.user.password'))->setCol('col-lg-6')
            ->min(8);

        // Auth level
        $roles = [];
        foreach ($this->getRoles() as $role) {
            $roles[$role->id] = $role->displayName;
        }
        $form->addSelect('role', $roles, $this->_('auth.user.role'))->setCol('col-lg-4')
            ->required();

        // Other info
        $form->addText('name', $this->_('auth.user.name'))->setCol('col-lg-4')
            ->required();
        $form->addText('phone', $this->_('auth.user.phone'))->setCol('col-lg-4')
            ->int()
            ->required();

        // Add submit button
        $form->addButton('submit', $this->_('auth.user.save'))->setClass('btn btn-success');

        return $form;
    }

}
