<?php

namespace Vermal\Admin\Modules\Auth\Entities\Resource;

use Vermal\Admin\Defaults\Model;

/**
 * @ORM\Entity @ORM\Table(name="resource")
 * @ORM\HasLifecycleCallbacks
 **/
class Resource extends Model
{
    use ResourceTrait;
}
