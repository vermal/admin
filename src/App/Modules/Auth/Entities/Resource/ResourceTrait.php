<?php

namespace Vermal\Admin\Modules\Auth\Entities\Resource;

trait ResourceTrait
{
    /** @ORM\Column(type="string") **/
    protected $name;

    /** @ORM\Column(type="string", name="display_name") **/
    protected $displayName;

    /** @ORM\Column(type="integer") **/
    protected $authority;
}
