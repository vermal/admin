<?php

namespace Vermal\Admin\Modules\Auth\Entities\Permission;

trait PermissionTrait
{
    /**
     * @ORM\ManyToOne(targetEntity="\Resource")
     * @ORM\JoinColumn(name="resource_id", referencedColumnName="id")
     */
    protected $resource;

    /** @ORM\Column(type="integer", name="create_") **/
    protected $create;

    /** @ORM\Column(type="integer", name="read_") **/
    protected $read;

    /** @ORM\Column(type="integer", name="update_") **/
    protected $update;

    /** @ORM\Column(type="integer", name="delete_") **/
    protected $delete;
}
