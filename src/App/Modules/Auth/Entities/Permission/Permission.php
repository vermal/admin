<?php

namespace Vermal\Admin\Modules\Auth\Entities\Permission;

use Vermal\Admin\Defaults\Model;

/**
 * @ORM\Entity @ORM\Table(name="auth_permission")
 * @ORM\HasLifecycleCallbacks
 **/
class Permission extends Model
{
    use PermissionTrait;
}
