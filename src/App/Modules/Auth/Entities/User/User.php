<?php

namespace Vermal\Admin\Modules\Auth\Entities\User;

use Vermal\Admin\Defaults\Model;

/**
 * @ORM\Entity
 * @ORM\Table(name="auth_user")
 * @ORM\HasLifecycleCallbacks
 **/
class User extends Model
{
    use UserTrait;
}
