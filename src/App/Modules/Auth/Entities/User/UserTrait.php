<?php

namespace Vermal\Admin\Modules\Auth\Entities\User;

use Vermal\Admin\Modules\Auth\Entities\Permission\Permission;

trait UserTrait
{
    /** @ORM\Column(type="string") **/
    protected $email;

    /** @ORM\Column(type="string") **/
    protected $password;

    /** @ORM\Column(type="string") **/
    protected $name;

    /** @ORM\Column(type="string", nullable=true) **/
    protected $phone;

    /** @ORM\Column(type="string", nullable=true, name="reset_token") **/
    protected $resetToken;

    /** @ORM\Column(type="datetime", nullable=true, name="reset_token_valid_until") **/
    protected $resetTokenValidUntil;

    /**
     * @ORM\ManyToOne(targetEntity="\Role")
     * @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     */
    protected $role;
}
