<?php

namespace Vermal\Admin\Modules\Auth\Entities\Role;

use Doctrine\Common\Collections\ArrayCollection;

trait RoleTrait
{
    /** @ORM\Column(type="string") **/
    protected $name;

    /** @ORM\Column(type="string", name="display_name") **/
    protected $displayName;

    /** @ORM\Column(type="integer") **/
    protected $authority;

    /**
     * @ORM\ManyToMany(targetEntity="\Permission", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="auth_role_permission",
     *      joinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="permission_id", referencedColumnName="id")}
     * )
     */
    protected $permissions;

    public function __construct()
    {
        parent::__construct();
        $this->permissions = new ArrayCollection();
    }

    /**
     * Add permission
     *
     * @param $permission
     */
    public function addPermission($permission)
    {
    	if (!$this->permissions->contains($permission))
        	$this->permissions[] = $permission;
    }

    /**
     * Remove permissions
     *
     * @param $permission
     */
    public function removePermissions($permission)
    {
        if ($this->permissions->contains($permission)) {
            $this->permissions->removeElement($permission);
        }
    }
}
