<?php

namespace Vermal\Admin\Modules\Auth\Entities\Role;

use Vermal\Admin\Defaults\Model;

/**
 * @ORM\Entity @ORM\Table(name="auth_role")
 * @ORM\HasLifecycleCallbacks
 **/
class Role extends Model
{
    use RoleTrait;
}
