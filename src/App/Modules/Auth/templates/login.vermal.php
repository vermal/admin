@extends('layout')

@section('body')


    <main class="admin-main">
        <div class="container-fluid">
            <div class="row ">
                <div class="col-lg-4  bg-white">
                    <div class="row align-items-center m-h-100">
                        <div class="mx-auto col-md-8">
                            <div class="p-b-20 text-center">
                                <p>
                                    <img v:admin:public:src="assets/img/logo.svg" width="80" alt="">
                                </p>
                                <p class="admin-brand-content">
                                    vermal
                                </p>
                            </div>

                            <div>
                                @foreach($alerts as $key => $alert)
                                    <div class="alert alert-{{ $alert["type"] }}">
                                        {{ $alert["msg"] }}
                                    </div>
                                @endforeach
                            </div>

                            {{{ $form }}}

                            <p class="text-right p-t-10">
                                <a href="{{ routerLink('admin.resetPasswordView') }}" class="text-underline">@__(auth.resetPassword)</a>
                            </p>
                        </div>

                    </div>
                </div>
                <div class="col-lg-8 d-none d-md-block bg-cover" style="background-image: url('{{ $publicURL }}assets/img/login.svg');"></div>
            </div>
        </div>
    </main>

@endsection
