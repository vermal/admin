@extends('layout')

@section('body')

   <div class="row">
       <div class="col-12">
           <div class="card shadow-lg pt-2 pb-5">
               <div class="card-body">
                   <h3>@__(global.welcome) {{ $currentUser->name }} </h3>
               </div>
           </div>
       </div>
      {{--<div class="col-lg-8 ">--}}
         {{--<div class="card shadow-lg m-b-30">--}}
            {{--<div class="card-body">--}}

               {{--<h3>Quarterly User Growth </h3>--}}
               {{--<p>--}}
                  {{--Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br> A asperiores atque--}}
                  {{--corporis doloribus error--}}
               {{--</p>--}}
               {{--<div id="chart-05"></div>--}}
            {{--</div>--}}
            {{--<div class="card-footer">--}}
               {{--<div class="d-flex  justify-content-between">--}}
                  {{--<h6 class="m-b-0 my-auto"><span class="opacity-75"> <i class="mdi mdi-information"></i> Restart your Re-targeting Campaigns</span>--}}
                  {{--</h6>--}}
                  {{--<a href="#!" class="btn btn-white shadow-none">See Campaigns</a>--}}
               {{--</div>--}}
            {{--</div>--}}
         {{--</div>--}}
         {{--<div class="card shadow-lg m-b-30">--}}
            {{--<div class="card-body">--}}

               {{--<h3> Revenue Retains </h3>--}}
               {{--<p>--}}
                  {{--Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br> A asperiores atque--}}
                  {{--corporis doloribus error--}}
               {{--</p>--}}
               {{--<div id="chart-06"></div>--}}
            {{--</div>--}}
            {{--<div class="card-footer">--}}
               {{--<div class="d-flex  justify-content-between">--}}
                  {{--<h6 class="m-b-0 my-auto"><span class="opacity-75"> <i class="mdi mdi-information"></i> Revenue is down from last QTY</span>--}}
                  {{--</h6>--}}
                  {{--<a href="#!" class="btn btn-white shadow-none">Previous Reports</a>--}}
               {{--</div>--}}
            {{--</div>--}}
         {{--</div>--}}
         {{--<div class="card shadow-lg m-b-30">--}}
            {{--<div class="card-body">--}}

               {{--<h3> Revenue Retains </h3>--}}
               {{--<p>--}}
                  {{--Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br> A asperiores atque--}}
                  {{--corporis doloribus error--}}
               {{--</p>--}}
               {{--<div id="chart-07"></div>--}}
            {{--</div>--}}
            {{--<div class="card-footer">--}}
               {{--<div class="d-flex  justify-content-between">--}}
                  {{--<h6 class="m-b-0 my-auto"><span class="opacity-75"> <i class="mdi mdi-information"></i> Revenue is down from last QTY</span>--}}
                  {{--</h6>--}}
                  {{--<a href="#!" class="btn btn-white shadow-none">Previous Reports</a>--}}
               {{--</div>--}}
            {{--</div>--}}
         {{--</div>--}}
         {{--<div class="card shadow-lg m-b-30">--}}
            {{--<div class="card-body">--}}

               {{--<h3> Traffic Overview </h3>--}}
               {{--<p>--}}
                  {{--Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br> A asperiores atque--}}
                  {{--corporis doloribus error--}}
               {{--</p>--}}
               {{--<div id="chart-08"></div>--}}
            {{--</div>--}}
            {{--<div class="card-footer">--}}
               {{--<div class="d-flex  justify-content-between">--}}
                  {{--<h6 class="m-b-0 my-auto"><span class="opacity-75"> <i class="mdi mdi-information"></i>  Revenue is down from last QTY</span>--}}
                  {{--</h6>--}}
                  {{--<a href="#!" class="btn btn-white shadow-none">Previous Reports</a>--}}
               {{--</div>--}}
            {{--</div>--}}
         {{--</div>--}}
      {{--</div>--}}
      {{--<div class="col-lg-4 m-b-30">--}}
         {{--<div class="card shadow-lg">--}}
            {{--<div class="">--}}
               {{--<div class="p-t-30 p-b-10 text-center">--}}

                  {{--<div class="avatar avatar-lg">--}}
                                 {{--<span class="avatar-title bg-dark rounded-circle">--}}
                                     {{--<i class="mdi mdi-download"></i>--}}
                                 {{--</span>--}}
                  {{--</div>--}}
                  {{--<h5 class="text-center p-t-10 ">Download The Report</h5>--}}
               {{--</div>--}}

               {{--<div class="bg-dark rounded-bottom card-body text-white">--}}
                  {{--<p class="opacity-75">--}}
                     {{--Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci cumque cupiditate--}}
                     {{--dolores ducimus possimus, rem ut. At blanditiis cumque deserunt, dolor in iure--}}
                     {{--nesciunt, odit porro quasi rerum velit veniam.--}}
                  {{--</p>--}}
                  {{--<a href="#" class="btn btn-white-translucent btn-block btn-lg">Download PDF</a>--}}
               {{--</div>--}}
            {{--</div>--}}


         {{--</div>--}}
      {{--</div>--}}
   </div>

@endsection
