@extends('layout')

@section('body')

    <div class="container">
        <div class="row m-h-100 ">
            <div class="col-md-8 col-lg-4  m-auto">
                <div class="card shadow-lg ">
                    <div class="card-body ">
                        <div class=" padding-box-2 ">
                            <div class="text-center p-b-20 pull-up-sm">
                                <div class="avatar avatar-lg">
                                        <span class="avatar-title rounded-circle bg-pink">
                                            <i class="mdi mdi-key-change"></i>
                                        </span>
                                </div>
                            </div>

                            <h3 class="text-center">@__(auth.mail.passwordReset.title)</h3>
                            @form("reset-password-mail")

                                <div class="col-12">
                                    @foreach($alerts as $key => $alert)
                                        <div class="alert alert-{{ $alert["type"] }}">
                                            {{ $alert["msg"] }}
                                        </div>
                                    @endforeach
                                </div>

                                @label("email")
                                @control("email")

                                <div class="col-12">
                                    <div class="form-group">
                                        <button class="btn text-uppercase btn-block  btn-primary">
                                            @__(auth.mail.passwordReset.button)
                                        </button>
                                    </div>
                                </div>
                            @endform

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('bodyClass', 'bg-pattern')
