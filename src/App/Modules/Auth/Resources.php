<?php

namespace Vermal\Admin\Modules\Auth;


use Vermal\Admin\Defaults\Controller;
use Vermal\Admin\Sanitizer;
use Vermal\Database\Database;
use Vermal\Database\Entity;
use Vermal\DataGrid;
use Vermal\Form\Form;
use Vermal\Router;
use Vermal\Admin\View;


class Resources extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->requiredPermission('resource', $this->CRUDAction);
        $this->addToBreadcrumb('resource', 'admin.resource.index');
    }

    /**
     * Browse all resources
     */
    public function index()
    {
        // Create new data grid
        $datagrid = new DataGrid(Database::Resource()->order('r.id', 'DESC'));

        // Add columns
        $datagrid->addColumn('id', 'ID');
        $datagrid->addColumn('displayName', $this->_('auth.resource.name'));
        $datagrid->addColumn('authority', $this->_('auth.resource.authority'));

        // Add actions
        $datagrid->addEdit(['admin.resource.edit', ['id']]);
        $datagrid->addDelete(['admin.resource.destroy_', ['id']]);

        // Return view
        View::view('crud.read', [
            'datagrid' => $datagrid->build()
        ]);
    }

    /**
     * Show create page
     */
    public function create()
    {
        $form = $this->form();
        View::view('crud.create', [
            "form" => $form->build()
        ]);
    }

    /**
     * Store resource
     *
     * @param array $post
     */
    public function store($post)
    {
        $form = $this->form();
        if ($form->hasError()) {
            Router::redirect('admin.resource.create');
        }

        // Create new resource
        $resource = Entity::getEntity("Resource");

        $resource->displayName = Sanitizer::applyFilter('trim|escape|strip_tags', $post->displayName);
        $resource->name = self::slugify($resource->displayName);
        $resource->authority = Sanitizer::applyFilter('digit', $post->authority);

        // Save resource
        Database::saveAndFlush($resource);

        // Redirect to browse view
        Router::redirect('admin.resource.index');
    }

    /**
     * Edit view for resource
     *
     * @param $id
     */
    public function edit($id)
    {
        $resource = Database::Resource()->find($id);
        $form = $this->form();

        // Add to breadcrumb
        $this->addToBreadcrumb('dyn', 'admin.resource.edit', ['id' => $id], [$resource->displayName]);

        // Set values
        $form->setValues($resource);

        // Set edit action
        $form->setAction(Router::link('admin.resource.update', ['id' => $id]));
        $form->setMethod('PUT');

        View::view('crud.edit', [
            "form" => $form->build()
        ]);
    }

    /**
     * Update resource
     *
     * @param array $post
     * @param $id
     */
    public function update($post, $id)
    {
        $form = $this->form();

        // Check if for has some errors
        if ($form->hasError()) {
            Router::redirect('admin.resource.edit', ['id' => $id]);
        }

        // Create new user
        $resource = Database::Resource()->find($id);

        $resource->displayName = Sanitizer::applyFilter('trim|escape|strip_tags', $post->displayName);
        $resource->name = self::slugify($resource->displayName);
        $resource->authority = Sanitizer::applyFilter('digit', $post->authority);

        // Update user
        Database::saveAndFlush($resource);

        // Redirect to user edit
        Router::redirect('admin.resource.edit', ['id' => $id]);
    }

    /**
     * Delete resource
     *
     * @param $id
     */
    public function destroy($id)
    {
        $permissions = Database::Permission()->where('p.resource', $id)->get();

        if (!empty((array)$permissions)) {
            $this->alert($this->_('auth.resource.unable_to_delete'), 'danger');
            Router::redirect('admin.resource.index');
        }

        Database::Resource()->delete($id);
        Router::redirect('admin.resource.index');
    }

    /**
     * Create register form
     *
     * @return Form
     */
    private function form()
    {
        $form = new Form('resource', Router::link('admin.resource.store'));

        // Login info
        $form->addText('displayName', $this->_('auth.resource.name'))->setCol('col-md-6')->min(2);
        $form->addText('authority', $this->_('auth.resource.authority'))->setCol('col-md-6')->int();

        // Add submit button
        $form->addButton('submit', $this->_('auth.resource.save'))->setClass('btn btn-success');

        return $form;
    }

}
