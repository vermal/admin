<?php

namespace Vermal\Admin\Modules\Auth;


use Vermal\Admin\Defaults\Controller;
use Vermal\Admin\Modules\Auth\Entities\Permission\Permission;
use Vermal\Admin\Modules\Auth\Entities\Role\Role;
use Vermal\Admin\Sanitizer;
use Vermal\Database\Database;
use Vermal\Database\Entity;
use Vermal\DataGrid;
use Vermal\Form\Form;
use Vermal\Router;
use Vermal\Admin\View;


class Roles extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->requiredPermission('role', $this->CRUDAction);
        $this->addToBreadcrumb('role', 'admin.role.index');
    }

    private function getResources()
    {
        return Database::Resource()
            ->where('r.authority', '>=', Auth::getUser()->authority)
            ->get();
    }

    /**
     * Browse all roles
     */
    public function index()
    {
        // Create new data grid
        $datagrid = new DataGrid(Database::Role()
            ->where('r.authority', '>=', Auth::getUser()->authority)
            ->order('r.id', 'DESC'));

        // Add columns
        $datagrid->addColumn('id', 'ID');
        $datagrid->addColumn('displayName', $this->_('auth.role.name'));

        // Add actions
        $datagrid->addEdit(['admin.role.edit', ['id']]);
        $datagrid->addDelete(['admin.role.destroy_', ['id']]);

        // Return view
        View::view('crud.read', [
            'datagrid' => $datagrid->build()
        ]);
    }

    /**
     * Show create page
     */
    public function create()
    {
        $form = $this->form();
        View::view('crud.create', [
            "form" => $form->build()
        ]);
    }

    /**
     * Store role
     *
     * @param array $post
     */
    public function store($post)
    {
        $form = $this->form();
        if ($form->hasError()) {
            Router::redirect('admin.role.create');
        }

        // Get all resources
        $resources = $this->getResources();

        // Create new role
        $role = Entity::getEntity('Role');

        $role->displayName = Sanitizer::applyFilter('trim|escape|strip_tags', $post->displayName);
        $role->name = self::slugify($role->displayName);
        $role->authority = Sanitizer::applyFilter('digit', $post->authority);
        $role = $this->assignCRUD($resources, $post, $role);

        // Save role
        Database::saveAndFlush($role);

        // Redirect to browse view
        Router::redirect('admin.role.index');
    }

    /**
     * Edit view for role
     *
     * @param $id
     */
    public function edit($id)
    {
        $role = Database::Role()->find($id);
        if ($role->authority < Auth::getUser()->authority) {
            $this->alert($this->_('global.low_permissions'), 'warning');
            Router::redirect('admin.role.index');
        }

        $form = $this->form();

        // Add to breadcrumb
        $this->addToBreadcrumb('dyn', 'admin.role.edit', ['id' => $id], [$role->displayName]);

        // Set values
        $resources = $this->getResources();

        $skip = [];
        foreach ($resources as $resource) {
            // Skip setting values
            $skip[] = 'create_' . $resource->name;
            $skip[] = 'read_' . $resource->name;
            $skip[] = 'update_' . $resource->name;
            $skip[] = 'delete_' . $resource->name;

            // Set values
            $permission = $this->findPermission($resource, $role);
            if (!empty($permission)) {
                $form->setValue($permission->create, 'create_' . $resource->name);
                $form->setValue($permission->read, 'read_' . $resource->name);
                $form->setValue($permission->update, 'update_' . $resource->name);
                $form->setValue($permission->delete, 'delete_' . $resource->name);
            }
        }
        $form->setValues($role, $skip);

        // Set edit action
        $form->setAction(Router::link('admin.role.update', ['id' => $id]));
        $form->setMethod('PUT');
        $form->setAjax(true);

        View::view('crud.edit', [
            "form" => $form->build()
        ]);
    }

    private function findPermission($resource, $role)
    {
        foreach ($role->permissions as $permission) {
            if ($permission->resource->id == $resource->id) {
                return $permission;
            }
        }
        return null;
    }

    /**
     * Update role
     *
     * @param array $post
     * @param $id
     */
    public function update($post, $id)
    {
        $form = $this->form();

        // Check if for has some errors
        if ($form->hasError()) {
            $this->alert($this->_('error.form.wrong'), 'danger');
            die();
        }

        // Get all resources
        $resources = $this->getResources();

        // Create new user
        $role = Database::Role()->find($id);
        if ($role->authority < Auth::getUser()->authority) {
            $this->alert($this->_('global.low_permissions'), 'warning');
            Router::redirect('admin.role.index');
        }

        $permissionsToDelete = [];
        foreach ($role->permissions as $permission) {
            $permissionsToDelete[] = $permission->id;
        }

        $role->displayName = Sanitizer::applyFilter('trim|escape|strip_tags', $post->displayName);
        $role->name = self::slugify($role->displayName);
        $role->authority = Sanitizer::applyFilter('digit', $post->authority);

        // Delete permissions
        Database::deleteItems($role, 'permissions');

        $role = $this->assignCRUD($resources, $post, $role);

        // Update user
        Database::saveAndFlush($role);
        Database::Permission()->where('p.id', $permissionsToDelete)->delete();

        // Redirect to user edit
        $this->alert($this->_('auth.role.updated'));
    }

    /**
     * @param $resources
     * @param $post
     * @param $role
     * @return mixed
     */
    private function assignCRUD($resources, $post, $role)
    {
        foreach ($resources as $resource) {
            // Skip if nothing is set
            if (!isset($post->{'create_' . $resource->name}) && !isset($post->{'read_' . $resource->name}) &&
                !isset($post->{'update_' . $resource->name}) && !isset($post->{'delete_' . $resource->name})) {
                continue;
            }

            if (!(int)$post->{'create_' . $resource->name} && !(int)$post->{'read_' . $resource->name} &&
                !(int)$post->{'update_' . $resource->name} && !(int)$post->{'delete_' . $resource->name}) {
                continue;
            }
            $permission = Entity::getEntity('Permission');

            $permission->create = (int)$post->{'create_' . $resource->name};
            $permission->read = (int)$post->{'read_' . $resource->name};
            $permission->update = (int)$post->{'update_' . $resource->name};
            $permission->delete = (int)$post->{'delete_' . $resource->name};

            $permission->resource = $resource;

            $role->addPermission($permission);
        }
        return $role;
    }

    /**
     * Delete role
     *
     * @param $id
     */
    public function destroy($id)
    {
        $role = Database::Role()->find($id);
        if ($role->authority <= Auth::getUser()->authority) {
            $this->alert($this->_('global.low_permissions'), 'warning');
            Router::redirect('admin.role.index');
        }

        // Check if role has some users
        $users = Database::User()->where('u.role', $id)->get();

        // Generate error message if some users are assigned to this role
        if (!empty((array)$users)) {
            $usersWithRole = '';
            foreach ($users as $user) {
                $usersWithRole .= $user->email . ', ';
            }
            $usersWithRole = rtrim($usersWithRole, ', ');

            $this->alert(sprintf($this->_('auth.role.unable_to_delete'), $usersWithRole), 'danger');
            Router::redirect('admin.role.index');
        }


        $permissionsToDelete = [];
        foreach ($role->permissions as $permission) {
            $permissionsToDelete[] = $permission->id;
        }

        // Delete associations
        Database::deleteItems($role, 'permissions');
        Database::saveAndFlush($role);
        Database::Permission()->where('p.id', $permissionsToDelete)->delete();


        // Delete role
        Database::Role()->delete($id);

        Router::redirect('admin.role.index');
    }

    /**
     * Create register form
     *
     * @return Form
     */
    private function form()
    {
        $form = new Form('role', Router::link('admin.role.store'));

        // Login info
        $form->addText('displayName', $this->_('auth.role.name'))->setCol('col-md-6')->min(2);
        $form->addText('authority', $this->_('auth.role.authority'))->setCol('col-md-6');

        // Get all resources
        $resources = $this->getResources();

        foreach ($resources as $resource) {
            $form->startGroup('group_' . $resource->name, 'col-lg-3');

            $form->addHeading('3', $resource->displayName)->setClass('col-12');

            if (Auth::isAuthorized($resource->name, 'create') || Auth::hasRole('developer'))
                $form->addCheckboxSwitch('create_' . $resource->name, 'Create');
            if (Auth::isAuthorized($resource->name, 'read') || Auth::hasRole('developer'))
                $form->addCheckboxSwitch('read_' . $resource->name, 'Read');
            if (Auth::isAuthorized($resource->name, 'update') || Auth::hasRole('developer'))
                $form->addCheckboxSwitch('update_' . $resource->name, 'Update');
            if (Auth::isAuthorized($resource->name, 'delete') || Auth::hasRole('developer'))
                $form->addCheckboxSwitch('delete_' . $resource->name, 'Delete');

            $form->endGroup();
        }

        // Add submit button
        $form->addButton('submit', $this->_('auth.role.save'))->setClass('btn btn-success');


        return $form;
    }

}
