<?php

namespace Vermal\Admin\Modules\Auth;


use Gregwar\Image\Source\Data;
use Vermal\Admin\Defaults\Controller;
use Vermal\App;
use Vermal\Database\Database;
use Vermal\Form\Form;
use Vermal\Router;
use Vermal\Admin\View;


class Auth extends Controller
{

    public $defaultViewPath = false;

    /**
     * Check if is user logged in
     *
     * @return bool
     */
    public static function isLoggedIn()
    {
        return Auth::getUser() && isset(Auth::getUser()->role) && isset(Auth::getUser()->permissions);
    }

    /**
     * Check if user is authorized to do this action
     *
     * @param $permission
     * @param $action
     * @return bool
     */
    public static function isAuthorized($permission, $action = null)
    {
        if (!self::isLoggedIn()) return false;
        else if (is_array($permission)) {
            foreach ($permission as $k => $v) {
                if (!self::isAuthorized($v, null)) {
                    return false;
                }
            }
            return true;
        }
        else {
            if (Auth::getUser() && !is_array($permission) && array_key_exists($permission, Auth::getUser()->permissions)) {
                $permission = Auth::getUser()->permissions[$permission];
                if ($action !== null && $permission->{$action} == 0) return false;
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Get user
     *
     * @return mixed
     */
    public static function getUser()
    {
        return isset($_SESSION['user']) ? $_SESSION['user'] : false;
    }

    /**
     * Chcek if user has role
     *
     * @param $role
     * @return bool
     */
    public static function hasRole($role)
    {
        if (!self::isLoggedIn()) return false;
        else if (is_array($role)) {
            foreach ($role as $r) {
                if (self::hasRole($r)) {
                    return true;
                }
            }
            return false;
        }
        else {
            if (Auth::getUser() && !is_array($role) && Auth::getUser()->role->name == $role) {
                return true;
            }
        }
        return false;
    }

    /**
     * Dashboard
     */
    public function index()
    {
        $this->requiredPermission('dashboard');
        $data = apply_filters('admin_dashboard_data', [
			"name" => $this->user->name
		]);
        echo apply_filters(
        	'admin_dashboard_template',
			View::view('dashboard', $data, true)
		);
    }

    /**
     * Show login page
     */
    public function loginView()
    {
        View::view('login', [
            "form" => $this->form()->build()
        ]);
    }

    /**
     * Login user based on his role
     *
     * @param array $post
     */
    public function login($post)
    {
        $form = $this->form();
        if ($form->hasError()) {
            Router::redirect('admin.loginView', [
				'r' => Router::$currentName
			]);
        }

        $user = Database::User()->where('u.email', $post->email)->first();
        if (empty((array)$user)) {
            $form->setGlobalError($this->_('auth.login.wrong_credentials'),'danger');
            Router::redirect('admin.loginView');
        }

        if (!password_verify($post->password, $user->password)) {
            $form->setGlobalError($this->_('auth.login.wrong_credentials'),'danger');
            Router::redirect('admin.loginView');
        }

        $_SESSION['user'] = $this->userEntityToStdClass($user);
        $this->alert($this->_('auth.login.loggedIn'),'success');

        if (empty($post->r)) {
            Router::redirect('admin.dashboard');
        } else {
            Router::redirect($post->r);
        }
    }

    /**
     * Create login form
     *
     * @return Form
     */
    private function form()
    {
        $form = new Form('login', Router::link('admin.login'));

        $form->addEmail('email', $this->_('auth.login.email'))->email();
        $form->addPassword('password', $this->_('auth.login.password'))->min(8);
        $form->addHidden('r', Router::getParam('r'));

        $form->addButton('submit', $this->_('global.form.login'))->setClass('btn btn-primary btn-block btn-lg');

        return $form;
    }

    /**
     * @param $user
     * @return \stdClass $saveUser
     */
    private function userEntityToStdClass($user)
    {
        $saveUser = new \stdClass();

        $saveUser->id = $user->id;
        $saveUser->email = $user->email;
        $saveUser->name = $user->name;
        $saveUser->phone = $user->phone;
        $saveUser->role = new \stdClass();

        $saveUser->role->id = $user->role->id;
        $saveUser->role->name = $user->role->name;
        $saveUser->permissions = [];
        $saveUser->authority = $user->role->authority;

        foreach ($user->role->permissions as $key => $permission) {
            $savePermission = new \stdClass();

            $savePermission->create = $permission->create;
            $savePermission->read = $permission->read;
            $savePermission->update = $permission->update;
            $savePermission->delete = $permission->delete;
            $savePermission->name = $permission->resource->name;

            $saveUser->permissions[$permission->resource->name] = $savePermission;
        }

        return $saveUser;
    }

    /**
     * Forgot password view
     */
    public function resetPasswordView()
    {
        View::view('reset-password', [
            "form" => $this->resetPasswordForm()->build()
        ]);
    }

    /**
     * Reset password
     *
     * @param $post
     * @throws \Exception
     */
    public function resetPasswordMail($post)
    {
        $form = $this->resetPasswordForm();
        if ($form->hasError()) Router::redirect('admin.resetPasswordView');

        // Get user
        $user = Database::Model('User')->where('u.email', $post->email)->first();
        if ($user !== null) {
            $token = uniqid();
            $user->resetToken = $token;
            $user->resetTokenValidUntil = (new \DateTime())->modify('+30 minutes');

            $mail = parent::mailer();
            $mail->body($this->resetPasswordGenerateMail($user))
                ->from($this->settings->mailer_username, 'System')
                ->addAddress($user->email, $user->name)
                ->subject($this->_('auth.mail.passwordReset.title'));

            try {
                $mail->send();
            } catch (\Exception $e) {
                $this->alert($this->_('auth.mail.passwordReset.error'), 'danger');
                Router::redirect('admin.resetPasswordView');
            }

            // Save user
            Database::saveAndFlush($user);
        }

        $this->alert($this->_('auth.mail.passwordReset.success'), 'success');
        Router::redirect('admin.loginView');
    }

    /**
     * Generate password reset email
     *
     * @param $user
     * @return string
     * @throws \Whoops\Exception\ErrorException
     */
    public function resetPasswordGenerateMail($user)
    {
        $mail = View::view('mail.password-reset', [
            'resetLink' => routerLink('admin.resetPasswordFromMailView', ['token' => $user->resetToken]),
            'user' => $user
        ], true);
        return $mail;
    }

    /**
     * @return Form
     */
    private function resetPasswordForm()
    {
        $form = new Form('reset-password-mail', routerLink('resetPasswordMail'));

        $form->addText('email', 'Email')
            ->addAttribute('placeholder', 'email@example.com')
            ->required();

        return $form;
    }

    /**
     * Forgot password view
     *
     * @param $token
     * @throws \Exception
     */
    public function resetPasswordFromMailView($token)
    {
        View::view('reset-password-from-mail', [
            "form" => $this->resetPasswordFromMailForm($token)->build()
        ]);
    }

    /**
     * Finally reset password for user
     *
     * @param $post
     */
    public function resetPasswordFromMail($post)
    {
        // Check if link is valid
        $user = Database::Model('User')->where('u.resetToken', $post->token)->first();

        // Token does not exist
        if ($user == null) {
            $this->alert($this->_('auth.resetPasswordMsg.error'), 'danger');
            Router::redirect('admin.loginView');
        }

        // Token has expired
        if($user->resetTokenValidUntil->getTimestamp() < strtotime("-30 minutes")) {
            $this->alert($this->_('auth.resetPasswordMsg.error'), 'danger');
            Router::redirect('admin.loginView');
        }

        // Passwords doesn't match
        if ($post->pwd !== $post->pwd_check) {
            $this->alert($this->_('auth.resetPasswordMsg.errorPwd'), 'warning');
            Router::redirect('admin.resetPasswordFromMailView');
        }

        // Save password
        $user->resetToken = null;
        $user->resetTokenValidUntil = null;
        $user->password = password_hash($post->pwd, ADMIN_HASH);
        Database::saveAndFlush($user);

        // Redirect to login page
        $this->alert($this->_('auth.resetPasswordMsg.success'), 'info');
        Router::redirect('admin.loginView');
    }

    /**
     * @param $token
     * @return Form
     */
    private function resetPasswordFromMailForm($token)
    {
        $form = new Form('reset-password-from-mail', routerLink('resetPasswordFromMail'));

        $form->addPassword('pwd', 'Password')->required();
        $form->addPassword('pwd_check', 'Password again')->required();
        $form->addHidden('token', $token);

        return $form;
    }

}
