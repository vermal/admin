@extends('layout')

@section('body')

    <div class="container">
        <div class="card shadow-lg m-b-30 p-4">
            <ul class="nav nav-tabs tab-line" id="myTab" role="tablist">
                @foreach($languages as $lang)
                    <li class="nav-item">
                        <a class="nav-link {{ $loop->first ? "show active" : "" }}" id="tab-{{ $lang }}" data-toggle="tab" href="#{{ $lang }}" role="tab" aria-controls="{{ $lang }}" aria-selected="true">
                            {{ strtoupper($lang) }}
                        </a>
                    </li>
                @endforeach
            </ul>
            @form("text")
                <div class="col-12 tab-content pt-4" id="myTabContent">

                    @foreach($languages as $lang)

                        <div class="tab-pane row fade {{ $loop->first ? "show active" : "" }}" id="{{ $lang }}" role="tabpanel" aria-labelledby="tab-{{ $lang }}">
                            @label("{$lang}_title")
                            @control("{$lang}_title")

                            @label("{$lang}_te")
                            @control("{$lang}_te")
                        </div>

                    @endforeach

                </div>

                @label("place")
                @control("place")

                <div class="col-12 text-right mt-4">
                    @control("submit")
                </div>
            @endform
        </div>
    </div>


    <script>
        $(document).ready(function() {
            $('#form-settings').on('fields-error', function(res, data) {
                let tabId = data.fGroup.closest('.tab-pane').attr('id');
                $('#tab-' + tabId).click();
            });
        })
    </script>

@endsection
