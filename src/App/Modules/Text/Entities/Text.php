<?php

namespace Vermal\Admin\Modules\Text\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Entities\Translatable;
use Vermal\Database\MagicAccessor;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity @ORM\Table(name="content_text")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\TranslationEntity(class="\TextTr")
 **/
class Text
{
    use MagicAccessor;
    use Translatable;

    /** @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue **/
    protected $id;

	/**
	 * @Gedmo\Translatable
	 * @ORM\Column(type="string", nullable=true) *
	 */
	protected $title;

	/**
	 * @Gedmo\Translatable
	 * @ORM\Column(type="text", nullable=true) *
	 */
	protected $te;

	/** @ORM\Column(type="string", nullable=true) **/
	protected $place;

	/** @ORM\Column(type="integer", name="sort") **/
	protected $order;

	/** @ORM\Column(type="integer", options={"default" : 1}) **/
	protected $active;

	/**
	 * @ORM\OneToMany(
	 *   targetEntity="\TextTr",
	 *   mappedBy="object",
	 *   cascade={"persist", "remove"},
	 *   orphanRemoval=true
	 * )
	 */
	protected $translations;

	public function __construct()
	{
		$this->translations = new ArrayCollection();
	}
}
