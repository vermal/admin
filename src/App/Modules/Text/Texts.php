<?php

namespace Vermal\Admin\Modules\Text;


use Vermal\Admin\Defaults\Controller;
use Vermal\Admin\Defaults\Languages;
use Vermal\Admin\Modules\Text\Entities\Text;
use Vermal\Admin\Modules\Text\Entities\TextTr;
use Vermal\Admin\Sanitizer;
use Vermal\App;
use Vermal\Database\Database;
use Vermal\Database\Entity;
use Vermal\DataGrid;
use Vermal\Form\Form;
use Vermal\Router;
use Vermal\Admin\View;
use Vermal\Traits\DataGridSort;


class Texts extends Controller
{

	use DataGridSort;

    protected $defaultViewPath = false;

    public function __construct()
    {
        parent::__construct();
//        $this->requiredPermission('text', $this->CRUDAction);
        $this->addToBreadcrumb('text', 'admin.text.index');
    }

    /**
     * Browse all users
     */
    public function index()
    {
        $this->setDefaultViewPath();

        // Create new data grid
        $datagrid = new DataGrid(Database::Model('Text')->order('t.order', 'ASC'));

        // Add columns
        $datagrid->addColumn('id', 'ID');
        $datagrid->addColumn('title', $this->_('text.title'));
		$datagrid->addColumn('te', $this->_('text.te'))->setRenderer(function($entity) {
			if (empty($entity->te)) return null;
			return substr(strip_tags($entity->te), 0, 50) . ' ...';
		});
		$datagrid->addColumn('place', $this->_('text.place'))->setRenderer(function($entity) {
			return apply_filters('admin_text_places', [null => '-'])[$entity->place];
		});

        // Add actions
		$datagrid->addSortable(['admin.text.sort']);
		$datagrid->addEdit(['admin.text.edit', ['id']]);
		$datagrid->addDelete(['admin.text.destroy_', ['id']]);
		$datagrid->addSwitch(['admin.switch', ['id'], ['c' => str_replace('\\', '__', self::class)]],
			'active');

		// Add filter
		$datagrid->addFilter('place', apply_filters('admin_text_places', []), $this->_('text.place'));

        // Return view
        View::view('crud.read', [
            'datagrid' => $datagrid->build()
        ]);
    }

    /**
     * Show login page
     */
    public function create()
    {
        $form = $this->form();
        View::view('texts', [
            "form" => $form->build()
        ]);
    }

    /**
     * Login user based on his role
     *
     * @param array $post
     */
    public function store($post)
    {
        $form = $this->form();
        if ($form->hasError()) {
            Router::redirect('admin.text.create');
        }

        $text = Entity::getEntity('Text');

		$text->order = $this->getMaxValue('Text');
		$text->active = 1;

		if (!empty($post->place))
			$text->place = Sanitizer::applyFilter('trim|escape|strip_tags', $post->place);

        // Add translations
        foreach (Languages::get() as $lang) {
            $entity = 'TextTr';
			Database::translate($text, $entity, 'title', $lang, $this->sanitize('strip_tags|trim', $post->{$lang . '_title'}));
			Database::translate($text, $entity, 'te', $lang, $this->sanitize('trim', $post->{$lang . '_te'}));
        }

        Database::saveAndFlush($text);

        // Redirect with message
        $this->alert($this->_('text.created'));
        Router::redirect('admin.text.index');
    }

    /**
     * Show use dit page
     *
     * @param $id
     * @throws
     */
    public function edit($id)
    {
        $form = $this->form();
        $form->setAjax(true);
        $form->setMethod('PUT');
        $form->setAction(routerLink('admin.text.update', ['id' => $id]));

        $text = Database::Model('Text')->find($id);

        // Add to breadcrumb
        $this->addToBreadcrumb('dyn', 'admin.text.edit', ['id' => $id], [$text->title]);

        // Set values
        foreach (Languages::get() as $lang) {
            $form->getComponent($lang . '_title')->setValue($text->getTr($lang, 'title'));
            $form->getComponent($lang . '_te')->setValue($text->getTr($lang, 'te'));
        }

        $form->setValues([
        	'place' => $text->place
		]);

        View::view('texts', [
            "form" => $form->build()
        ]);
    }

    /**
     * Update user
     *
     * @param array $post
     * @param $id
     */
    public function update($post, $id)
    {
        $form = $this->form();
        if ($form->hasError()) {
            $this->alert($this->_('error.form.empty'), 'danger');
            die();
        }

        // Find text
		/** @var Text $text */
        $text = Database::Model('Text')->find($id);

        if (!empty($post->place))
        	$text->place = Sanitizer::applyFilter('trim|escape|strip_tags', $post->place);

        // Clear all translations and add new ones
        $text->clearTr();
        Database::saveAndFlush($text);
        foreach (Languages::get() as $lang) {
			$entity = 'TextTr';
			Database::translate($text, $entity, 'title', $lang, $this->sanitize('strip_tags|trim', $post->{$lang . '_title'}));
			Database::translate($text, $entity, 'te', $lang, $this->sanitize('trim', $post->{$lang . '_te'}));
        }

        $this->alert($this->_('text.edited'));
        Database::saveAndFlush($text);
    }

    /**
     * Delete menu
     *
     * @param $id
     */
    public function destroy($id)
    {
        $text = Database::Model('Text')->find($id);
        $text->clearTr();
        Database::saveAndFlush($text);
        Database::Model('Text')->delete($id);

        // Redirect
        $this->alert($this->_('text.deleted'));
        Router::redirect('admin.text.index');
    }

    /**
     * Create register form
     *
     * @return Form
     */
    private function form()
    {
        $form = new Form('text', Router::link('admin.text.store'));

        // Texts Data
        foreach (Languages::get() as $lang) {
            $form->addText($lang . '_title', $this->_('text.title'));
            $form->addTextEditor($lang . '_te', $this->_('text.te'));
        }
        $form->addSelect('place', apply_filters('admin_text_places', [null => '-']), $this->_('text.place'));

        // Add submit button
        $form->addButton('submit', $this->_('global.form.save'))->setClass('btn btn-success');

        return $form;
    }

    /**
     * Edit single text
     *
     * @param $post
     */
    public function editSingle($post)
    {
        $id = $post->id;
        $type = $post->type;
        $locale = $post->locale;
        $value = $post->value;

        $text = Database::Model('Text')->find($id);
        if ($text !== null) {
        	foreach ($text->translations as $translation) {
        		if ($translation->getLocale() == $locale && $translation->getField() == $type) {
        			if (App::get('locale') == $locale) {
        				$text->{$type} = $value;
        				Database::saveAndFlush($text);
					}
					$translation->setContent($value);
					Database::saveAndFlush($translation);
				}
			}
        }
    }

	/**
	 * @param $post
	 */
	public function sort($post)
	{
		$prev_id = isset($post->prev_id) ? $post->prev_id : false;
		$next_id = isset($post->next_id) ? $post->next_id : false;
		$this->sortHelper('Text', $post->item_id, $prev_id, $next_id);
	}

	/**
	 * @param $id
	 */
	public static function switchItem($id)
	{
		$entity = Database::Model('Text')->find($id);
		if (!empty($entity)) {
			$entity->active = !$entity->active;
			Database::saveAndFlush($entity);
		}
		Router::redirectToURL(Router::prevLink());
	}
}
