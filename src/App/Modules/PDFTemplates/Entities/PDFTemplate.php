<?php

namespace Vermal\Admin\Modules\PDFTemplates\Entities;

use Vermal\Admin\Defaults\Model;

/**
 * @ORM\Entity @ORM\Table(name="pdftemplate")
 * @ORM\HasLifecycleCallbacks
 **/
class PDFTemplate extends Model
{
    /** @ORM\Column(type="string") **/
    protected $title;

    /** @ORM\Column(type="text") **/
    protected $design;

    /** @ORM\Column(type="array") **/
    protected $methods;
}
