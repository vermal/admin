@extends('layout')

@section('body')

    <div class="card shadow-lg m-b-30 p-4 col-lg-10 top-form-wrapper">
        {{{ $form }}}
        <div id="fpdf_designer_output" class="p-4 d-none">
            <div id="output_content"></div>
        </div>
    </div>

    <div class="show-fpdf-editor">
        <div class="m-0 row editor-wrapper">
            <div class="card shadow-lg m-b-30 p-4 col-lg-10">
                <div class="row">
                    <!-- - - - - - - - - - - - - - - - - - - - - - - -->
                    <!-- ruler grid vertical -->
                    <!-- - - - - - - - - - - - - - - - - - - - - - - -->
                    <div class="col ruler-col p-0 d-flex pt-4">
                        <div class="d-flex">
                            <div class="ruler-vertical">
                                <div class="ruler_cm">
                                    <svg width="100%" height="100%" xmlns="http://www.w3.org/2000/svg">
                                        <defs>
                                            <pattern id="cmGrid_vertical" width="15" height="37.79" patternUnits="userSpaceOnUse">
                                                <line x1="0" y1="0" x2="15" y2="0" stroke="black" stroke-width="1" />
                                            </pattern>
                                        </defs>
                                        <rect width="100%" height="100%" fill="url(#cmGrid_vertical)" />
                                    </svg>
                                    <div class="ruler_mm">
                                        <svg width="100%" height="100%" xmlns="http://www.w3.org/2000/svg">
                                            <defs>
                                                <pattern id="mmGrid_vertical" width="3.779" height="3.779" patternUnits="userSpaceOnUse">
                                                    <line x1="0" y1="0" x2="20" y2="0" stroke="black" stroke-width="1" />
                                                </pattern>
                                            </defs>
                                            <rect width="100%" height="100%" fill="url(#mmGrid_vertical)" />
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- - - - - - - - - - - - - - - - - - - - - - - -->
                    <!-- ruler grid horizontal -->
                    <!-- - - - - - - - - - - - - - - - - - - - - - - -->
                    <div class="col p-0">
                        <div>
                            <div class="ruler-horizontal">
                                <div class="ruler_cm">
                                    <svg width="100%" height="100%" xmlns="http://www.w3.org/2000/svg">
                                        <defs>
                                            <pattern id="cmGrid_horizontal" width="37.79" height="15" patternUnits="userSpaceOnUse">
                                                <line x1="0" y1="0" x2="0" y2="15" stroke="black" stroke-width="1" />
                                            </pattern>
                                        </defs>
                                        <rect width="100%" height="100%" fill="url(#cmGrid_horizontal)" />
                                    </svg>
                                    <div class="ruler_mm">
                                        <svg width="100%" height="100%" xmlns="http://www.w3.org/2000/svg">
                                            <defs>
                                                <pattern id="mmGrid_horizontal" width="3.779" height="3.779" patternUnits="userSpaceOnUse">
                                                    <line x1="0" y1="0" x2="0" y2="20" stroke="black" stroke-width="1" />
                                                </pattern>
                                            </defs>
                                            <rect width="100%" height="100%" fill="url(#mmGrid_horizontal)" />
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- - - - - - - - - - - - - - - - - - - - - - - -->
                        <!-- editor -->
                        <!-- - - - - - - - - - - - - - - - - - - - - - - -->
                        <div class="p-4">
                            <div id="fpdf_designer_content">
                                <div id="fpdf_designer_paper_template" class="align-flex"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <!-- - - - - - - - - - - - - - - - - - - - - - - -->
        <!-- menu -->
        <!-- - - - - - - - - - - - - - - - - - - - - - - -->
        <div class="card shadow-lg m-b-30 p-4 fixedElement" style="margin-left: 50px">
            <div id="fpdf_designer_elements" class="position-relative text-right py-4 mb-3">
                <div >
                    {{--<button class="btn btn-primary fdpf-element" id="send_fpdf">Preview</button>--}}
                    {{--<button class="btn btn-danger fdpf-element" id="clear_fpdf">Clear</button>--}}
                    <button class="btn btn-warning fdpf-element" id="save_design">Save</button>
                    {{--<button class="btn btn-warning fdpf-element" id="load_design">Load</button>--}}
                    <button class="btn btn-primary fdpf-element" data-fpdf="cell" data-is-new-element="true">Cell</button>
                    <button class="btn btn-primary fdpf-element" data-fpdf="multicell" data-is-new-element="true">Muliticell</button>
                    <button class="btn btn-primary fdpf-element" data-fpdf="text" data-is-new-element="true">Text</button>
                    <button class="btn btn-primary fdpf-element" data-fpdf="write" data-is-new-element="true">Write</button>
                    <button class="btn btn-primary fdpf-element" data-fpdf="rect" data-is-new-element="true">Rect</button>
                    <button class="btn btn-primary fdpf-element" data-fpdf="link" data-is-new-element="true">Link</button>
                    <button class="btn btn-primary fdpf-element" data-fpdf="line" data-is-new-element="true">Line</button>
                    <button class="btn btn-primary fdpf-element" data-fpdf="image" data-is-new-element="true">Image</button>
                    <button class="btn btn-primary fdpf-element" data-fpdf="ln" data-is-new-element="true">Ln</button>
                    <button class="btn btn-primary fdpf-element" data-fpdf="setfillcolor" data-is-new-element="true">Fill color</button>
                    <button class="btn btn-primary fdpf-element" data-fpdf="setdrawcolor" data-is-new-element="true">Draw color</button>
                    <button class="btn btn-primary fdpf-element" data-fpdf="settextcolor" data-is-new-element="true">Text color</button>
                </div>
            </div>
        </div>

    </div>

    <script>
        $(document).ready(function() {
            let fpdf_designer = $('#fpdf_designer_paper_template').fpdf_designer({
                paper_width: 290,
                paper_height: 210
            });
            try {
                let data = JSON.parse($('input[name="design"]').val());
                fpdf_designer.load(data);
            } catch (e) {}

            // Save data to input
            fpdf_designer.on('fpdf.changed', function() {
                $('input[name="output"]').val($('#output_content').html());
                $('input[name="design"]').val(JSON.stringify(fpdf_designer.returnDesign()));
            });

            $('#save_design').on('click', function(){
                fpdf_designer.saveDesign();
            });
        });

        $(window).scroll(function(e){
            var $el = $('.fixedElement');
            let top = $('.top-form-wrapper').offset().top;
            var isPositionFixed = ($el.css('position') == 'fixed');
            if ($(this).scrollTop() > (top - 60) && !isPositionFixed){
                $el.css({'position': 'fixed', 'top': '100px'});
            }
            else if ($(this).scrollTop() < (top - 59) && isPositionFixed){
                $el.css({'position': 'absolute', 'top': top + 'px'});
            }
        });
    </script>

@endsection


@section('head')

    <style>
        .fixedElement {
            position:fixed;
            top:0;
            right: 30px;
            width: 200px;
            z-index:100;
        }
    </style>

    <script v:admin:public:src="assets/vendor/jquery.contextmenu/jquery.contextmenu.min.js"></script>
    <script v:admin:public:src="assets/vendor/jquery.confirm/jquery.confirm.min.js"></script>
    <script v:admin:public:src="assets/vendor/jquery.wheelcolorpicker/jquery.wheelcolorpicker-3.0.5.min.js"></script>

    <link rel="stylesheet" v:admin:public:href="assets/vendor/jquery.contextmenu/jquery.contextmenu.min.css">
    <link rel="stylesheet" v:admin:public:href="assets/vendor/jquery.confirm/jquery.confirm.min.css">
    <link rel="stylesheet" v:admin:public:href="assets/vendor/jquery.wheelcolorpicker/jquery.wheelcolorpicker.css">

    <script v:admin:public:src="assets/vendor/pdfdesigner/js/fpdf-designer-contextmenus.js"></script>
    <script v:admin:public:src="assets/vendor/pdfdesigner/js/jquery.fpdf-designer.js"></script>

    <link rel="stylesheet" v:admin:public:href="assets/vendor/pdfdesigner/css/fpdf-designer-style.css">
    <link rel="stylesheet" v:admin:public:href="assets/vendor/pdfdesigner/css/ruler.css">
@endsection
