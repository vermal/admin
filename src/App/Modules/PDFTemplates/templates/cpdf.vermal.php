@extends('layout')

@section('body')

    <div class="card shadow-lg m-b-30 p-4 col-lg-10 top-form-wrapper">
        {{{ $form }}}
    </div>

    <div class="card shadow-lg m-b-30 p-4">
        <div id="pdfeditor" class="top-form-wrapper"></div>
    </div>

    <div class="card shadow-lg m-b-30 p-4 fixedElement" style="margin-left: 50px">
        <div class="actions">
            <button class="btn btn-primary cpdf-action" data-type="add-rect">Rect</button>
            <button class="btn btn-primary cpdf-action" data-type="add-text">Text</button>
        </div>
    </div>

    <style>
        @font-face {
            font-family: ArialCE;
            src: url('vendor/vermal/admin/src/public/fonts/arialCE/ArialCE.ttf');
            font-weight: normal;
        }

        .cpdf-editor {
            font-family: 'ArialCE', sans-serif !important;
            position: relative;
        }
        .cpdf-editor .page {
            position: relative;
            overflow: hidden;
            box-shadow: 0 0 5px rgba(0,0,0,.5);
            background-color: #fff;
        }
        .cpdf-editor .cpdf-actions {
            margin-top: 20px;
            margin-bottom: 50px;
        }
        .cpdf-editor .cpdf-actions:last-child {
            margin-bottom: 0;
        }
        .cpdf-editor .page-part {
            border: 1px dashed rgba(0,0,0,.4);
        }
        .cpdf-editor .page-part.active {
            border: 1px solid rgba(0,0,0,.4);
        }

        .cpdf-editor .cpdf_element {
            position: relative;
        }
        .cpdf-editor .cpdf_element:focus {
            outline: none;
        }
        .cpdf-editor .cpdf_element .value {
            position: absolute;
            display: block;
            width: 100%;
            z-index: 0;
            padding: 0 1px;
            top: 0;
            font-size: 12pt;
            line-height: 1em;
        }
        .cpdf-editor .cpdf_element .value.middle {
            top: 50%;
            transform: translateY(-50%);
        }
        .cpdf-editor .cpdf_element .value.bottom {
            top: 100%;
            transform: translateY(-100%);
        }
        .cpdf-editor .cpdf_element .value.bold { font-weight: bold }
        .cpdf-editor .cpdf_element .value.italic { font-style: italic }

        .cpdf-editor .helper-line {
            position: absolute;
            display: none;
            background-color: #0c0c0c;
            opacity: .4;
            width: 100%;
            height: 1px;
            z-index: 9;
        }
        .cpdf-editor .helper-line.horizontal {
            transform: rotate(-90deg);
        }
        .cpdf-editor .center-line {
            position: absolute;
            background-color: pink;
        }
        .cpdf-editor .center-line.center-vertical { top: 50%; margin-top: -0.5px; width: 100%;height: 1px; }
        .cpdf-editor .center-line.center-horizontal { left: 50%; margin-left: -0.5px; width: 1px; height: 100%; }

        .cpdf-editor .cpdf_element .resizers {
            position: relative;
            z-index: 1;
            width: 100%;
            height: 100%;
            box-sizing: border-box;
            cursor: move;
        }
        .cpdf-editor .cpdf_element .resizers:hover .resizer {
            opacity: 1;
        }
        .cpdf-editor .cpdf_element .resizers .resizer{
            opacity: 0;
            width: 10px;
            height: 10px;
            border-radius: 50%; /*magic to turn square into circle*/
            background: white;
            border: 3px solid black;
            position: absolute;
        }
        .cpdf-editor .cpdf_element .resizers .resizer.top-left {
            left: -5px;
            top: -5px;
            cursor: nwse-resize; /*resizer cursor*/
        }
        .cpdf-editor .cpdf_element .resizers .resizer.top-right {
            right: -5px;
            top: -5px;
            cursor: nesw-resize;
        }
        .cpdf-editor .cpdf_element .resizers .resizer.bottom-left {
            left: -5px;
            bottom: -5px;
            cursor: nesw-resize;
        }
        .cpdf-editor .cpdf_element .resizers .resizer.bottom-right {
            right: -5px;
            bottom: -5px;
            cursor: nwse-resize;
        }

        .cpdf-context-menu {
            position: absolute;
            z-index: 999;
            width: 200px;
            background-color: #fff;
            box-shadow: 0 0 10px rgba(0,0,0,.2);
        }
        .cpdf-context-menu .dropdown {
            position: absolute;
            display: none;
            left: 200px;
            margin-top: -41px;
            width: 200px;
            background-color: #fff;
            box-shadow: 0 0 10px rgba(0,0,0,.2);
        }
        .cpdf-context-menu .dropdown:hover {
            display: block;
        }
        .cpdf-context-menu a {
            display: block;
            padding: 10px 5px;
            background-color: #fff;
            cursor: pointer;
        }
        .cpdf-context-menu a:hover {
            background-color: #EEEEEE;
        }
        .cpdf-context-menu a:hover + .dropdown {
            display: block;
        }

        .cpdf-modal {
            position: fixed;
            z-index: 999;
            display: none;
            left: 0;
            top: 0;
            width: 100vw;
            height: 100vh;
            background-color: rgba(0,0,0,.2);
        }
        .cpdf-modal .inner {
            position: relative;
            width: 40%;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            background-color: #fff;
            padding: 30px;
        }
        .cpdf-modal .form-control { display: none }
        .cpdf-modal .form-control.active { display: block }
    </style>

    <script>
        $.fn.pdfeditor = function( options ) {

            let obj = this;
            let editor = $(obj);
            let activePage;
            let activePagePart;
            let activePageID;
            let pages = [];
            let pagesCount = 0;
            let elements = {};
            let elementsNextId = 1;
            let settings = $.extend({
                pages: 1,
                page_width: 210,
                page_height: 297,
                px_per_cm: 37.79,
                px_per_mm: 3.779,
                px_per_point: 1.33,
                top_margin:	10,
                right_margin: 10,
                left_margin: 10,
                page_header: 20,
                page_footer: 20,
                json: {}
            }, options );

            // Init
            editor.addClass('cpdf-editor');
            // Create context menu
            let contextMenu = $('body').append('<div class="cpdf-context-menu">').find('.cpdf-context-menu');
            // Create modal
            let modal = $('body').append(
                $('<div class="cpdf-modal cpdf-modal-context">').append(
                    $('<div class="inner">').append(
                        $('<div class="form-group">').append(
                            $('<label for="cpdf-input">'),
                            $('<textarea id="cpdf-input" class="form-control active">'),
                            $('<input id="cpdf-input2" class="wheel form-control">').wheelColorPicker('format', 'css')
                        ),
                        $('<button class="btn btn-primary save-modal" style="margin-right: 10px">').html('Save'),
                        $('<button class="btn btn-secondary close-modal">').html('Close')
                    )
                )
            ).find('.cpdf-modal.cpdf-modal-context');
            let addPageModal = $('body').append(
                $('<div class="cpdf-modal add-page-modal">').append(
                    $('<div class="inner">').append(
                        // $('<div class="form-group">').append(
                        //     $('<label for="cpdf-input-w">').text('Width (mm)'),
                        //     $('<input id="cpdf-input-w" name="w" class="form-control active">').val(settings.page_width)
                        // ),
                        // $('<div class="form-group">').append(
                        //     $('<label for="cpdf-input-h">').text('Height (mm)'),
                        //     $('<input id="cpdf-input-h" name="h" class="form-control active">').val(settings.page_height)
                        // ),
                        $('<div class="form-group">').append(
                            $('<label for="cpdf-input-paper">').text('Paper'),
                            $('<select id="cpdf-input-paper" name="paper" class="form-control active">').append(
                                $('<option>').val(210).html('A4'),
                                $('<option>').val(297).html('A3'),
                                $('<option>').val(148).html('A5')
                            )
                        ),
                        $('<div class="form-group">').append(
                            $('<label for="cpdf-input-orientation">').text('Orientation'),
                            $('<select id="cpdf-input-orientation" name="orientation" class="form-control active">').append(
                                $('<option>').val('P').html('Portrait'),
                                $('<option>').val('L').html('Landscape')
                            )
                        ),
                        $('<div class="form-group">').append(
                            $('<label for="cpdf-input-tm">').text('Margin top/bottom (mm)'),
                            $('<input id="cpdf-input-tm" name="tm" class="form-control active">').val(settings.top_margin)
                        ),
                        $('<div class="form-group">').append(
                            $('<label for="cpdf-input-lm">').text('Margin left (mm)'),
                            $('<input id="cpdf-input-lm" name="lm" class="form-control active">').val(settings.left_margin)
                        ),
                        $('<div class="form-group">').append(
                            $('<label for="cpdf-input-rm">').text('Margin right (mm)'),
                            $('<input id="cpdf-input-rm" name="rm" class="form-control active">').val(settings.right_margin)
                        ),
                        $('<div class="form-group">').append(
                            $('<label for="cpdf-input-header">').text('Header height (mm)'),
                            $('<input id="cpdf-input-header" name="header" class="form-control active">').val(settings.page_header)
                        ),
                        $('<div class="form-group">').append(
                            $('<label for="cpdf-input-footer">').text('Footer height (mm)'),
                            $('<input id="cpdf-input-footer" name="footer" class="form-control active">').val(settings.page_footer)
                        ),
                        $('<button class="btn btn-primary save-modal" style="margin-right: 10px">').html('Add'),
                        $('<button class="btn btn-secondary close-modal">').html('Close')
                    )
                )
            ).find('.cpdf-modal.add-page-modal');
            // Create helper lines
            let tVerticalLine = editor.append('<div id="vertical-t-line" class="helper-line">').find('#vertical-t-line');
            let bVerticalLine = editor.append('<div id="vertical-b-line" class="helper-line">').find('#vertical-b-line');
            let lHorizontalLine = editor.append('<div id="horizontal-l-line" class="helper-line horizontal">').find('#horizontal-l-line');
            let rHorizontalLine = editor.append('<div id="horizontal-r-line" class="helper-line horizontal">').find('#horizontal-r-line');

            // Init buttons
            $('.cpdf-action').on('click', function() {
                let type = $(this).data('type');
                if (type === 'add-rect') {
                    createElement('rect');
                } else if (type === 'add-text') {
                    createElement('text');
                }
            });

            /**
             * Add page
             */
             function addPage(paper = null, orientation = null, top_margin = null, right_margin = null, left_margin = null, header = null, footer = null, insertAfter = null)
            {
                let width = settings.page_width,
                    height = settings.page_height;
                if (top_margin === null) top_margin = settings.top_margin;
                if (right_margin === null) right_margin = settings.right_margin;
                if (left_margin === null) left_margin = settings.left_margin;
                if (header === null) header = settings.page_header;
                if (footer === null) footer = settings.page_footer;

                // Set paper width, height
                let pageSize = 'A4';
                if (paper !== null && orientation !== null) {
                    console.log(paper);
                    if (paper === 297) {
                        if (orientation === 'P') {
                            width = 297;
                            height = 420;
                        } else if (orientation === 'L') {
                            width = 420;
                            height = 297;
                        }
                        pageSize = 'A3';
                    } else if (paper === 210) {
                        if (orientation === 'P') {
                            width = 210;
                            height = 297;
                        } else if (orientation === 'L') {
                            width = 297;
                            height = 210;
                        }
                    } else if (paper === 148) {
                        if (orientation === 'P') {
                            width = 148;
                            height = 210;
                        } else if (orientation === 'L') {
                            width = 210;
                            height = 148;
                        }
                        pageSize = 'A5';
                    }
                }

                // Remove active page
                if (typeof activePagePart !== 'undefined') {
                    activePagePart.removeClass('active');
                }

                // Create paper
                let page = $('<div class="page">').css({
                    'width': (width * settings.px_per_mm) + 'px',
                    'height': (height * settings.px_per_mm) + 'px',
                    'padding-top': (top_margin * settings.px_per_mm) + 'px',
                    'padding-bottom': (top_margin * settings.px_per_mm) + 'px',
                    'padding-right': (right_margin * settings.px_per_mm) + 'px',
                    'padding-left': (left_margin * settings.px_per_mm) + 'px',
                    'position': 'relative'
                });

                pagesCount++;
                page.attr('id', 'page-' + pagesCount);
                page.attr('data-page', pagesCount);
                pages.push(pagesCount);
                elements[pagesCount] = {};
                elements[pagesCount]['page'] = {};
                elements[pagesCount]['page']['width'] = width;
                elements[pagesCount]['page']['height'] = height;
                elements[pagesCount]['page']['padding-top'] = top_margin;
                elements[pagesCount]['page']['padding-right'] = right_margin;
                elements[pagesCount]['page']['padding-left'] = left_margin;
                elements[pagesCount]['page']['size'] = pageSize;

                // Create editable area
                let innerArea = $('<div class="inner">').css({
                    'width': ((width - right_margin - left_margin) * settings.px_per_mm) + 'px',
                    'height': ((height - top_margin - top_margin) * settings.px_per_mm) + 'px',
                    'position': 'relative'
                });
                page.append(innerArea);

                // Create multiple parts in page
                // Header
                if (header) {
                    elements[pagesCount]['header'] = {};
                    elements[pagesCount]['header']['height'] = header;
                    elements[pagesCount]['header']['elements'] = {};

                    let header_ = $('<div class="header page-part" data-type="header">').css({
                        'position': 'absolute',
                        'width': 'calc(100% + 2px)',
                        'left': '-1px',
                        'top': '-1px',
                        'height': (header * settings.px_per_mm) + 'px',
                    });
                    innerArea.append(header_);
                }
                // Footer
                if (footer) {
                    elements[pagesCount]['footer'] = {};
                    elements[pagesCount]['footer']['height'] = footer;
                    elements[pagesCount]['footer']['elements'] = {};
                    let footer_ = $('<div class="footer page-part" data-type="footer">').css({
                        'position': 'absolute',
                        'width': 'calc(100% + 2px)',
                        'left': '-1px',
                        'bottom': '-1px',
                        'height': (footer * settings.px_per_mm) + 'px',
                    });
                    innerArea.append(footer_);
                }

                // Main content / body
                let body = $('<div class="body page-part" data-type="body">').css({
                    'position': 'absolute',
                    'width': 'calc(100% + 2px)',
                    'left': '-1px',
                    'top': (header * settings.px_per_mm) - 2 + 'px',
                    'height': ((height - header - footer - (top_margin * 2)) * settings.px_per_mm) + 4 + 'px',
                });
                elements[pagesCount]['body'] = {};
                elements[pagesCount]['body']['elements'] = {};
                body.addClass('active');
                innerArea.append(body);

                let actions = $('<div class="cpdf-actions">').append(
                    $('<button class="btn btn-sm btn-success page-action" data-action="add_page" data-id="' + pagesCount + '">').html('Add page').on('click', add_page),
                    $('<button class="btn btn-sm btn-primary page-action" data-action="copy_page" data-id="' + pagesCount + '">').html('Copy page').on('click', copy_page),
                    $('<button class="btn btn-sm btn-warning page-action" data-action="delete_page" data-id="' + pagesCount + '">').html('Delete page').on('click', delete_page),
                );

                // Add new page
                if (insertAfter !== null) {
                    let p = $('#page-' + insertAfter).next();
                    p.after(page);
                    page.after(actions);
                } else {
                    $(obj).append(page);
                    page.after(actions);
                }

                activePageID = pagesCount;
                activePage = page;
                activePagePart = body;
            }
            addPage();

            function add_page(e)
            {
                e.preventDefault();
                let pageID = $(this).data('id');
                addPageModal.show();
                addPageModal.off('click', '.save-modal');
                addPageModal.on('click', '.save-modal', function(e) {
                    e.preventDefault();
                    addPage(
                        parseInt(addPageModal.find('select[name="paper"]').val()),
                        addPageModal.find('select[name="orientation"]').val(),
                        parseInt(addPageModal.find('input[name="tm"]').val()),
                        parseInt(addPageModal.find('input[name="rm"]').val()),
                        parseInt(addPageModal.find('input[name="lm"]').val()),
                        parseInt(addPageModal.find('input[name="header"]').val()),
                        parseInt(addPageModal.find('input[name="footer"]').val()),
                        pageID
                    );
                    addPageModal.hide();
                });
            }
            addPageModal.on('click', '.close-modal', function(e) { e.preventDefault(); addPageModal.hide(); });
            function copy_page(e)
            {
                e.preventDefault();

                // Create next page id
                pagesCount = Number(pagesCount) + 1;

                let pageID = $(this).closest('.cpdf-actions').prev().data('page');
                let newPageId = pagesCount;
                let clonedPage = editor.find('#page-' + pageID).clone();

                // Default page settings
                clonedPage.attr('id', 'page-' + newPageId).data('page', newPageId).attr('data-page', newPageId);
                pages.push(newPageId);

                // Store ids to create events on them
                let ids = [];

                // Copy back part
                let copiedElements = jsonCopy(elements[pageID]);
                let newPage = {};
                $.each(copiedElements, function(key, pagePart) {
                    if (typeof pagePart.elements !== "undefined") {
                        newPage[key] = {};
                        if (key !== 'body')
                            newPage[key].height = pagePart.height;
                        // Copy elements
                        newPage[key]["elements"] = {};
                        $.each(pagePart.elements, function(i, el) {
                            let newId = uniqueId();
                            newPage[key]["elements"][newId] = el;
                            newPage[key]["elements"][newId].id = newId;
                            newPage[key]["elements"][newId].layer = elementsNextId;
                            clonedPage.find('#cpdf_element_' + i).attr('id', 'cpdf_element_' + newId).data('id', newId)
                                .attr('data-id', newId).data('layer', elementsNextId);
                            ids.push(newId);
                            elementsNextId++;
                        });
                    } else {
                        newPage.page = pagePart;
                    }
                });

                // Add new page to object
                elements[newPageId] = newPage;

                // Add page to view
                $(this).closest('.cpdf-actions').after(clonedPage);

                // Add events to new elements
                $.each(ids, function(i, id) {
                    setElementEvents(id);
                });

                let clonedActions = $(this).closest('.cpdf-actions').clone();
                clonedActions.find('.page-action').each(function() {
                    let action = $(this).data('action');
                    if (action === 'add_page') $(this).on('click', add_page);
                    else if (action === 'delete_page') $(this).on('click', delete_page);
                    else if (action === 'copy_page') $(this).on('click', copy_page);
                    $(this).attr('data-id', pagesCount).data('id', pagesCount);
                });
                clonedPage.after(clonedActions);
            }
            /**
             * Delete page
             */
            function delete_page(e)
            {
                e.preventDefault();
                if (pages.length < 2) return;

                let pageID = $(this).closest('.cpdf-actions').prev().data('page');
                // Delete elements on page
                delete elements[pageID];
                // Delete page
                pages = jQuery.grep(pages, function(value) {
                    return value != pageID;
                });
                editor.find('#page-' + pageID).remove();
                $(this).closest('.cpdf-actions').remove();

                // Activate first available page
                activePageID = pages[0];
                activePage = editor.find('#page-' + pages[0]);
                activePagePart = activePage.find('.body').addClass('active');
            }


            /**
             * Activate page part
             */
            function activatePagePart(e) {
                $(this).closest('.inner').find('.active').removeClass('active');
                $(this).addClass('active');
                activePage = $(this).closest('.page');
                activePageID = activePage.attr('data-page');
                activePagePart = $(this);
            }
            editor.on('mousedown', '.page-part', activatePagePart);

            /**
             * Create new element
             *
             * @param type
             * @returns {boolean}
             */
            function createElement(type)
            {
                // Check if is selected some page
                if (typeof activePagePart === 'undefined') return false;

                let id = uniqueId();
                let newElement = {
                    id: id,
                    type: type,
                    layer: elementsNextId,
                    top: 0,
                    left: 0,
                    value: '',
                    bg_color: 'transparent',
                    bg: 'yes',
                    border_color: 'black',
                    border: 'yes'
                };

                let elem;
                let w = 80;
                let h = 30;
                if (type === 'rect') {
                    elem = $('<div class="cpdf_element cpdf_rect" tabindex="0" data-id="' + id + '" data-layer="' + elementsNextId + '" data-type="rect" id="cpdf_element_' + id + '">').append(
                        $('<div class="resizers">').append(
                            "<div class='resizer top-left'></div><div class='resizer top-right'></div><div class='resizer bottom-left'></div><div class='resizer bottom-right'></div>"
                        )
                    );
                    newElement.bg_color = '#ffffff';
                    newElement.border_color = '#63a1ff';
                    w = 50;
                    h = 50;
                }
                else if (type === 'text') {
                    elem = $('<div class="cpdf_element cpdf_text" tabindex="0" data-id="' + id + '" data-layer="' + elementsNextId + '" data-type="text" id="cpdf_element_' + id + '">').append(
                        $('<div class="resizers">').append(
                            "<div class='resizer top-left'></div><div class='resizer top-right'></div><div class='resizer bottom-left'></div><div class='resizer bottom-right'></div>"
                        ),
                        $('<span class="value">').text('Value')
                    );
                    newElement.value = 'Value';
                    newElement.font_size = 12;
                    newElement.line_height = 16;
                    newElement.font_style = 'R';
                    newElement.bg_color = '#ffffff';
                    newElement.border_color = '#eee';
                    newElement.border = 'no';
                    newElement.hAlign = "L";
                    newElement.vAlign = "T";
                    newElement.padding = 0;
                }

                newElement.width = Number((w / settings.px_per_mm).toFixed());
                newElement.height = Number((h / settings.px_per_mm).toFixed());
                setElement(id, newElement);

                // Set default css
                elem.css({
                    'position': 'absolute',
                    'width': w,
                    'height': h,
                    'background-color': newElement.bg_color,
                    'border': '1px ' + (newElement.border == 'yes' ? 'solid ' : 'dashed ') + newElement.border_color
                });
                activePagePart.append(elem);
                setElementEvents(id);

                // Set new next id
                elementsNextId++;
            }
            function setElementEvents(id) {
                let elem = $('#cpdf_element_' + id);
                elem.mousedown(handle_dragger);
                makeResizableDiv('#cpdf_element_' + id, elem);
                elem.on('contextmenu', context_menu);
                elem.on('dblclick', function (e) {
                    window.contextMenuElementID = $(this).data('id');
                    context_methods.el_value();
                });
                elem.trigger('mousedown');
                elem.on('mouseenter', function() { $(this).addClass('active') });
                elem.on('mouseleave', function() { $(this).removeClass('active') });
            }

            function getElement(id) {
                return elements[activePageID][activePagePart.attr('data-type')]['elements'][id];
            }
            function deleteElement(id) {
                delete elements[activePageID][activePagePart.attr('data-type')]['elements'][id];
            }
            function setElement(id, value) {
                elements[activePageID][activePagePart.attr('data-type')]['elements'][id] = value;
            }
            function getElementKey(id, key, value) {
                return elements[activePageID][activePagePart.attr('data-type')]['elements'][id][key];
            }
            function setElementKey(id, key, value) {
                elements[activePageID][activePagePart.attr('data-type')]['elements'][id][key] = value;
            }


            // Show modal with params
            function showModal(labelValue, save, val = null, wheel = false)
            {
                // Hide context menu
                contextMenu.hide();

                // Color wheel
                if (wheel) {
                    modal.find('.form-control.active').removeClass('active').parent()
                        .find('.wheel').addClass('active');
                    if (val !== null) {
                        let color = $.fn.wheelColorPicker.strToColor(val);
                        modal.find('.wheel').wheelColorPicker('setColor', color);
                        val = rgb2hex(val);
                    }
                } else {
                    modal.find('.form-control.active').removeClass('active').parent()
                        .find('textarea').addClass('active');
                }
                let input = modal.find('.form-control.active');
                let label = modal.find('label');

                label.html(labelValue);
                input.val(val);
                modal.show();
                modal.find('.save-modal').unbind();
                modal.off('click', '.save-modal');
                modal.on('click', '.save-modal', save);
            }
            function getModalValue() {
                return modal.find('.form-control.active').val();
            }
            modal.on('click', '.close-modal', function() {
                modal.hide();
            });

            let context_methods = {};
            // Delete element
            context_methods.el_delete = function()
            {
                let el = getActiveElement();
                let elID = el.data('id');
                deleteElement(elID);
                el.remove();
                contextMenu.hide();
            };
            // Height for the element in milimeters
            context_methods.el_height = function(e)
            {
                showModal('Height for the element in milimeters', save, (getActiveElement().height() / settings.px_per_mm));
                function save() {
                    let v = Number(getModalValue()) * settings.px_per_mm;
                    getActiveElement().height(v);
                    setElementKey(window.contextMenuElementID, 'height', v);
                    modal.hide();
                }
            };
            // Width for the element in milimeters
            context_methods.el_width = function()
            {
                showModal('Width for the element in milimeters', save, (getActiveElement().width() / settings.px_per_mm));
                function save() {
                    let v = Number(getModalValue()) * settings.px_per_mm;
                    getActiveElement().width(v);
                    setElementKey(window.contextMenuElementID, 'width', v);
                    modal.hide();
                }
            };
            // Set background color
            context_methods.el_bgcolor = function()
            {
                showModal('Background color for the element', save, getActiveElement().css("backgroundColor"), true);
                function save() {
                    let v = getModalValue();
                    getActiveElement().css({'background-color': v});
                    setElementKey(window.contextMenuElementID, 'bg_color', v);
                    modal.hide();
                }
            };
            // Set background
            context_methods.el_bg = function(e)
            {
                if (e.data.data === "v") return;
                if (e.data.data === 'no') getActiveElement().css({'background': 'transparent'});
                if (e.data.data === 'yes') getActiveElement().css({'border': getElementKey(window.contextMenuElementID, "bg_color")});
                setElementKey(window.contextMenuElementID, "bg", e.data.data);
            };
            // Set border color
            context_methods.el_bordercolor = function()
            {
                showModal('Border color for the element', save, getActiveElement().css("border-left-color"), true);
                function save() {
                    let v = getModalValue();
                    getActiveElement().css({'border-color': v});
                    setElementKey(window.contextMenuElementID, 'border_color', v);
                    modal.hide();
                }
            };
            // Set border
            context_methods.el_border = function(e)
            {
                if (e.data.data === "v") return;
                if (e.data.data === 'no') getActiveElement().css({'border': '1px dashed #eee'});
                if (e.data.data === 'yes') getActiveElement().css({'border': '1px solid' + getElementKey(window.contextMenuElementID, "border_color")});
                setElementKey(window.contextMenuElementID, "border", e.data.data);
            };
            // Set text color
            context_methods.el_textcolor = function()
            {
                showModal('Text color for the element', save, getActiveElement().css("color"), true);
                function save() {
                    let v = getModalValue();
                    getActiveElement().css({'color': v});
                    setElementKey(window.contextMenuElementID, 'color', v);
                    modal.hide();
                }
            };
            // Set border color
            context_methods.el_value = function()
            {
                showModal('Value', save, getActiveElement().find('.value').text());
                function save() {
                    let v = getModalValue();
                    v = v.replace(/(?:\r\n|\r|\n)/g, '<br>');
                    getActiveElement().find('.value').html(v);
                    setElementKey(window.contextMenuElementID, 'value', v);
                    modal.hide();
                }
            };
            // Align value horizontally
            context_methods.el_hAlign = function(e)
            {
                let v = "L";
                if (e.data.data === "v") return;
                else if (e.data.data === "center") v = "C";
                else if (e.data.data === "right") v = "R";
                getActiveElement().find('.value').css({'text-align': e.data.data});
                setElementKey(window.contextMenuElementID, 'hAlign', v);
                contextMenu.hide();
            };
            // Align value vertically
            context_methods.el_vAlign = function(e)
            {
                let v = "T";
                if (e.data.data === "v") return;
                else if (e.data.data === "middle") v = "M";
                else if (e.data.data === "bottom") v = "B";
                getActiveElement().find('.value').removeClass('middle').removeClass('bottom').removeClass('top')
                    .addClass(e.data.data);
                setElementKey(window.contextMenuElementID, 'vAlign', v);
                contextMenu.hide();
            };
            // Font style
            context_methods.el_fontstyle = function(e)
            {
                let v = "R";
                let el = getElement(window.contextMenuElementID);
                if (e.data.data === "v") return;
                else if (e.data.data === "bold" && el.font_style === "I") v = "BI";
                else if (e.data.data === "italic" && el.font_style === "B") v = "BI";
                else if (e.data.data === "bold") v = "B";
                else if (e.data.data === "italic") v = "I";

                if (e.data.data === "bold" && el.font_style === "I" || e.data.data === "italic" && el.font_style === "B") {
                    getActiveElement().find('.value').removeClass('regular').addClass('italic').addClass('bold');
                } else {
                    getActiveElement().find('.value').removeClass('bold').removeClass('italic').removeClass('regular')
                        .addClass(e.data.data);
                }
                setElementKey(window.contextMenuElementID, 'font_style', v);
                contextMenu.hide();
            };
            // Font size
            context_methods.el_fontsize = function()
            {
                let fontSize = parseInt(getActiveElement().find('.value').css("font-size"));
                fontSize = parseInt(fontSize * 0.75);
                showModal('Value', save, fontSize);
                function save() {
                    let v = parseInt(getModalValue());
                    let el = getActiveElement();
                    el.find('.value').css({"font-size": v + 'pt'});
                    let lineHeight = parseFloat(el.find('.value').css('line-height')) * settings.px_per_mm;
                    setElementKey(window.contextMenuElementID, 'font_size', v);
                    setElementKey(window.contextMenuElementID, 'line_height', lineHeight);
                    modal.hide();
                }
            };
            // Padding
            context_methods.el_padding = function()
            {
                showModal('Value', save, parseFloat(getActiveElement().find('.value').css("padding-top")));
                function save() {
                    let v = parseFloat(getModalValue());
                    getActiveElement().find('.value').css("padding", v);
                    setElementKey(window.contextMenuElementID, 'padding', v);
                    modal.hide();
                }
            };

            // Helper function to convert rgb to hex
            function rgb2hex(rgb){
                rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
                return (rgb && rgb.length === 4) ? "#" +
                    ("0" + parseInt(rgb[1],10).toString(16)).slice(-2) +
                    ("0" + parseInt(rgb[2],10).toString(16)).slice(-2) +
                    ("0" + parseInt(rgb[3],10).toString(16)).slice(-2) : '';
            }

            // Get active element
            function getActiveElement() {
                if (typeof window.contextMenuElementID !== 'undefined') {
                    return activePagePart.find('.cpdf_element[data-id="' + window.contextMenuElementID + '"]');
                }
            }
            function context_menu(e)
            {
                e.preventDefault();
                let type = $(this).data('type');
                window.contextMenuElementID = $(this).data('id');
                let menu = {};
                menu.delete = 'Delete';
                if (type === 'rect') {
                    // menu.height = 'Change height';
                    // menu.width = 'Change width';
                    menu.bgcolor = 'Background color';
                    menu.bordercolor = 'Border color';

                    menu.border = {};
                    menu.border.v = "Border";
                    menu.border.yes = "Yes";
                    menu.border.no = "No";

                    menu.bg = {};
                    menu.bg.v = "Background";
                    menu.bg.yes = "Yes";
                    menu.bg.no = "No";
                } else if (type === 'text') {
                    menu.value = 'Value';

                    menu.textcolor = 'Text color';
                    menu.bgcolor = 'Background color';
                    menu.bordercolor = 'Border color';

                    menu.padding = 'Padding';

                    menu.fontstyle = {};
                    menu.fontstyle.v = "Font style";
                    menu.fontstyle.regular = "Regular";
                    menu.fontstyle.bold = "Bold";
                    menu.fontstyle.italic = "Italic";

                    menu.fontsize = "Font size";

                    menu.hAlign = {};
                    menu.hAlign.v = "Text align horizontal";
                    menu.hAlign.left = "Left";
                    menu.hAlign.center = "Center";
                    menu.hAlign.right = "Right";

                    menu.vAlign = {};
                    menu.vAlign.v = "Text align vertical";
                    menu.vAlign.top = "Top";
                    menu.vAlign.middle = "Middle";
                    menu.vAlign.bottom = "Bottom";

                    menu.border = {};
                    menu.border.v = "Border";
                    menu.border.yes = "Yes";
                    menu.border.no = "No";

                    menu.bg = {};
                    menu.bg.v = "Background";
                    menu.bg.yes = "Yes";
                    menu.bg.no = "No";
                }

                let html = $('<div class="inner" data-id="' + window.contextMenuElementID + '">');
                for (const key in menu) {
                    if ( !menu.hasOwnProperty(key) ) return;
                    let item = menu[key];
                    if (typeof item !== "object") {
                        html.append(
                            $('<a>').html(item).on('click', context_methods['el_' + key])
                        );
                    } else {
                        html.append(
                            $('<a>').html(item.v)
                        );
                        let dropdown = $('<div class="dropdown">');
                        for (const k in item) {
                            if ( !item.hasOwnProperty(k)) return;
                            let i = item[k];
                            dropdown.append(
                                $('<a>').html(i).on('click', {data: k},context_methods['el_' + key])
                            );
                        }
                        html.append(dropdown);
                    }
                }
                contextMenu.html(html);
                contextMenu.css({
                    'left': e.pageX,
                    'top': e.pageY
                });
                contextMenu.show();
            }
            $('body').on('click', function(e) {
                if ($(e.target).closest('.cpdf-context-menu').length < 1) {
                    $('.cpdf-context-menu').hide();
                }
            });


            /**
             * Handle dragging
             *
             * @param e
             */
            function handle_dragger(e)
            {
                window.my_dragging = {};
                my_dragging.pageX0 = e.pageX;
                my_dragging.pageY0 = e.pageY;
                my_dragging.elem = this;
                my_dragging.offset0 = $(this).offset();
                my_dragging.width = $(this).outerWidth();
                my_dragging.height = $(this).outerHeight();
                // Setup active page positions
                my_dragging.pageLeft = activePagePart.offset().left;
                my_dragging.pageRight = my_dragging.pageLeft + activePagePart.outerWidth();
                my_dragging.pageTop = activePagePart.offset().top;
                my_dragging.pageBottom = my_dragging.pageTop + activePagePart.outerHeight();
                my_dragging.pageHeight = activePagePart.height();
                my_dragging.pageWidth = activePagePart.width();
                // Setup inner section border
                my_dragging.pageBorder = 1;

                // Show lines
                lHorizontalLine.show();
                rHorizontalLine.show();
                tVerticalLine.show();
                bVerticalLine.show();

                function handle_dragging(e) {
                    if (typeof window.is_resizing !== "undefined") return false;

                    let left = my_dragging.offset0.left + (e.pageX - my_dragging.pageX0);
                    let right = left + my_dragging.width;
                    let top = my_dragging.offset0.top + (e.pageY - my_dragging.pageY0);
                    let bottom = top + my_dragging.height;

                    // Prevent from dragging out of wrapper
                    // Left
                    if ((left - my_dragging.pageLeft) <= my_dragging.pageBorder)
                        left = my_dragging.pageLeft + my_dragging.pageBorder;
                    // Right
                    if ((my_dragging.pageRight - right) <= my_dragging.pageBorder)
                        left = (my_dragging.pageRight - my_dragging.width) - my_dragging.pageBorder;
                    // Top
                    if ((top - my_dragging.pageTop) <= my_dragging.pageBorder)
                        top = my_dragging.pageTop + my_dragging.pageBorder;
                    // Bottom
                    if ((my_dragging.pageBottom - bottom) <= my_dragging.pageBorder)
                        top = (my_dragging.pageBottom - my_dragging.height) - my_dragging.pageBorder;

                    lHorizontalLine.offset({left: left});
                    rHorizontalLine.offset({left: left + (my_dragging.width - 1)});
                    tVerticalLine.offset({top: top});
                    bVerticalLine.offset({top: top + (my_dragging.height - 1)});

                    // Remove old center lines
                    activePagePart.find('.center-line').remove();
                    // Is in center vertical
                    let cssTop = parseInt($(my_dragging.elem).css('top'), 10);
                    let elementTopWithHeight = (my_dragging.height / 2) + cssTop;
                    let verticalPageCenter = my_dragging.pageHeight / 2;
                    if (elementTopWithHeight > (verticalPageCenter - 1) && elementTopWithHeight < (verticalPageCenter + 1)) {
                        activePagePart.append(
                            $('<span class="center-line center-vertical">')
                        )
                    }
                    // Is in center horizontal
                    let cssLeft = parseInt($(my_dragging.elem).css('left'), 10);
                    let elementLeftWithWidth = (my_dragging.width / 2) + cssLeft;
                    let horizontalPageCenter = my_dragging.pageWidth / 2;
                    if (elementLeftWithWidth > (horizontalPageCenter - 1) && elementLeftWithWidth < (horizontalPageCenter + 1)) {
                        activePagePart.append(
                            $('<span class="center-line center-horizontal">')
                        )
                    }

                    $(my_dragging.elem)
                        .offset({top: top, left: left});

                    // Update left, top
                    let elID = $(my_dragging.elem).data('id');
                    let elem = getElement(elID);
                    if (typeof elem !== 'undefined') {
                        elem.left = parseInt($(my_dragging.elem).css('left'), 10) / settings.px_per_mm;
                        elem.top = parseInt($(my_dragging.elem).css('top'), 10) / settings.px_per_mm;
                        setElement(elID, elem);
                    }
                }
                function handle_mouseup(e) {
                    $('body')
                        .off('mousemove', handle_dragging)
                        .off('mouseup', handle_mouseup);
                    lHorizontalLine.hide();
                    rHorizontalLine.hide();
                    tVerticalLine.hide();
                    bVerticalLine.hide();
                    activePagePart.find('.center-line').remove();
                }

                $('body')
                    .on('mouseup', handle_mouseup)
                    .on('mousemove', handle_dragging);
            }


            /**
             * Make resizable element
             *
             * @param div
             * @param elem
             */
            function makeResizableDiv(div, elem) {
                // Prevent from dragging
                elem.find('.resizer').on('mouseover', function() {
                    elem.off('mousedown', handle_dragger);
                });
                elem.find('.resizer').on('mouseleave', function() {
                    elem.on('mousedown', handle_dragger);
                });

                // Handle stuff
                const element = document.querySelector(div);
                const resizers = document.querySelectorAll(div + ' .resizer');
                const minimum_size = 5;
                let original_width = 0;
                let original_height = 0;
                let original_x = 0;
                let original_y = 0;
                let original_mouse_x = 0;
                let original_mouse_y = 0;

                // Setup active page positions
                let pageLeft = activePagePart.offset().left;
                let pageTop = activePagePart.offset().top;

                for (let i = 0;i < resizers.length; i++) {
                    const currentResizer = resizers[i];
                    currentResizer.addEventListener('mousedown', function(e) {
                        e.preventDefault();
                        original_width = parseFloat(getComputedStyle(element, null).getPropertyValue('width').replace('px', ''));
                        original_height = parseFloat(getComputedStyle(element, null).getPropertyValue('height').replace('px', ''));
                        original_x = element.getBoundingClientRect().left;
                        original_y = element.getBoundingClientRect().top;
                        original_mouse_x = e.pageX;
                        original_mouse_y = e.pageY;
                        window.addEventListener('mousemove', resize);
                        window.addEventListener('mouseup', stopResize);
                    });

                    function resize(e) {
                        if (currentResizer.classList.contains('bottom-right')) {
                            const width = original_width + (e.pageX - original_mouse_x);
                            const height = original_height + (e.pageY - original_mouse_y);

                            if (width > minimum_size) {
                                element.style.width = width + 'px'
                            }
                            if (height > minimum_size && ((elem.offset().top + height) - (pageTop + activePagePart.outerHeight())) <= 0) {
                                element.style.height = height + 'px'
                            }
                        }
                        else if (currentResizer.classList.contains('bottom-left')) {
                            const height = original_height + (e.pageY - original_mouse_y);
                            const width = original_width - (e.pageX - original_mouse_x);
                            const left = original_x + (e.pageX - pageLeft - original_mouse_x);

                            if (height > minimum_size) {
                                element.style.height = height + 'px';
                            }
                            if (width > minimum_size && left >= 0) {
                                element.style.width = width + 'px';
                                element.style.left = left + 'px'
                            }
                        }
                        else if (currentResizer.classList.contains('top-right')) {
                            const width = original_width + (e.pageX - original_mouse_x);
                            const height = original_height - (e.pageY - original_mouse_y);
                            const top = original_y + (e.pageY - pageTop - original_mouse_y);

                            if (width > minimum_size) {
                                element.style.width = width + 'px';
                            }
                            if (height > minimum_size && top >= 0) {
                                element.style.height = height + 'px';
                                element.style.top = top + 'px';
                            }
                        }
                        else {
                            const width = original_width - (e.pageX - original_mouse_x);
                            const height = original_height - (e.pageY - original_mouse_y);
                            if (width > minimum_size) {
                                element.style.width = width + 'px';
                                element.style.left = original_x + (e.pageX - pageLeft - original_mouse_x) + 'px';
                            }
                            if (height > minimum_size) {
                                element.style.height = height + 'px';
                                element.style.top = original_y + (e.pageY - pageTop - original_mouse_y) + 'px';
                            }
                        }
                    }

                    function stopResize() {
                        // Update width, height
                        let $element = $(element);
                        if ($element.length < 1) return;
                        else if (typeof $element === "undefined") return;

                        let elID = $element.data('id');
                        let elem = getElement(elID);
                        elem.width = parseInt(($element.width() / settings.px_per_mm).toFixed(0));
                        if (elem.width === 189) elem.width = 190;
                        elem.height = parseInt(($element.height() / settings.px_per_mm).toFixed(0));
                        setElement(elID, elem);

                        // Remove listener
                        window.removeEventListener('mousemove', resize)
                    }
                }
            }

            function jsonCopy(src) {
                return JSON.parse(JSON.stringify(src));
            }

            // Clone feature
            $(function() {
                let ctrlDown = false,
                    ctrlKey = 17,
                    cmdKey = 91,
                    vKey = 86,
                    cKey = 67,
                    clone;

                $(document).keydown(function(e) {
                    if (e.keyCode == ctrlKey || e.keyCode == cmdKey) ctrlDown = true;
                    if (ctrlDown && e.keyCode === cKey) {
                        clone = editor.find('.cpdf_element.active').clone();
                        ctrlDown = false;
                    }
                    if (ctrlDown && e.keyCode === vKey) {
                        if (typeof clone !== 'undefined') {
                            let cloneID = clone.data('id');
                            let newID = uniqueId();
                            clone.data('id', newID).attr('data-id', newID).attr('id', 'cpdf_element_' + newID);

                            let newElement = {};
                            newElement = jsonCopy(getElement(cloneID));
                            newElement.id = newID;
                            newElement.layer = elementsNextId;
                            setElement(newID, newElement);

                            elementsNextId++;
                            activePagePart.append(clone);
                            setElementEvents(newID);
                            clone = '';

                            console.log(elements);
                        }
                        ctrlDown = false;
                    }
                }).keyup(function(e) {
                    if (e.keyCode == ctrlKey || e.keyCode == cmdKey) ctrlDown = false;
                });
            });

            /**
             * Generate unique id
             *
             * @returns {string}
             */
            function uniqueId() {
                return Math.random().toString(36).substr(2, 16);
            };

            editor.data = function()
            {
                let data = {};
                data.front = b64EncodeUnicode(editor.html());
                data.back = elements;
                data = JSON.stringify(data);
                return data;
            };

            editor.save = function(url)
            {
                $.ajax({
                    url: url,
                    method: "POST",
                    data: {
                        'data': editor.data()
                    },
                    success: function(data) {
                        editor.trigger('cpdf.send_success');
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        editor.trigger('cpdf.send_failed');
                    }
                })
            };

            editor.load = function(data)
            {
                editor.html(b64DecodeUnicode(data.front));
                elements = data.back;
                pages = [];
                $.each(elements, function(i, page) {
                    pagesCount = i;
                    pages.push(i);
                    $.each(page, function(i, pagePart) {
                        if (typeof pagePart.elements !== "undefined") {
                            $.each(pagePart.elements, function(i, el) {
                                setElementEvents(el.id);
                                elementsNextId = el.layer + 1;
                            });
                        }
                    });
                });
                // Activate page
                editor.find('.page-part:first').trigger('mousedown');
                // Add handlers to page actions
                $.each(editor.find('.page-action'), function() {
                    let action = $(this).data('action');
                    if (action === 'add_page') $(this).on('click', add_page);
                    else if (action === 'delete_page') $(this).on('click', delete_page);
                    else if (action === 'copy_page') $(this).on('click', copy_page);
                });
            };

            function b64EncodeUnicode(str) {
                return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
                    function toSolidBytes(match, p1) {
                        return String.fromCharCode('0x' + p1);
                    }));
            }

            function b64DecodeUnicode(str) {
                // Going backwards: from bytestream, to percent-encoding, to original string.
                return decodeURIComponent(atob(str).split('').map(function(c) {
                    return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
                }).join(''));
            }

            return editor;
        };

        let editor = $('#pdfeditor').pdfeditor();
        $('button[name="submit"]').click(function(e) {
            $('#data_pdftemplate').val(editor.data());
        });
        try {
            let data = JSON.parse($('input[name="data"]').val());
            editor.load(data);
        } catch (e) {}


        $(window).scroll(function(e){
            var $el = $('.fixedElement');
            let top = $('.top-form-wrapper').offset().top;
            var isPositionFixed = ($el.css('position') == 'fixed');
            if ($(this).scrollTop() > (top - 60) && !isPositionFixed){
                $el.css({'position': 'fixed', 'top': '100px'});
            }
            else if ($(this).scrollTop() < (top - 59) && isPositionFixed){
                $el.css({'position': 'absolute', 'top': top + 'px'});
            }
        });
    </script>

@endsection


@section('head')

    <style>
        .fixedElement {
            position:fixed;
            top:0;
            right: 30px;
            width: 200px;
            z-index:100;
        }
    </style>

    {{--<script v:admin:public:src="assets/vendor/jquery.contextmenu/jquery.contextmenu.min.js"></script>--}}
    {{--<script v:admin:public:src="assets/vendor/jquery.confirm/jquery.confirm.min.js"></script>--}}
    <script v:admin:public:src="assets/vendor/jquery.wheelcolorpicker/jquery.wheelcolorpicker-3.0.5.min.js"></script>
    {{--<link rel="stylesheet" v:admin:public:href="assets/vendor/jquery.contextmenu/jquery.contextmenu.min.css">--}}
    {{--<link rel="stylesheet" v:admin:public:href="assets/vendor/jquery.confirm/jquery.confirm.min.css">--}}
    <link rel="stylesheet" v:admin:public:href="assets/vendor/jquery.wheelcolorpicker/jquery.wheelcolorpicker.css">

    {{--<script v:admin:public:src="assets/vendor/pdfdesigner/js/fpdf-designer-contextmenus.js"></script>--}}
    {{--<script v:admin:public:src="assets/vendor/pdfdesigner/js/jquery.fpdf-designer.js"></script>--}}

    {{--<link rel="stylesheet" v:admin:public:href="assets/vendor/pdfdesigner/css/fpdf-designer-style.css">--}}
    {{--<link rel="stylesheet" v:admin:public:href="assets/vendor/pdfdesigner/css/ruler.css">--}}
@endsection
