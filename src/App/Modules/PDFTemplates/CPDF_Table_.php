<?php

namespace Vermal\Admin\Modules\PDFTemplates;


trait CPDF_Table_
{
    private $header;
    private $rows;
    private $widths;

    private $fontFile;

    private $pxPermm = 3.779528;
    private $pxPercm = 37.79528;

    public $maxLineWidth = 300;

    /**
     * Create table
     *
     * @param $data
     * @param array $props
     */
    public function table($data, $props = [])
    {
        $this->header = $data['header'];
        $this->rows = $data['data'];
        $this->widths = $data['widths'];

        $this->SetFont('DejaVu', 'B', 12);
        $this->row($this->header);
    }

    protected function row($row)
    {
        $modifiedRow = [];
        $highest = 0;
        $multiline = false;
        foreach ($row as $key => $cell) {
            $type_space = imagettfbbox($this->FontSizePt, 0, $this->CurrentFont['ttffile'], $cell);

            $width = abs($type_space[4] - $type_space[0]);
            $originalHeight = abs($type_space[5] - $type_space[1]) + 10;

            $lines = ceil(($width / $this->pxPermm) / $this->widths[$key]);
            $value = $this->splitString($cell, $lines);

            $height = (count($value) * $originalHeight) / $this->pxPermm;
            if ($height > $highest) $highest = $height;
            if (count($value) > 1) $multiline = true;

            $modifiedRow[$key] = [
                'w' => $this->widths[$key],
                'h' => $originalHeight / $this->pxPermm,
                'value' => $value,
                'originalValue' => $cell
            ];
        }

        $x = 10;
        foreach ($modifiedRow as $key => $cell) {
            if (isset($modifiedRow[$key - 1]))
                $x += $modifiedRow[$key - 1]['w'];
            $this->SetXY($x, 15);
            $this->column($cell, $highest, $x);
        }
    }

    protected function column($cell, $highest, $x)
    {
        // var_dump($highest);
        $value = $cell['value'];
        if (count($value) < 2) {
            $this->MultiCell($cell['w'], $highest, $value[0],1,'L',false);
        } else {
            // var_dump($cell['h']);
            $this->drawTextBox($cell['originalValue'], $cell['w'], $highest, 'L', 'M', 1);
            // $this->MultiCell($cell['w'], $cell['h'], $cell['originalValue'],1,'L',false);
//            $i = 0;
//            foreach ($value as $k => $v) {
//                $i++;
//                $border = ($i == 1 ? 'LRT' : 'LRB');
//                $border = (isset($value[$k + 1]) && $i !== 1 ? 'LR' : $border);
//                $this->MultiCell($cell['w'], $cell['h'], $v,$border,'L',false);
//            }
        }
    }

    /**
     * Split string into multiple parts
     *
     * @param $string
     * @param $parts
     *
     * @return string $string
     */
    private function splitString($string, $parts)
    {
        $limit = ceil(strlen($string) / $parts);
        $string = str_split($string, $limit);
        return $string;
    }

}
