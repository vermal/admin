<?php

namespace Vermal\Admin\Modules\PDFTemplates;


use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Controller;
use Vermal\Admin\Defaults\Languages;
use Vermal\Admin\Modules\PDFTemplates\Entities\PDFTemplate;
use Vermal\Admin\Modules\Text\Entities\Text;
use Vermal\Admin\Modules\Text\Entities\TextTr;
use Vermal\Admin\Sanitizer;
use Vermal\Database\Database;
use Vermal\DataGrid;
use Vermal\Form\Form;
use Vermal\Router;
use Vermal\Admin\View;


class PDFTemplates extends Controller
{
    protected $defaultViewPath = false;

    public function __construct()
    {
        parent::__construct();
        $this->requiredPermission('pdf-templates', $this->CRUDAction);
        $this->addToBreadcrumb('pdftemplate', 'admin.pdftemplate.index');
    }

    /**
     * Browse all users
     */
    public function index()
    {
        $this->setDefaultViewPath();

        // Create new data grid
        $datagrid = new DataGrid(Database::Model('PDFTemplate')->order('p.id', 'DESC'));

        // Add columns
        $datagrid->addColumn('id', 'ID');
        $datagrid->addColumn('title', $this->_('pdftemplate.title'));

        // Add actions
        $datagrid->addEdit(['admin.pdftemplate.edit', ['id']]);
        $datagrid->addDelete(['admin.pdftemplate.destroy_', ['id']]);

        // Return view
        View::view('crud.read', [
            'datagrid' => $datagrid->build()
        ]);
    }

    /**
     * Show login page
     */
    public function create()
    {
        $this->setFluidLayoutContainer();
        $form = $this->form();
        View::view('cpdf', [
            "form" => $form->build()
        ]);
    }

    /**
     * Login user based on his role
     *
     * @param array $post
     */
    public function store($post)
    {

        $data = json_decode($post->data);

        $form = $this->form();
        if ($form->hasError()) {
            Router::redirect('admin.pdftemplate.create');
        }

        $template = Entity::getEntity('PDFTemplate');

        $template->title = Sanitizer::applyFilter('strip_tags|trim|escape', $post->title);
        $template->design = Sanitizer::applyFilter('trim|escape', $data->front);
        $template->methods = Sanitizer::applyFilter('trim|escape', $data->back);

        Database::saveAndFlush($template);

        // Redirect with message
        $this->alert($this->_('pdftemplate.created'));
        Router::redirect('admin.pdftemplate.index');
    }

    /**
     * Show use dit page
     *
     * @param $id
     * @throws
     */
    public function edit($id)
    {
        $this->setFluidLayoutContainer();

        $form = $this->form();
        $form->setAjax(true);
        $form->setMethod('PUT');
        $form->setAction(routerLink('admin.pdftemplate.update', ['id' => $id]));

        $template = Database::Model('PDFTemplate')->find($id);

        // Set values
        $form->getComponent('title')->setValue($template->title);

        $data = htmlspecialchars(json_encode([
            'front' => $template->design,
            'back' => $template->methods
        ]));
        $form->getComponent('data')->setValue($data);

        // Add to breadcrumb
        $this->addToBreadcrumb('dyn', 'admin.pdftemplate.edit', ['id' => $id], [$template->title]);

        View::view('cpdf', [
            "form" => $form->build()
        ]);
    }

    /**
     * Update user
     *
     * @param array $post
     * @param $id
     */
    public function update($post, $id)
    {
        $form = $this->form();
        if ($form->hasError()) {
            Router::redirect('admin.pdftemplate.create');
        }

        $template = Database::Model('PDFTemplate')->find($id);

        $data = json_decode($post->data);

        foreach ($data->back as $page) {
            foreach ($page as $key => $pagePart) {
                if ($key == 'page') continue;
                var_dump($pagePart->elements);
            }
        }

        $template->title = Sanitizer::applyFilter('strip_tags|trim|escape', $post->title);
        $template->design = Sanitizer::applyFilter('trim|escape', $data->front);
        $template->methods = Sanitizer::applyFilter('trim|escape', $data->back);

        Database::saveAndFlush($template);
        $this->alert($this->_('pdftemplate.edited'));
    }

    /**
     * Delete menu
     *
     * @param $id
     */
    public function destroy($id)
    {
        Database::Model('PDFTemplate')->delete($id);

        // Redirect
        $this->alert($this->_('pdftemplate.deleted'));
        Router::redirect('admin.pdftemplate.index');
    }

    /**
     * Create register form
     *
     * @return Form
     */
    private function form()
    {
        $setValues = false;
        if (isset($_SESSION['form_pdftemplate'])) $setValues = true;
        $form = new Form('pdftemplate', Router::link('admin.pdftemplate.store'));

        // modify values
        if ($setValues) {
            $form->components['data']['value'] = htmlspecialchars($form->components['data']['value']);
        }

        $form->addText('title', $this->_('pdftemplate.title'))->required();
        $form->addHidden('data', '');
        $form->getComponent('output')->setValue('');

        // Add submit button
        $form->addButton('submit', $this->_('global.form.save'))->setClass('btn btn-success');

        return $form;
    }

    /**
     * Generate PDF
     *
     * @param $methods
     * @param $variables
     */
    public static function generatePDF($methods, $variables = [])
    {
        // Create PDF with required methods
        $PDF = new \tFPDF();
        // $PDF->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
        $PDF->AddFont('DejaVu', '', 'DejaVuSansCondensed.ttf', true);
        $PDF->AddFont('DejaVu', 'B', 'DejaVuSans-Bold.ttf', true);
        $PDF->AddFont('DejaVu', 'I', 'DejaVuSans-Oblique.ttf', true);
        $PDF->AddFont('DejaVu', 'BI', 'DejaVuSans-BoldOblique.ttf', true);

        $PDF->SetFont('DejaVu','',12);
        foreach ($methods as $method) {
            $params = $method['params'];
            foreach ($params as $key => $param) {
                if (preg_match('~__variable__-(.*?)-__condition__-(.*?)-__value__-(.*?)$~', $param, $m)) {
                    $params[$key] = self::checkIf(self::getVariable($variables, $m[1]), self::getVariable($variables, $m[2]), $m[3]);
                }  else if (preg_match('~__variable__(.*?)$~', $param, $m)) {
                    $params[$key] = self::getVariable($variables, $m[1]);
                }
            }
            call_user_func_array([$PDF, $method['method']], $params);
        }

        $PDF->Output();
    }

    /**
     * Get value from variables
     *
     * @param $variables
     * @param $key
     * @return mixed
     */
    private static function getVariable($variables, $key)
    {
        if (array_key_exists($key, $variables)) return $variables[$key];
        else return null;
    }

    /**
     * Check if
     *
     * @param $return
     * @param $haystack
     * @param $needle
     * @return string
     */
    private static function checkIf($return, $haystack, $needle)
    {
        if (is_array($haystack)) {
             if (in_array($needle, $haystack) || array_key_exists($needle, $haystack)) {
                 return $return;
             }
        } else if ($haystack === $needle) {
            return $return;
        }
        return '';
    }

    /**
     * Parse pdf methods from jquery plugin
     *
     * @param $output
     * @return array
     */
    public static function parsePDFMethods($output)
    {
        $pdf = Sanitizer::applyFilter('strip_tags', $output);
        $pdf = html_entity_decode($pdf);
        $pdf = htmlspecialchars_decode($pdf);
        $pdf = str_replace('<?PHPrequire(\'fpdf.php\');$pdf = new FPDF();', '', $pdf);
        $pdf = str_replace('$pdf->Output(\'created_pdf.pdf\',\'I\');?>', '', $pdf);
        $pdf = str_replace('$pdf->SetFont(\'Arial\', \'\', 12);', '', $pdf);
        $pdf = preg_replace('!/\*.*?\*/!s', '', $pdf);
        $pdf = explode(';', $pdf);
        $pdf = array_values(array_filter($pdf));

        $pdfMethods = [];
        foreach ($pdf as $el) {
            $el = str_replace('$pdf->', '', $el);
            preg_match('~(.*?)\((.*?)\)$~', $el, $match);
            $params = array_map('trim', explode(',', $match[2]));
            foreach ($params as $key => $param) {
                $params[$key] = str_replace('"', "", $param);
                $params[$key] = str_replace("'", "", $param);
                if ($param === 'true' || $param === 'false') {
                    $params[$key] = $param === 'true' ? true : false;
                } else if (is_numeric($param)) {
                    $params[$key] = (int)$param;
                } else if (preg_match('~\$(.*?)\{\:\$(.*?)\:\}.?=.?\"(.*?)\"$~', $param, $m)) {
                    $params[$key] = '__variable__-' . $m[1] . '-__condition__-' . $m[2] . '-__value__-' . $m[3];
                } else if (preg_match('~\$(.*?)$~', $param, $m)) {
                    $params[$key] = '__variable__' . $m[1];
                }
            }
            $pdfMethods[] = [
                'method' => $match[1],
                'params' => $params
            ];
        }

        return $pdfMethods;
    }

    public static function mergePDF($data, $template)
    {
        $key = 'a370af7d0b76ab754c96600a6cbfede36b48c9b7229980b504aae5a91c6026d4';
        $secret = '49633d9f50d96b71108ecf22d8de829945fc1c04ee4431dfc5b758938ef8cdf5';
        $workspace = 'patrik.m583@gmail.com';
        $resource = 'templates/' . $template . '/output';
        $documentData = json_encode($data);

        $data = [
            'key' => $key,
            'resource' => $resource,
            'workspace' => $workspace
        ];
        ksort($data);

        $signature = hash_hmac('sha256', implode('', $data), $secret);

        $client = new \GuzzleHttp\Client([
            'base_uri' => 'https://us1.pdfgeneratorapi.com/api/v3/'
        ]);

        /**
         * Authentication params sent in headers
         */
        $response = $client->request('POST', $resource, [
            'body' => $documentData,
            'query' => [
                'format' => 'pdf',
                'output' => 'base64'
            ],
            'headers' => [
                'X-Auth-Key' => $key,
                'X-Auth-Workspace' => $workspace,
                'X-Auth-Signature' => $signature,
                'Accept' => 'application/json',
                'Content-Type' => 'application/json; charset=utf-8',
            ]
        ]);
        return json_decode($response->getBody()->getContents());
    }

    public static function createPDF($data, $back)
    {
        $payload = json_encode([
            'data' => $data,
            'back' => $back
        ], JSON_FORCE_OBJECT);

        // $url = "http://185.111.88.189:8080/pdf/sk";
        $url = "http://localhost:8181/sk";
        // Prepare new cURL resource
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

        // Set HTTP Header for POST request
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($payload))
        );

        // Submit the POST request
        $result = curl_exec($ch);

        // Close cURL session handle
        curl_close($ch);

        echo $result;
    }

}
