<?php

namespace Vermal\Admin\Modules\PDFTemplates;


class CPDF extends \tFPDF
{
    use CPDF_TextBox;
    use CPDF_Table_;
//    use CPDF_HTMLTable;

    private $front;
    private $back;

    private $header;
    private $footer;

    public function __construct($data)
    {
        parent::__construct();
        $data = json_decode($data, true);
        $this->front = $data['front'];
        $this->back = $data['back'];

        $this->AddFont('DejaVu', '', 'DejaVuSans.ttf', true);
        $this->AddFont('DejaVu', 'B', 'DejaVuSans-Bold.ttf', true);
        $this->AddFont('DejaVu', 'I', 'DejaVuSans-Oblique.ttf', true);
        $this->AddFont('DejaVu', 'BI', 'DejaVuSans-BoldOblique.ttf', true);

        $this->SetFont('DejaVu','',12);

        $this->AddPage('P', 'A4');

//        $html='<table border="1" width="100%">
//<tr>
//<td width="200" height="30">cell  wefw efwe g erg wef rgwg <br>erwgre er gerwgewr er ergerw erger ergre erer 1</td><td width="200" height="30" bgcolor="#D0D0FF">cell 2</td>
//</tr>
//<tr>
//<td width="200" height="30">cell 3</td><td width="200" height="30">cell 4</td>
//</tr>
//</table>';
//
//        $this->WriteHTML($html);

        $data = [
            'header' => ['Test wef wkejf jowiejo fjrojj ejoj reijgjeú wefwe fwj ejwjf owej fwj oiwje fpw eij owf iej owej fej iofjwei jwoj fiwejúfweooewii rj iúgjerigerjioj gjerj irejg ioúerjúgj erúj r1', 'Test2eee', 'Test3'],
            'data' => [
                ['aaawefwef rg erhtr wefew fer grt erwfrgqe g qgeqrgerg erg ', 'aaa', 'aaaa'],
                ['fwef', 'aaa', 'aaaa'],
                ['awefewaa', 'aaa', 'aaaa'],
                ['feg', 'grg', 'aaaa']
            ],
            'widths' => [
                85, 60, 40
            ]
        ];
        $this->table($data);

        //$this->createPages();
    }


    /**
     * Create all pages based on data
     */
    private function createPages()
    {

        $i = 0;
        foreach ($this->back as $page) {
            $i++;
            $pageMeta = $page['page'];

            // Define orientation
            $orientation = 'P';
            if ($pageMeta['width'] > $pageMeta['height']) $orientation = 'L';
            // Add page
            $this->AddPage($orientation, $pageMeta['size']);
            $this->SetMargins($pageMeta['padding-left'], $pageMeta['padding-top'], $pageMeta['padding-right']);
            // Add header
            $this->pageHeader($page);
            // Create page
            $this->pageBody($page);
            // Add footer
            $this->pageFooter($page);
        }

    }

    /**
     * Create page header and add elements to it
     *
     * it automatically saves header from first page
     * and reuses it on each page
     *
     * @param $page
     */
    function pageHeader($page)
    {
        if (empty($this->header)) $this->header = $page['header'];
        $p = $page['page'];
        foreach ($this->header['elements'] as $element) {
            $element['top'] = $element['top'] + $p['padding-top'];
            $element['left'] = $element['left'] + $p['padding-left'];
            $this->addElement($element['type'], $element);
        }
    }

    /**
     * Create page footer and add elements to it
     *
     * it automatically saves footer from first page
     * and reuses it on each page
     *
     * @param $page
     */
    function pageFooter($page)
    {
        if (empty($this->footer)) $this->footer = $page['footer'];
        $p = $page['page'];
        foreach ($this->footer['elements'] as $element) {
            $element['top'] = $element['top'] + ($p['height'] - $p['padding-top'] - $this->header['height']);
            $element['left'] = $element['left'] + $p['padding-left'];
            $this->addElement($element['type'], $element);
        }
    }

    /**
     * Create page body and add elements to it
     *
     * @param $page
     */
    public function pageBody($page)
    {
        $p = $page['page'];
        foreach ($page['body']['elements'] as $element) {
            $element['top'] = $element['top'] + $p['padding-top'] + $this->header['height'] - 1;
            $element['left'] = $element['left'] + $p['padding-left'];
            $element['height']++;
            $this->addElement($element['type'], $element);
        }
    }

    /**
     * Add new element
     *
     * @param $type
     * @param $data
     */
    private function addElement($type, $data)
    {
        $x = $data['left'];
        $y = $data['top'];
        $w = $data['width'];
        $h = $data['height'];
        $bg_color = $this->hexToRgb($data['bg_color']);
        $border_color = $this->hexToRgb($data['border_color']);

        if ($type === 'rect') {
            $this->SetFillColor($bg_color['r'], $bg_color['g'], $bg_color['b']);
            $this->SetDrawColor($border_color['r'], $border_color['g'], $border_color['b']);
            $this->Rect($x, $y, $w, $h, 'DF');
        } else if ($type === 'text') {
            $this->SetXY($x, $y);
            $this->SetFillColor($bg_color['r'], $bg_color['g'], $bg_color['b']);
            $this->SetDrawColor($border_color['r'], $border_color['g'], $border_color['b']);
            $this->drawTextBox($data['value'], $w, $h, 'L', 'T');
        }
    }

    /**
     * Convert hex to rgb
     *
     * @param $hex
     * @return array
     */
    private function hexToRgb($hex)
    {
        list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
        return [
            'r' => $r,
            'g' => $g,
            'b' => $b
        ];
    }

    /**
     * Create PDF
     */
    public function doPDF()
    {
        // var_dump($this->back);
        $this->Output();
    }


}
