<?php

namespace Vermal\Admin\Modules\Seo;


use Vermal\Admin\Defaults\Controller;
use Vermal\Admin\Defaults\Languages;
use Vermal\Admin\Modules\Multimedia\Entities\Multimedia;
use Vermal\Admin\Upload;
use Vermal\Database\Entity;
use Vermal\DataGrid;
use Vermal\Admin\Modules\Seo\Entities\Seo;
use Vermal\Form\Form;
use Vermal\Admin\View;
use Vermal\Database\Database;
use Vermal\Router;
use Vermal\Traits\DataGridSort;


class Seos extends Controller
{
	use DataGridSort;

	public $defaultViewPath = false;

    public function __construct()
    {
        parent::__construct();
        $this->requiredPermission('seo', $this->CRUDAction);
        $this->addToBreadcrumb('seo', 'admin.seo.index');
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
    	$this->setDefaultViewPath();

        $data = Database::Model('Seo');
        $datagrid = new DataGrid($data);

        $datagrid->addColumn('id', 'ID');
        $datagrid->addColumn('title', $this->_('seo.title'));

        // Add actions
        $datagrid->addEdit(['admin.seo.edit', ['id']]);
        $datagrid->addDelete(['admin.seo.destroy_', ['id']]);

        // Return view
        View::view('crud.read', [
            'datagrid' => $datagrid->build()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
	    $form = $this->form();
	    View::view('create', [
		    "form" => $form->build()
	    ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \stdClass $post
     * @throws
     */
    public function store($post)
    {
        $form = $this->form();
        if ($form->hasError()) {
            Router::redirect('admin.seo.create');
        }

        $seo = Entity::getEntity('Seo');
		$form->hydrate($seo);
		$this->hydrate($seo, $post);

        Database::saveAndFlush($seo);

        // Redirect with message
        $this->alert($this->_('seo.created'));
        Router::redirect('admin.seo.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @throws
     */
    public function edit($id)
    {
        $form = $this->form();
        $form->setAjax(true);
        $form->setMethod('PUT');
        $form->setAction(routerLink('admin.seo.update', ['id' => $id]));

        /** @var Seo $address */
        $seo = Database::Model('Seo')->find($id);

	    $form->setValues($seo, ['image']);

		/** @var Multimedia $multimedia */
		$multimedia = $seo->image;
		if (!empty($multimedia))
			$form->getComponent('image')->setValue($multimedia->getMiniFile());

        View::view('create', [
	        "form" => $form->build(),
			'seo' => $seo
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \stdClass $post
     * @param  int  $id
     * @throws
     */
    public function update($post, $id)
    {
        $form = $this->form();
        if ($form->hasError()) {
            $this->alert($this->_('error.form.empty'), 'danger');
            die();
        }

	    $seo = Database::Model('Seo')->find($id);
		$form->hydrate($seo);
		$this->hydrate($seo, $post);

        $this->alert($this->_('seo.edited'));
        Database::saveAndFlush($seo);
    }

	/**
	 * Hydrate
	 *
	 * @param $entity
	 * @param $post
	 * @throws \Exception
	 */
	public function hydrate(&$entity, $post)
	{
		// Remove image
		if (empty($post->image_hidden_image)) {
			$entity->image = null;
		}
		// Upload file
		if (!empty($_FILES['image']['name'])) {
			$files = Upload::uploadFile('image', 1);
			Upload::cropImages($files);
			foreach ($files as $file) {
				$entity->image = Database::Model('Multimedia')->find($file);
			}
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        Database::Model('Seo')->delete($id);

        // Redirect
        $this->alert($this->_('seo.deleted'));
        Router::redirect('admin.seo.index');
    }

    /**
     * Create register form
     *
     * @return Form
     */
    private function form()
    {
        $form = new Form('seo', routerLink('admin.seo.store'));

		$form->addText('url', $this->_('seo.url'));
		$form->addText('title', $this->_('seo.title'));
		$form->addTextArea('description', $this->_('seo.description'));
		$form->addTextArea('keywords', $this->_('seo.keywords'));
		$form->addInput('image', 'file', $this->_('seo.image'));

        // Add submit button
        $form->addButton('submit', $this->_('global.form.save'))->setClass('btn btn-success');

        return $form;
    }

	/**
	 * Get seo for current page
	 *
	 * @return mixed
	 */
    public static function getSeoForCurrentPage()
	{
		$url = '/' . Router::link(Router::$currentName, Router::$params, false);
		return Database::Model('Seo')
			->where('s.url', '%', $url)
			->first();
	}
}
