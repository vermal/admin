@extends('layout')

@section('body')

	<link rel="stylesheet" v:admin:public:href="js/seo-preview/css/jquery-seopreview.css" type="text/css">
	<script v:admin:public:src="js/seo-preview/js/jquery-seopreview.min.js"></script>

	<div class="card shadow-lg m-b-30">
		<div class="card-body">
			{!! $form !!}
			<div class="mt-5">
				<h6>Google Preview</h6>
				<div id="seopreview-google"></div>
				<h6>Facebook Preview</h6>
				<div id="seopreview-facebook"></div>
			</div>
		</div>
	</div>

	<script>
		$(document).ready(function() {
			function readURL(input) {
				if (input.files && input.files[0]) {
					var reader = new FileReader();
					reader.onload = function (e) {
						$('#js-seo-preview__facebook-image').attr('src', e.target.result);
					};
					reader.readAsDataURL(input.files[0]);
				}
			}

			$("#image_seo").change(function(){
				readURL(this);
			});

			$.seoPreview({
				google_div: "#seopreview-google",
				facebook_div: "#seopreview-facebook",
				metadata: {
					title: $('#title_seo'),
					desc: $('#description_seo'),
					url: {
						full_url: $('#url_seo'),
						base_domain: "https://www.artexe.sk/",
						auto_dash: true,
						use_slug:false
					}
				},
				google: {
					show: true
				},
				facebook: {
					show: true,
					featured_image: $('#image_seo')
				}
			});

			@if(!empty($seo->image))
				$('#js-seo-preview__facebook-image').attr('src', '{{ $seo->image->getFile() }}');
			@endif
		});
	</script>

@endsection
