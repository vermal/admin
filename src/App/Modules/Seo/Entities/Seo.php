<?php

namespace Vermal\Admin\Modules\Seo\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Entities\Translatable;
use Vermal\Admin\Defaults\Model;
use Vermal\Ecommerce\Modules\Product\Entities\Extensions\Attributes;
use Vermal\Ecommerce\Modules\Product\Entities\Extensions\Categories;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="seo")
 * @ORM\HasLifecycleCallbacks
 **/
class Seo extends Model
{
    /**
     * @ORM\Column(type="string")
     */
    protected $title;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $description;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $keywords;

	/**
	 * @ORM\Column(type="string")
	 */
	protected $url;

	/**
	 * @ORM\ManyToOne(targetEntity="\Multimedia")
	 * @ORM\JoinColumn(name="multimedia_id", referencedColumnName="id", nullable=true)
	 */
	protected $image;
}
