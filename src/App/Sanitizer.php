<?php

namespace Vermal\Admin;

class Sanitizer
{

    /**
     * @var array
     */
    private $data;

    /**
     * @var array
     */
    private $rules;

    /**
     * Sanitizer constructor.
     * @param $data
     * @param $rules
     */
    public function __construct($data, $rules)
    {
        $this->data = $data;
        $this->rules = $this->parseRulesArray($rules);
    }

    /**
     * Sanitize data
     *
     * @return array
     */
    public function sanitize()
    {
        $sanitized = $this->data;

        foreach ($this->rules as $attr => $rules) {
            $value = $this->getArrayValue($this->data, $attr);

            foreach ($rules as $rule) {
                $value = self::applyFilter($rule['name'], $value, $rule['options']);
            }

            $this->setArrayValue($sanitized, $attr, $value);
        }

        return $sanitized;
    }

    /**
     * Wrapper arround apply filter
     *
     * @param $rule
     * @param $value
     * @param array $options
     * @return mixed
     */
    public function filter($rule, $value, $options = [])
    {
        return self::applyFilter($rule, $value, $options);
    }

    /**
     * Apply filter for rule
     *
     * @param $rule
     * @param $value
     * @param $options
     * @return mixed
     */
    public final static function applyFilter($rule, $value, $options = [])
    {
        if (!is_null($rule) && is_string($rule) && method_exists(self::class, $rule)) {
            // Apply single filter
            return self::{$rule}($value, $options);
        } else if (strpos($rule, '|') !== false) {
            // Apply multiple filters
            $rules = self::parseRulesFromString($rule);
            foreach ($rules as $rule) {
                $value = self::applyFilter($rule['name'], $value, $options);
            }
            return $value;
        } else {
            // No filters found return given value
            return $value;
        }
    }

    /**
     *  Parse a rules array.
     *
     *  @param  array $rules
     *  @return array
     */
    private function parseRulesArray(array $rules)
    {
        $parsedRules = [];

        $rawRules = $rules;

        foreach ($rawRules as $attribute => $rules) {
            $parsedRules[$attribute] = $this->parseRulesFromString($rules);
        }

        return $parsedRules;
    }

    /**
     * Parse rules from string
     *
     * @param $rules
     * @return array
     */
    private static function parseRulesFromString($rules)
    {
        $rules = explode('|', $rules);
        $parsedRules = [];
        foreach ($rules as $rule) {
            if (strpos($rule, ':')  !== false) {
                // The rule has some options
                list($rule, $options) = explode(':', $rule, 2);
                $parsedRule = [
                    'name' => method_exists(self::class, $rule) ? $rule : null,
                    'options' => array_map('trim', explode(',', $options))
                ];
            } else {
                // This is simple rule e.g. uppercase, trim, ....
                $parsedRule = [
                    'name' => method_exists(self::class, $rule) ? $rule : null,
                    'options' => []
                ];
            }
            $parsedRules[] = $parsedRule;
        }
        return $parsedRules;
    }

    /**
     * Capitalize the given value
     *
     * @param $value
     * @param $options
     * @return string
     */
    private final static function capitalize($value, $options = [])
    {
        return is_string($value) ? ucwords(strtolower($value)) : $value;
    }

    /**
     * Lowercase the given string
     *
     * @param $value
     * @param $options
     * @return string
     */
    private final static function lowercase($value, $options = [])
    {
        return is_string($value) ? strtolower($value) : $value;
    }

    /**
     * Uppercase the given string
     *
     * @param $value
     * @param $options
     * @return string
     */
    private final static function uppercase($value, $options = [])
    {
        return is_string($value) ? strtoupper($value) : $value;
    }

    /**
     * Trims the given string.
     *
     * @param  string  $value
     * @param $options
     * @return string
     */
    private final static function trim($value, $options = [])
    {
        return is_string($value) ? trim($value) : $value;
    }

    /**
     * Strip tags from the given string.
     *
     * @param  string  $value
     * @param $options
     * @return string
     */
    private final static function strip_tags($value, $options = [])
    {
        $allowed_tags = "";
        foreach ($options as $option) {
            $allowed_tags .= (strpos($option, '<') !== false) ? $option : "<{$option}>";
        }
        return is_string($value) ? strip_tags($value, $allowed_tags) : $value;
    }

    /**
     * Escape value
     *
     * @param $value
     * @param $options
     * @return mixed
     */
    private final static function escape($value, $options = [])
    {
        return is_string($value) ? filter_var($value, FILTER_SANITIZE_STRING) : $value;
    }

    /**
     * Get only digit characters from the string.
     *
     * @param  string  $value
     * @param $options
     * @return string
     */
    private final static function digit($value, $options = [])
    {
        return preg_replace('/[^0-9]/si', '', $value);
    }

    /**
     * Get only digit characters from the string.
     *
     * @param  string  $value
     * @param $options
     * @return string
     */
    private final static function float($value, $options = [])
    {
        return (float)filter_var(str_replace(',', '.', str_replace(' ', '', $value)),
            FILTER_VALIDATE_FLOAT);
    }

    /**
     * Format date
     *
     * @param $value
     * @param $options
     * @return false|string
     * @throws \Exception
     */
    private final static function format_date($value, $options = [])
    {
        if (!$value) {
            return $value;
        }
        if (sizeof($options) != 2) {
            throw new \Exception('The Sanitizer Format Date filter requires both the current date format as well as the target format.');
        }
        $currentFormat = trim($options[0]);
        $targetFormat  = trim($options[1]);
        return date_format(date_create_from_format($currentFormat, $value), $targetFormat);
    }

    /**
     * Cast
     *
     * @param  string  $value
     * @param $options
     * @return string
     * @throws
     */
    private final static function cast($value, $options = [])
    {
        $type = isset($options[0]) ? $options[0] : null;
        switch ($type) {
            case 'int':
            case 'integer':
                return (int) $value;
            case 'real':
            case 'float':
            case 'double':
                return (float) $value;
            case 'string':
                return (string) $value;
            case 'bool':
            case 'boolean':
                return (bool) $value;
            case 'object':
                return is_array($value) ? (object) $value : json_decode($value, false);
            case 'array':
                return json_decode($value, true);
            default:
                throw new \Exception("Wrong Sanitizer casting format: {$type}.");
        }
    }


    /**
     * Get value from array
     *
     * @param $array
     * @param $key
     * @return mixed
     */
    private function getArrayValue($array, $key)
    {
        if (isset($array[$key])) {
            return $array[$key];
        } else return false;
    }

    /**
     * Set array value
     *
     * @param $array
     * @param $key
     * @param $value
     * @return bool
     */
    private final static function setArrayValue(&$array, $key, $value)
    {
        if (isset($array[$key])) {
            return $array[$key] = $value;
        } else return false;
    }

}
