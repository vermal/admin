$.fn.formBuilder = function( options, importData = {} ) {

    let obj = this;
    let data = {};
    obj.itemsCount = 0;
    let settings = $.extend({
        modal: "#edit-element",
        json: {},
        locale: {
            modal: {
                title: 'Title',
                placeholder: 'Placeholder',
                name: 'Name',
                description: 'Description',
                choices: 'Choices',
                close: 'Close',
                saveChanges: 'Save Changes',
                other: 'Add choice with custom value'
            },
            el: {
                input: 'Input',
                textarea: 'TextArea',
                date: 'Date field',
                select: 'Select',
                checkbox: 'Checkbox',
                radio: 'Radio'
            },
            add: 'Add',

        }
    }, options );

    // Localization
    if (typeof options.locale !== "undefined" && typeof options.locale === 'string') {
        if (options.locale === 'sk') {
            settings.locale = {
                modal: {
                    title: 'Štítok',
                        placeholder: 'Zástupný text',
                        name: 'Kľúč',
                        description: 'Popis',
                        value: 'Hodnota',
                        choices: 'Možnosti',
                        other: 'Pridať možnosť z vlastnou hodnotou',
                        close: 'Zavrieť',
                        saveChanges: 'Uložiť zmeny'
                },
                el: {
                    input: 'Textové pole',
                    date: 'Dátum',
                    textarea: 'Veľké textové pole',
                    select: 'Rozbalovacie menu',
                    checkbox: 'Zaškrtávacie polícka s viacerými možnosťami',
                    radio: 'Zaškrtávacie polícka s jednou možnostou'
                },
                add: 'Pridať'
            }
        }
    }

    // Add form preview
    let formPreview = $('<div>').attr('id', 'form-preview').addClass('row');
    this.append(formPreview);

    // Add form options
    let addElementWrapper = $('<div>').addClass('add-elements').append($('<div>').addClass('row').append(
        $('<div>').addClass('form-group col-lg-9').append(
            $('<select>').attr('id', 'element').addClass('form-control js-select2').append(
                $('<option>').val('input').html(settings.locale.el.input),
                $('<option>').val('date').html(settings.locale.el.date),
                $('<option>').val('textarea').html(settings.locale.el.textarea),
                $('<option>').val('select').html(settings.locale.el.select),
                $('<option>').val('checkbox').html(settings.locale.el.checkbox),
                $('<option>').val('radio').html(settings.locale.el.radio),
                $('<option>').val('heading').html('Heading'),
                $('<option>').val('paragraph').html('Pragraph'),
                $('<option>').val('margin').html('Margin'),
            )
        ),
        $('<div>').addClass('form-group col-lg-2').append(
            $('<select>').attr('id', 'column').addClass('form-control js-select2').append(
                $('<option>').html('12'),
                $('<option>').html('11'),
                $('<option>').html('10'),
                $('<option>').html('9'),
                $('<option>').html('8'),
                $('<option>').html('7'),
                $('<option>').html('6'),
                $('<option>').html('5'),
                $('<option>').html('4'),
                $('<option>').html('3'),
                $('<option>').html('2'),
                $('<option>').html('1')
            )
        ),
        $('<div>').addClass('col-lg-1').append(
            $('<button>').attr('id', 'add-element').addClass('btn btn-primary').html(settings.locale.add)
        ))
    );
    this.append(addElementWrapper);

    // Create modal for editing
    let modal = $('<div>')
        .addClass('modal fade')
        .attr('id', settings.modal.substr(1))
        .append(
            $('<div>')
                .addClass('modal-dialog')
                .append(
                    $('<div>')
                        .addClass('modal-content')
                        .append(
                            // Modal header
                            $('<div>').addClass('modal-header').append(
                                $('<h5>').addClass('modal-title').html('Form element'),
                                $('<button>')
                                    .attr('type', 'butotn').attr('data-dismiss', 'modal')
                                    .addClass('close')
                                    .append(
                                        $('<span>').attr('aria-hidden', true).html("&times;")
                                    )
                            ),
                            // Element options
                            $('<div>').addClass('modal-body').append(
                                $('<input>').attr('name', 'id').attr('type', 'hidden'),
                                $('<div>').addClass('form-group').append(
                                    $('<label>').attr('for', '_title').text(settings.locale.modal.title),
                                    $('<input>').attr('id', '_title').attr('name', 'title')
                                        .attr('type', 'text').addClass('form-control'),
                                ),
                                $('<div>').addClass('form-group').append(
                                    $('<label>').attr('for', '_placeholder').text(settings.locale.modal.placeholder),
                                    $('<input>').attr('id', '_placeholder').attr('name', 'placeholder')
                                        .attr('type', 'text').addClass('form-control'),
                                ),
                                $('<div>').addClass('form-group').append(
                                    $('<label>').attr('for', '_name').text(settings.locale.modal.name),
                                    $('<input>').attr('id', '_name').attr('name', 'name')
                                        .attr('type', 'text').addClass('form-control'),
                                ),
                                $('<div>').addClass('form-group').append(
                                    $('<label>').attr('for', '_value').text(settings.locale.modal.value),
                                    $('<textarea>').attr('id', '_value').attr('name', 'value')
                                        .addClass('form-control'),
                                ),
                                $('<div>').addClass('form-group').append(
                                    $('<label>').attr('for', '_description').text(settings.locale.modal.description),
                                    $('<textarea>').attr('id', '_description').attr('name', 'description')
                                        .addClass('form-control'),
                                ),
                                $('<div>').addClass('form-group choices').append(
                                    $('<label>').text(settings.locale.modal.choices),
                                    createChoice(null, null)
                                ),
                            ),
                            // Modal footer
                            $('<div>').addClass('modal-footer').append(
                                $('<button>').addClass('btn btn-secondary')
                                    .attr('data-dismiss', 'modal')
                                    .html(settings.locale.modal.close),
                                $('<button>').addClass('btn btn-primary save-changes')
                                    .attr('data-dismiss', 'modal')
                                    .html(settings.locale.modal.saveChanges)
                            )
                        )
                )
        );
    this.append(modal);

    // Create choice for modal
    function createChoice (value, text) {
        return $('<div>').addClass('input-group choice mb-2').append(
            $('<input>').attr('name', 'choices_v[]')
                .attr('type', 'text').addClass('form-control')
                .attr('placeholder', 'Value').val(value),
            $('<input>').attr('name', 'choices[]')
                .attr('type', 'text').addClass('form-control')
                .attr('placeholder', 'Text to show').val(text),
            $('<div>').addClass('input-group-append').append(
                $('<button>').addClass('btn-success shadow-none input-group-text add-choice').text('+'),
                $('<button>').addClass('btn-warning shadow-none input-group-text remove-choice').text('-')
            )
        );
    };

    // generate checkbox/radio
    function createCheckbox (type, name, label, disabled = true) {
        if (type === 'checkbox') name += '[]';
        return $('<div>').addClass('input-group choice mb-2').append(
            $('<div>').addClass('input-group-append').append(
                $('<div>').addClass('input-group-text').append(
                    $('<input>').attr('name', name)
                        .attr('type', type)
                )
            ),
            $('<input>').prop('disabled', disabled).attr('type', 'text').val(label).addClass('form-control')
        )
    };

    // Modal add choice
    $(document).on('click', settings.modal + ' .add-choice', function(e) {
        e.preventDefault();
        let choice = $(this).closest('.choice').clone();
        choice.find('input').val('');
        choice.insertAfter($(this).closest('.choice'));
    });

    // Modal delete choice
    $(document).on('click', settings.modal + ' .remove-choice', function(e) {
        e.preventDefault();
        if ($(this).closest('.choices').find('.choice').length > 1) {
            $(this).closest('.choice').remove();
        }
    });

    // Build element
    function buildElement(element, col, elID = null, title = 'Title', placeholder = null, name = null, choices = [], choiceValues = [], choiceWithCustomValue = false, value = null, order = null, description = null) {
        if (elID === null) elID = unique();
        if (name === null) name = elID;
        let wrapper = $('<div/>')
            .addClass('col-lg-' + col)
            .addClass('form-group')
            .attr('data-id', elID);
        if (order === null) {
            obj.itemsCount++;
            order = obj.itemsCount;
        }

        // Prepare data for JSON
        let jsonData = {
            order: order,
            el: element,
            id: elID,
            title: title,
            placeholder: placeholder,
            col: col,
            name: name,
            value: value,
            description: description,
            choices: choices,
            choiceValues: choiceValues,
            choiceWithCustomValue: choiceWithCustomValue
        };

        // console.log(jsonData.choiceValues);
        // return;

        // Create element
        let el;
        let showLabel = true;
        switch (element) {
            case 'input':
                el = $('<input type="text">')
                    .addClass('form-control')
                    .attr('name', name)
                    .attr('placeholder', placeholder)
                    .attr('value', value)
                    .attr('id', elID);
                break;
            case 'date':
                el = $('<input type="text" class="date">')
                    .addClass('form-control')
                    .attr('name', name)
                    .attr('placeholder', placeholder)
                    .attr('value', value)
                    .attr('id', elID);
                break;
            case 'textarea':
                el = $('<textarea>')
                    .addClass('form-control')
                    .attr('name', name)
                    .attr('placeholder', placeholder)
                    .attr('value', value)
                    .attr('id', elID);
                break;
            case 'select':
                el = $('<select>')
                    .addClass('form-control')
                    .attr('name', name)
                    .attr('id', elID);
                for (let i = 0; i < (jsonData.choices.length > 0 ? jsonData.choices.length : 2); i ++) {
                    let val = (typeof jsonData.choiceValues[i] !== "undefined" ? jsonData.choiceValues[i] : '');
                    let label = (typeof jsonData.choices[i] !== "undefined" ? jsonData.choices[i] : 'Choice ' + (i + 1));
                    el.append($('<option>').text(label).val(val));
                    if (jsonData.choices.length < 1) {
                        jsonData.choices.push(val);
                        jsonData.choiceValues.push(label);
                    }
                }
                break;
            case 'checkbox':
                el = $('<div>');
                for (let i = 0; i < (jsonData.choices.length > 0 ? jsonData.choices.length : 2); i ++) {
                    let val = (typeof jsonData.choiceValues[i] !== "undefined" ? jsonData.choiceValues[i] : 'on');
                    let label = (typeof jsonData.choices[i] !== "undefined" ? jsonData.choices[i] : 'Choice ' + (i + 1));
                    el.append(createCheckbox('checkbox', name, label));
                    if (jsonData.choices.length < 1) {
                        jsonData.choices.push(val);
                        jsonData.choiceValues.push(label);
                    }
                }
                if (choiceWithCustomValue) {
                    el.append(createCheckbox('checkbox', name, '', false));
                }
                break;
            case 'radio':
                el = $('<div>');
                for (let i = 0; i < (jsonData.choices.length > 0 ? jsonData.choices.length : 2); i ++) {
                    let val = (typeof jsonData.choiceValues[i] !== "undefined" ? jsonData.choiceValues[i] : 'on');
                    let label = (typeof jsonData.choices[i] !== "undefined" ? jsonData.choices[i] : 'Choice ' + (i + 1));
                    el.append(createCheckbox('radio', name, label));
                    if (jsonData.choices.length < 1) {
                        jsonData.choices.push(val);
                        jsonData.choiceValues.push(label);
                    }
                }
                if (choiceWithCustomValue) {
                    el.append(createCheckbox('checkbox', name, '', false));
                }
                break;
            case 'heading':
                el = $('<h3 class="el">').html( (value === null ? 'Your heading' : value) );
                showLabel = false;
                break;
            case 'paragraph':
                el = $('<p class="el">').html( (value === null ? 'Your text' : value) );
                showLabel = false;
                break;
            case 'margin':
                el = $('<div class="el mb-4">');
                showLabel = false;
                break;
        }

        // Add actions delete/edit
        let actionWrapper = $('<div/>').addClass('actions mt-2');
        let deleteButton = $('<button>').html('delete').addClass('delete btn btn-sm btn-link');
        let editButton = $('<button>').html('edit').addClass('edit btn btn-sm btn-secondary');
        let up = $('<button>').html('<i class="fas fa-arrow-up"></i>').addClass('up btn btn-sm btn-link');
        let down = $('<button>').html('<i class="fas fa-arrow-down"></i>').addClass('down btn btn-sm btn-link');
        actionWrapper.append(editButton);
        actionWrapper.append(deleteButton);
        actionWrapper.append(up);
        actionWrapper.append(down);

        // Finally add everything to form group
        if (showLabel) {
            wrapper.append($('<label>').addClass('control-label').html(title).attr('for', elID));
        }
        wrapper.append(el);
        wrapper.append($('<span class="form-text text-muted">' + (description !== null ? description : '') + '</span>'));
        wrapper.append(actionWrapper);

        data[elID] = jsonData;
        formPreview.append(wrapper);
    };

    // Create uniqe ID
    const unique = function () {
        return '_' + Math.random().toString(36).substr(2, 9);
    };

    // Add new element
    let addElement = function (e) {
        e.preventDefault();
        let element = $('#element').val();
        let column = $('#column').val();
        if (column === null) column = 12;
        let wrapper = buildElement(element, column);

        // init plugins
        initPlugins();
    };
    this.on('click', '#add-element', addElement);

    // Delete element
    this.on('click', '.delete', function(e) {
        e.preventDefault();
        let fg = $(this).closest('.form-group');
        delete data[fg.data('id')];
        fg.remove();
    });

    // Move element
    this.on('click', '.up', function(e) {
        e.preventDefault();
        let fg = $(this).closest('.form-group');

        if (fg.prev().length > 0) {
            data[fg.prev().data('id')].order++;
            fg.insertBefore(fg.prev());
            data[fg.data('id')].order--;
        }
    });

    let index = 0;
    function sortByKey(o, key, order) {
        let d = {};
        let i = 0;
        $.each(o, function(k, elem) {
            if (elem[key] === order) {
                index++;
                order++;
                return elem;
            } else {
                d[key] = sortByKey(o, key, order);
            }
            i++;
            if (index <= i) return;
        });
        return d;
    }

    this.on('click', '.down', function(e) {
        e.preventDefault();
        let fg = $(this).closest('.form-group');
        if (fg.next().length > 0) {
            data[fg.next().data('id')].order--;
            fg.insertAfter(fg.next());
            data[fg.data('id')].order++;
        }
    });

    // Prepare for edit
    this.on('click', '.edit', function(e) {
        e.preventDefault();
        let modal = $(settings.modal);
        modal.modal();
        let formGroup = $(this).closest('.form-group');
        let elId = formGroup.data('id');
        let label = formGroup.find('label');
        let el = formGroup.find('.form-control, input, .el');
        modal.find('[name="title"]').val(label.html());
        modal.find('[name="value"]').val(el.attr('value'));
        modal.find('[name="placeholder"]').val(el.attr('placeholder'));
        modal.find('[name="name"]').val( (typeof el.attr('name') !== 'undefined' ? el.attr('name').replace('[]', '') : '') );
        modal.find('[name="id"]').val(formGroup.data('id'));
        modal.find('[name="description"]').val(formGroup.find('.form-text').html());

        // Modal show/hide stuff
        modal.find('.choices').hide();
        modal.find('.other-choice').parent().remove();
        modal.find('[name="placeholder"]').parent().show();
        modal.find('[name="name"]').parent().show();
        modal.find('[name="title"]').parent().show();
        modal.find('[name="description"]').parent().show();

        if (el.is('select')) {
            let choices = modal.find('.choices');
            choices.find('.choice').remove();
            el.find('option').each(function() {
                let choice = createChoice($(this).val(), $(this).text());
                choices.append(choice);
            });
            // Modal show/hide stuff
            modal.find('[name="value"]').parent().hide();
            modal.find('[name="placeholder"]').parent().hide();
            modal.find('.choices').show();
        } else if (el.is(':checkbox') || el.is(':radio')) {
            let choices = modal.find('.choices');
            choices.find('.choice').remove();
            el = el.parent().parent();
            el.find('.choice').each(function() {
                if ($(this).children('.form-control').prop('disabled') === false) return;
                let choice = createChoice($(this).find('.input-group-text').val(), $(this).find('.form-control').val());
                choices.append(choice);
            });
            choices.append($('<label>').append(
                $('<input>').attr('type', 'checkbox').addClass('other-choice mr-2').prop('checked', data[elId].choiceWithCustomValue),
                settings.locale.modal.other
            ));
            // Modal show/hide stuff
            modal.find('[name="value"]').parent().hide();
            modal.find('[name="placeholder"]').parent().hide();
            modal.find('.choices').show();
        } else if (el.is('h3') || el.is('p')) {
            modal.find('[name="value"]').val(el.html()).parent().show();
            modal.find('[name="title"]').parent().hide();
            modal.find('[name="placeholder"]').parent().hide();
            modal.find('[name="name"]').parent().hide();
            modal.find('[name="description"]').parent().hide();
        }
    });

    // Save data from modal to element
    $(document).on('click', settings.modal + ' .save-changes', function(e) {
        e.preventDefault();
        let modal = $(this).closest('.modal');
        let elId = modal.find('[name="id"]').val();
        let formGroup = formPreview
            .find('.form-group[data-id="' + elId + '"]');
        let label = formGroup.find('.control-label');
        let el = formGroup.find('.form-control, input, .el');

        label.html(modal.find('[name="title"]').val());
        el.attr('placeholder', modal.find('[name="placeholder"]').val());
        el.attr('name', modal.find('[name="name"]').val());
        el.attr('value', modal.find('[name="value"]').val());
        formGroup.find('.form-text').html(modal.find('[name="description"]').val());

        data[elId].value = modal.find('[name="value"]').val();
        data[elId].title = modal.find('[name="title"]').val();
        data[elId].placeholder = modal.find('[name="placeholder"]').val();
        data[elId].name = modal.find('[name="name"]').val();
        data[elId].description = modal.find('[name="description"]').val();

        if (el.is('select')) {
            el.find('option').remove();
            data[elId].choices = [];
            data[elId].choiceValues = [];
            modal.find('.choices .choice').each(function() {
                let val = $(this).find('input:first').val();
                let text = $(this).find('input:last').val();
                if (val !== '' || text !== '') {
                    el.append($('<option>').val(val).text(text));
                    data[elId].choices.push(text);
                    data[elId].choiceValues.push(val);
                }
            });
        } else if (el.is(':checkbox') || el.is(':radio')) {
            let baseEl = el;
            el = el.parent().parent();
            el.find('.choice').remove();
            data[elId].choices = [];
            data[elId].choiceValues = [];
            modal.find('.choices .choice').each(function() {
                let val = $(this).find('input:first').val();
                let text = $(this).find('input:last').val();
                if (val !== '' || text !== '') {
                    el.append(createCheckbox(baseEl.attr('type'), modal.find('[name="name"]').val(), text));
                    data[elId].choices.push(text);
                    data[elId].choiceValues.push(val);
                }
            });
            // Check if isset other choice
            if (modal.find('.choices .other-choice').is(':checked')) {
                data[elId].choiceWithCustomValue = true;
                el.append(createCheckbox(baseEl.attr('type'), modal.find('[name="name"]').val(), '', false))
            } else {
                data[elId].choiceWithCustomValue = false;
            }
        } else if (el.is('h3') || el.is('p')) {
            el.html(modal.find('[name="value"]').val());
        }
    });

    function setDataForBuilder(data) {
        let order = 0;
        for (let key in data) {
            order++;
            obj.itemsCount++;
            if (data.hasOwnProperty(key)) {
                var row = data[key];
            }
            buildElement(row.el, row.col, row.id, row.title, row.placeholder, row.name, row.choices, row.choiceValues, row.choiceWithCustomValue, row.value, order, row.description);
        }
    }

    function preview() {
        formPreview.find('.actions').toggle();
        $(obj).find('.add-elements').toggle();
    }

    function getData() {
        let result = [];
        $.each(data, function(key, elem) {
            result.push(elem);
        });
        result = result.sort(function(a, b) {
            return (a.order > b.order) ? 1 : ((a.order < b.order) ? -1 : 0);
        });
        return result;
    }

    return {
        obj: obj,
        //json: data,
        setData: setDataForBuilder,
        preview: preview,
        getData: getData
    };
};
