let button = window.vermalButton;
let notify = window.vermalNotify;
let adminAjax = window.vermalAdminAjax;

let formObj = $('form.ajax');

/**
 * Default init handler for ajax forms
 */
formObj.on('start', function(item, data) {
    let btn = data.button;
    button(btn, 'disable', true);
});

/**
 * Default success handler for ajax forms
 */
formObj.on('success', function(item, data) {
    let response = data.response;
    try {
        let obj = JSON.parse(response);
        if (obj.notify !== undefined && obj.notify) {
            notify(obj.notify, obj.type);
        }
    } catch (e) {
        // Not json data
    }
});

/**
 * Default error handler for ajax forms
 */
formObj.on('error', function(item, data) {
    notify(data.response.responseText, 1);
});

/**
 * Conditional selects
 */
$(document).ready(function() {
    $('select.conditional').on('change', function() {
        $('div.' + $(this).attr('id')).hide();
        $('div#' + $(this).attr('id') + '_' + $(this).val()).show();
    });
    $('select.conditional').each(function() {
        $(this).trigger('change');
    });
});


/**
 * Menu Builder
 */
$(document).ready(function() {
    let domenu = $('#domenu-0');
    if (domenu.length < 1) return null;

    let menu = domenu.domenu({
        data: JSON.stringify(domenu.data('data'))
    }).parseJson();

    // Add new item
    menu.onItemAdded(function(item) {
        item.hide();
        item.data('new', true);
        item.find('.edit-item').click();
    });

    // Save menu order
    menu.onItemDrop(function () {
        $('#save-order-data').val(menu.toJson());
        $('#save-order').click();
    });

    // If modal is closed remove new item in menu
    $('#editItem .modal-close').click(function() {
        $('.dd-list .dd-item').each(function() {
            if ($(this).data('new') !== undefined) {
                $(this).remove();
            }
        });
    });

    // Show modal on click edit
    $(document).on('click', '.dd .edit-item', function() {
        let dis = $(this),
            inputs = $(this).closest('.dd3-content').find('.dd-edit-box'),
            item = $('#editItem');

        let type = inputs.find('input[name="type"]').val();
        if (type === "") {
            type = 'static';
        }

        let target = inputs.find('input[name="target"]').val();
        if (target === "") {
            target = '_self';
        }

        item.find('#id').val(inputs.find('input[name="id_"]').val());
        item.find('#title').val(inputs.find('input[name="title"]').val());
        item.find('#type').val(type);
        item.find('#type').change();
        item.find('#url').val(inputs.find('input[name="url"]').val());
        item.find('#route').val(inputs.find('input[name="route"]').val());
        item.find('#parameters').val(inputs.find('input[name="parameters"]').val());
        item.find('#iconClass').val(inputs.find('input[name="icon"]').val());
        item.find('#target').val(target);
        item.find('#target').change();
        item.find('#note').val(inputs.find('input[name="note"]').val()).change();
    });

    // Save value from modal to domenu
    $('#editItem .save').click(function() {
        let dis = $(this),
            item = dis.closest('#editItem'),
            id = item.find('#id').val();

        // Find inputs
        let inputs = null;
        $('.dd-edit-box').each(function() {
            let input = $(this).find('input[name="id_"]');
            if (input.length > 0 && input.val() === id) {
                inputs = $(this);
            }
        });

        if (inputs == null) {
            $('.dd-list .dd-item').each(function() {
                if ($(this).data('new') !== undefined) {
                    inputs = $(this).find('.dd-edit-box');
                    inputs.closest('.dd-item').show();
                }
            });
        }

        // Init editing in domenu
        inputs.closest('.dd3-content').find('.item-name').click();

        inputs.find('input[name="id_"]').val(item.find('#id').val());
        inputs.find('input[name="title"]').val(item.find('#title').val());
        inputs.find('input[name="type"]').val(item.find('#type').val());
        inputs.find('input[name="url"]').val(item.find('#url').val());
        inputs.find('input[name="route"]').val(item.find('#route').val());
        inputs.find('input[name="parameters"]').val(item.find('#parameters').val());
        inputs.find('input[name="target"]').val(item.find('#target').val());
        inputs.find('input[name="note"]').val(item.find('#note').val());

        // Save data to doMenu
        inputs.find('.end-edit').click();
        $('#editItem').modal('hide');
    });

    $('#menuBuilderForm').on('success', function(item, data) {
        let response = data.response;
        try {
            let obj = JSON.parse(response);
            if (obj.id !== undefined) {
                $('.dd-list .dd-item').each(function() {
                    if ($(this).data('new') !== undefined) {
                        $(this).show().find('input[name="id_"]').val(obj.id).attr('value', obj.id);
                    }
                });
            }
        } catch (e) {
            // Not json data
        }
    });

    // Remove menu item from database
    $(document).on('click', '.item-remove', function() {
        let id = $(this).closest('.dd3-content').find('.dd-edit-box input[name="id_"]').val(),
            form = $('#deleteItemForm');
        form.find('input[name="id"]').val(id);
        adminAjax($(this), form);
    });
});


// $(document).ready(function() {
//     let multimedia = $('#multimedia-wrapper');
//     if (multimedia.length < 1) return null;
//     let baseUrl = multimedia.data('base');
//
//     $('.read-folder').click(function(e) {
//         e.preventDefault();
//
//         axios.get(baseUrl + 'admin/multimedia/folder/' + $(this).data('folder'))
//             .then((res) => {
//                 console.log(res);
//             });
//     });
// });
